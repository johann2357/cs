#!/usr/bin/env python
import socket
import time


TCP_IP = '127.0.0.1'
TCP_IP = '192.168.201.63'
TCP_IP = '192.168.201.71'
TCP_PORT = 8005
TCP_PORT = 6000
BUFFER_SIZE = 30  # Normally 1024, but we want fast response

# Client
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

start_time = time.time()
print "SENDING all the messages: ", start_time
for message in xrange(20000):
    s.send(str(message).zfill(BUFFER_SIZE))
    #s.send("unsa")
elapsed_time = time.time() - start_time
print "SENDING all the messages: ", time.time()
print "SENT all the messages duration: ", elapsed_time
s.close()
# end of client


# Server
#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.bind((TCP_IP, TCP_PORT))
#s.listen(1)
#
#conn, addr = s.accept()
#print 'Connection address:', addr
#while 1:
#    data = conn.recv(BUFFER_SIZE)
#    if not data:
#        break
#    print "received data:", data
#conn.close()
## end of server
#
#elapsed_time = time.time() - start_time
#
#print elapsed_time
