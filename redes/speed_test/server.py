#!/usr/bin/env python
import socket
import time


TCP_IP = '0.0.0.0'
TCP_PORT = 6000
TCP_PORT = 8001
BUFFER_SIZE = 30  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
print 'Connection address:', addr
start_time = time.time()
first = True
while 1:
    data = conn.recv(BUFFER_SIZE)
    if first:
        start_time = time.time()
        first = False
    if not data:
        break
    print data,
conn.close()
elapsed_time = time.time() - start_time
print elapsed_time
