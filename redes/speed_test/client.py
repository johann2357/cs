#!/usr/bin/env python
import socket


TCP_IP = '127.0.0.1'
TCP_PORT = 6000
BUFFER_SIZE = 64

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))


for message in xrange(20000):
    s.send(str(message).zfill(BUFFER_SIZE))


# data = s.recv(BUFFER_SIZE)
s.close()
# print "received data:", data
