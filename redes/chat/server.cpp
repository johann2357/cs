#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

std::map<std::string, int> clients;
std::map<std::string, std::string> usr_pass;
std::map<
  std::string,
  std::vector<std::string>
> p_msgs;
std::vector<pthread_t> client_threads;
//char change_name[] = "NICKNAME";
char send_message[] = "MSG";
char p_message[] = "PMSG";
char list_clients[] = "LIST";
char all_clients[] = "REGISTERED-USERS";
char login[] = "LOGIN";
char logout[] = "LOGOUT";
char password[] = "PASSWORD";
char signup[] = "REGISTER";

std::vector<std::string> &split(
      const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

void save_registered_users() {
  std::string tmp;
  std::ofstream myfile;
  myfile.open("users.txt");
  for (std::map<std::string, std::string>::iterator it = usr_pass.begin();
       it != usr_pass.end();
       ++it) {
    tmp = it->first + " " + it->second + "\n";
    myfile << tmp.c_str();
  }
  myfile.close();
}

void load_registered_users() {
  std::string line;
  std::ifstream infile("users.txt");
  while (std::getline(infile, line))
  {
    std::istringstream iss(line);
    std::string a, b;
    if (!(iss >> a >> b)) {
      break;
    }
    usr_pass.insert(
      std::pair<std::string, std::string>(a, b)
    );
  }
}

//void save_p_msg() {
//  std::string tmp;
//  std::ofstream myfile;
//  myfile.open("pmsg.txt");
//  for (std::map<std::string, std::string>::iterator it = usr_pass.begin();
//       it != usr_pass.end();
//       ++it) {
//    tmp = it->first + " " + it->second + "\n";
//    myfile << tmp.c_str();
//  }
//  myfile.close();
//}
//
//void load_p_msg() {
//  std::string line;
//  std::ifstream infile("pmsg.txt");
//  while (std::getline(infile, line))
//  {
//    std::istringstream iss(line);
//    std::string a, b;
//    if (!(iss >> a >> b)) {
//      break;
//    }
//    usr_pass.insert(
//      std::pair<std::string, std::string>(a, b)
//    );
//  }
//}

void* client_thread(void* fd) {
  int n;
  int ConnectFD = *((int*) fd);
  char buffer[256];
  bool logged_in = false;
  bool password_auth = false;
  std::string name;
  std::map<std::string, std::string>::iterator usr_pass_it = usr_pass.end();
  for (;;) {
    bzero(buffer, 256);
    n = read(ConnectFD, buffer, 256);
    if (n < 0) {
      perror("ERROR reading to socket 1");
    }
    std::string tmp_bytes = std::string(buffer);
    int bytes;
    try {
      bytes = std::stoi(tmp_bytes);
    } catch (std::exception const & e) {
      continue;
    }
    char* buffer_message = new char[bytes + 1];
    bzero(buffer_message, bytes + 1);
    n = read(ConnectFD, buffer_message, bytes);
    if (n < 0) {
      perror("ERROR reading to socket 2");
    }
    std::vector<std::string> x = split(buffer_message, ':');
    if (! logged_in) {
      if (x.size() == 3) {
        if (strcmp(signup, x[0].c_str()) == 0) {
          printf("\nSignup: [%s]\n", x[1].c_str());
          // prepare message
          std::string message;
          std::map<
            std::string,
            std::string
          >::iterator it = usr_pass.find(x[1].c_str());
          if (it != usr_pass.end()) {
            message = std::string("Nickname already exists");
          } else {
            message = std::string("Registration was ok");
            usr_pass.insert(
              std::pair<std::string, std::string>(x[1], x[2])
            );
            save_registered_users();
          }
          std::string tmp = "<<< server >>> " + message + "\n";
          // send message
          n = write(ConnectFD, tmp.c_str(), 255);
          if (n < 0) {
            perror("ERROR writing to socket");
          }
        }
      } else if (x.size() == 2) {
        if (strcmp(password, x[0].c_str()) == 0) {
          std::string message;
          if (usr_pass_it != usr_pass.end()) {
            if (usr_pass_it->second == x[1]) {
              message = std::string("You are logged in!");
              name = usr_pass_it->first;
              clients.insert(
                std::pair<std::string, int>(usr_pass_it->first, ConnectFD)
              );
              logged_in = true;
              std::vector<std::string> blank;
              std::vector<std::string> v = p_msgs[name];
              for (auto i = v.begin(); i != v.end(); i++) {
                n = write(ConnectFD, (*i).c_str(), 255);
                if (n < 0) {
                  perror("ERROR writing to socket");
                }
              }
              p_msgs[x[1]] = blank;
            } else {
              message = std::string("Wrong password");
            }
          } else {
              message = std::string("User does not exist");
          }
          std::string tmp = "<<< server >>> " + message + "\n";
          // send message
          n = write(ConnectFD, tmp.c_str(), 255);
          if (n < 0) {
            perror("ERROR writing to socket");
          }
        } else if (strcmp(login, x[0].c_str()) == 0) {
          printf("\nLogin: [%s]\n", x[1].c_str());
          // prepare message
          std::string message;
          usr_pass_it = usr_pass.find(x[1].c_str());
          if (usr_pass_it != usr_pass.end()) {
            message = std::string("Insert your password");
          } else {
            message = std::string("User does not exist");
          }
          std::string tmp = "<<< server >>> " + message + "\n";
          // send message
          n = write(ConnectFD, tmp.c_str(), 255);
          if (n < 0) {
            perror("ERROR writing to socket");
          }
        } 
      }
    } else if (x.size() == 1) {
      if (strcmp(logout, x[0].c_str()) == 0) {
        printf("\nLogout User: [%s]\n", name.c_str());
        n = write(ConnectFD, x[0].c_str(), 256);
        clients.erase(name);
        logged_in = false;
        break;
      } else if (strcmp(list_clients, x[0].c_str()) == 0) {
        std::string tmp("");
        printf("\nList: [%s]\n", buffer_message);
        for (
            std::map<std::string, int>::iterator it = clients.begin();
            it != clients.end();
            ++it) {
          tmp = tmp + it->first + " ";
        }
        tmp = tmp + "\n";
        n = write(ConnectFD, tmp.c_str(), 255);
        if (n < 0) {
          perror("ERROR writing to socket");
        }
      } else if (strcmp(all_clients, x[0].c_str()) == 0) {
        std::string tmp("");
        printf("\nRegistered Users: [%s]\n", buffer_message);
        for (
            std::map<std::string, std::string>::iterator it = usr_pass.begin();
            it != usr_pass.end();
            ++it) {
          tmp = tmp + it->first + " ";
        }
        tmp = tmp + "\n";
        n = write(ConnectFD, tmp.c_str(), 255);
        if (n < 0) {
          perror("ERROR writing to socket");
        }
      }
    } else if (x.size() == 2) {
      // if (strcmp(change_name, x[0].c_str()) == 0) {
      //   printf("\nNew User: [%s]\n", x[1].c_str());
      //   name = x[1];
      //   clients.insert(
      //     std::pair<std::string, int>(x[1], ConnectFD)
      //   );
      // } else
    } else if (x.size() == 3) {
      if (strcmp(send_message, x[0].c_str()) == 0) {
        printf("\nMessage: [%s]\n", buffer);
        std::string tmp = "<<< " + name + " >>> " + std::string(x[2]) + "\n";
        n = write(clients[x[1]], tmp.c_str(), 255);
        if (n < 0) {
          perror("ERROR writing to socket");
        }
      } else if (strcmp(p_message, x[0].c_str()) == 0) {
        printf("\nPMessage: [%s]\n", buffer);
        std::string tmp = "<<< " + name + " >>> " + std::string(x[2]) + "\n";
        std::map<
          std::string,
          std::vector<std::string>
        >::iterator it = p_msgs.find(x[1].c_str());
        std::vector<std::string> v;
        if (it == p_msgs.end()) {
          v.push_back(tmp);
          p_msgs.insert(
            std::pair<std::string, std::vector<std::string>>(x[1], v)
          );
        } else {
          it->second.push_back(tmp);
        }
      }
    }
  }
  shutdown(ConnectFD, SHUT_RDWR);
  close(ConnectFD);
}

int main(void)
{
  load_registered_users();

  struct sockaddr_in stSockAddr;
  int SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

  if (-1 == SocketFD) {
    perror("can not create socket");
    exit(EXIT_FAILURE);
  }

  memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(45001);
  stSockAddr.sin_addr.s_addr = INADDR_ANY;

  if (-1 == bind(
       SocketFD,
       (const struct sockaddr *) &stSockAddr,
       sizeof(struct sockaddr_in)
     )) {
    perror("error bind failed");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  if (-1 == listen(SocketFD, 10)) {
    perror("error listen failed");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  for (;;) {
    int ConnectFD = accept(SocketFD, NULL, NULL);

    if (0 > ConnectFD) {
      perror("error accept failed");
      close(SocketFD);
      exit(EXIT_FAILURE);
    } else {
      pthread_t client_t;
      if (pthread_create(&client_t, NULL, client_thread, &ConnectFD)) {
        fprintf(stderr, "Error creating thread\n");
        return 1;
      }
      int status = pthread_detach(client_t);
      client_threads.push_back(client_t);
    }
  }

  close(SocketFD);
  return 0;
}
