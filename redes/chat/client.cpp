#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <string>
#include <iostream>

char end[] = "LOGOUT\0";
char end2[] = "LOGOUT:\0";

void* listen_thread(void* fd) {
  int n;
  int ConnectFD = *((int*) fd);
  char buffer[256];
  for (;;) {
    bzero(buffer, 256);
    n = read(ConnectFD, buffer, 256);
    printf("%s \n", buffer);
    if (strcmp(end, buffer) == 0) {
      break;
    }
  }
  shutdown(ConnectFD, SHUT_RDWR);
  close(ConnectFD);
}

int main(void) {
  struct sockaddr_in stSockAddr;
  int SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  char buffer[256];
  int n;

  if (-1 == SocketFD) {
    perror("cannot create socket");
    exit(EXIT_FAILURE);
  }

  memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(45001);
  int Res = inet_pton(AF_INET, "0.0.0.0", &stSockAddr.sin_addr);

  if (0 > Res) {
    perror("error: first parameter is not a valid address family");
    close(SocketFD);
    exit(EXIT_FAILURE);
  } else if (0 == Res) {
    perror("char string (second parameter does not contain valid ipaddress");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  if (-1 == connect(
        SocketFD,
        (const struct sockaddr *)&stSockAddr,
        sizeof(struct sockaddr_in)
      )) {
    perror("connect failed");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  bool first_time = true;
  pthread_t client_t;
  for (;;) {
    char input[256];
    std::string input_buffer;
    char size[10];
    bzero(input, 256);
    bzero(size, 10);
    std::getline(std::cin, input_buffer);
    std::string bytes = std::to_string(input_buffer.size()) + "\n";
    printf("-- sent -- \n\n");
    n = write(SocketFD, bytes.c_str(), 256);
    n = write(SocketFD, input_buffer.c_str(), input_buffer.size());
    if (first_time) {
      if (pthread_create(&client_t, NULL, listen_thread, &SocketFD)) {
        fprintf(stderr, "Error creating thread\n");
        return 1;
      }
      pthread_detach(client_t);
      first_time = !first_time;
    }
    if (strcmp(end2, input) == 0) {
      break;
    }
  }

  shutdown(SocketFD, SHUT_RDWR);

  close(SocketFD);
  return 0;
}

/*
 * TAREA
 *
 *             SERVER
 *             matrix
 *         julio | socket ID
 *         julio | socket ID
 *         julio | socket ID
 *         julio | socket ID
 *         julio | socket ID
 *         julio | socket ID
 *
 *   CLIENT                    CLIENT
 *             myname:johann
 *             pablo:holasss
 */
