# AUTHOR: JOHANN NUNEZ
from sys import exit
from time import sleep
import pygame
import matplotlib
matplotlib.use("Agg")
import matplotlib.backends.backend_agg as agg
import pylab

pygame.init()

# GLOBAL CONSTANTS
SIZE = WIDTH, HEIGHT = 1360, 700
GLOBAL_SPEED = 0.02
SCALE_TIME = 1

BLACK = (0, 0, 0)
GRAY = (128, 128, 128)

SCREEN = pygame.display.set_mode(SIZE)


class Time(object):
    now = 0

    def __init__(self, sec):
        self.seconds = sec
        self.pieces = sec * SCALE_TIME / GLOBAL_SPEED

    def __iter__(self):
        return self

    def next(self):
        if self.now < self.pieces:
            new_time = self.now / (SCALE_TIME / GLOBAL_SPEED)
            self.now += 1
            return new_time
        else:
            raise StopIteration()


class Rectangle(object):

    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.width = w
        self.height = h
        self.surface = False

    def update(self):
        self.rect = pygame.draw.rect(
            SCREEN,
            GRAY,
            (self.x, self.y, self.width, self.height)
        )


class Particle(object):

    def __init__(self, x, y, speed=1):
        self.x = x
        self.y = y
        self.surface = pygame.image.load("particle2.png")
        self.rect = self.surface.get_rect()
        self.speed = speed

    def update(self):
        self.x += self.speed
        self.rect = SCREEN.blit(self.surface, (self.x, self.y))


class Scenario(object):

    def __init__(self, *args):
        self.objects = args

    def update(self):
        for obj in self.objects:
            obj.update()
        pygame.display.update()


# USED TO PLOT
x_data = []
x_data2 = []
x_data3 = []
ax_time = []


def simulation():
    p = Particle(x=-12.5, y=100, speed=0.05)
    floor = Rectangle(x=5, y=150, w=1350, h=40)
    scenario = Scenario(p, floor)
    scenario.update()
    time = Time(sec=10.00001)
    aceleretion = 0
    for instant in iter(time):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()

        ax_time.append(instant)
        x_data2.append(p.x * 80 / 1283)
        x_data3.append(aceleretion)

        if int(instant) == instant:
            x_data.append(p.speed * 2 + 5)

            # PLOT 1 (LEFT)
            fig = pylab.figure(
                figsize=[4, 4],
                dpi=100,
            )
            ax = fig.gca()
            ax.plot(x_data)
            ax.set_xlim(0, 10)
            ax.set_ylim(-17, 17)
            canvas = agg.FigureCanvasAgg(fig)
            canvas.draw()
            renderer = canvas.get_renderer()
            raw_data = renderer.tostring_rgb()
            #
            # PLOT 2 (CENTER)
            fig2 = pylab.figure(
                figsize=[4, 4],
                dpi=100,
            )
            ax2 = fig2.gca()
            ax2.plot(ax_time, x_data2)
            ax2.set_xlim(0, 10)
            ax2.set_ylim(0, 100)
            canvas2 = agg.FigureCanvasAgg(fig2)
            canvas2.draw()
            renderer2 = canvas2.get_renderer()
            raw_data2 = renderer2.tostring_rgb()
            # PLOT 3 (RIGHT)
            fig3 = pylab.figure(
                figsize=[4, 4],
                dpi=100,
            )
            ax3 = fig3.gca()
            ax3.plot(ax_time, x_data3)
            ax3.set_xlim(0, 10)
            ax3.set_ylim(-8, 5)
            canvas3 = agg.FigureCanvasAgg(fig3)
            canvas3.draw()
            renderer3 = canvas3.get_renderer()
            raw_data3 = renderer3.tostring_rgb()
            #
            matplotlib.pyplot.close('all')
            height = canvas.get_width_height()
            #
            surf = pygame.image.fromstring(raw_data, height, "RGB")
            surf2 = pygame.image.fromstring(raw_data2, height, "RGB")
            surf3 = pygame.image.fromstring(raw_data3, height, "RGB")
            SCREEN.blit(surf, (0, 220))
            SCREEN.blit(surf2, (1350 / 3, 220))
            SCREEN.blit(surf3, (1350 * 2 / 3, 220))
            pygame.display.flip()
        else:
            sleep(GLOBAL_SPEED)

        if instant < 3:
            aceleretion = 10 / 3.0
        elif 3 <= instant <= 6:
            aceleretion = 0
        elif 6 <= instant <= 10:
            aceleretion = -15 / 2.0

        p.speed += round(aceleretion / 100, 3)
        scenario.update()
    raw_input('press enter to finish ... ')

if __name__ == '__main__':
    simulation()
