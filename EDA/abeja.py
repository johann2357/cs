
def solve(n):
    N = n
    START = (0, 0)

    def valid_move(moves_left, pos):
        return moves_left >= (
            (abs(pos[0]) + 1) / 2 + abs(pos[1]) / 2
        )

    def move_l_up(pos):
        return (pos[0] - 1, pos[1] - 1)

    def move_l_down(pos):
        return (pos[0] - 1, pos[1] + 1)

    def move_up(pos):
        return (pos[0], pos[1] - 2)

    def move_down(pos):
        return (pos[0], pos[1] + 2)

    def move_r_up(pos):
        return (pos[0] + 1, pos[1] - 1)

    def move_r_down(pos):
        return (pos[0] + 1, pos[1] + 1)

    def bt(moves_left, pos):
        if moves_left == 0:
            if START == pos:
                return 1
        moves_left -= 1
        acum = 0

        new_move = move_r_down(pos)
        if valid_move(moves_left, new_move):
            acum += bt(moves_left, new_move)

        new_move = move_l_down(pos)
        if valid_move(moves_left, new_move):
            acum += bt(moves_left, new_move)

        new_move = move_down(pos)
        if valid_move(moves_left, new_move):
            acum += bt(moves_left, new_move)

        new_move = move_l_up(pos)
        if valid_move(moves_left, new_move):
            acum += bt(moves_left, new_move)

        new_move = move_up(pos)
        if valid_move(moves_left, new_move):
            acum += bt(moves_left, new_move)

        new_move = move_r_up(pos)
        if valid_move(moves_left, new_move):
            acum += bt(moves_left, new_move)
        return acum

    return bt(N, START)
