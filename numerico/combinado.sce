//combinado

function [result]=combinado(f, a_b, err)
    new_x = bisection(f, a_b, 0.01);
    if (new_x == 'no result in interval given') then
       mprintf("BISECTION WARNING\n");
       result = new_x;
    else
       mprintf("\n");
       result = secante1(f, new_x, err);
    end
endfunction

function [result]=combinado2(f, a_b, err)
    new_x = false_position(f, a_b, 0.01);
    if (new_x == 'no result in interval given') then
       mprintf("FALSE POSITION WARNING\n");
       result = new_x;
    else
       mprintf("\n");
       result = secante1(f, new_x, err);
    end
endfunction

