function [a, b]=part_euler(f, p, xn, n)
    i = 0;
    h = (xn - p(1)) / n
    a(1) = p(1);
    b(1) = p(2);
    for i=1:n
        a(i + 1) = a(i) + h;
        x=a(i)
        y=b(i)
        b(i+1)=b(i)+h*eval(f);
    end
    plot(a,b,'b')
endfunction

function [a, b]=part_euler_mejorado(f, p, xn, n)
    i = 0;
    h = (xn - p(1)) / n;
    x = p(1)
    y = p(2)
    b1(1) = p(2) + (h) * eval(f);
    a(1) = p(1);
    b(1) = p(2);
    for i=1:n
        x = a(i);
        y = b(i);
        c = eval(f);
        b1(i + 1) = b(i) + (h) * c;
        a(i + 1) = a(i) + h;
        x = a(i+1);
        y = b1(i+1);
        d = eval(f);
        b(i + 1) = b(i) + (h/2) * (c + d);
    end
    plot(a,b,"r")
endfunction

function iter=print_euler(xi, yi, xj, yj)
  iter = size(xi, 1);
  printf('n\t \t EULER \t\t\t\t HEUN \t\n');
  printf('n\t X1\t\t Y1\t\t X2\t\t Y2\t\n');
  for i=1:iter
    printf("%2d  \t%f \t%f \t%f \t%f\n",i,xi(i),yi(i),xj(i),yj(i));
  end
endfunction

function [eul, heun]=euler_comp(f, p, interv, iter)
    if (p(1) <= interv(1))
      [xi, yi] = part_euler(f, p, interv(2), iter);
      plot(xi,yi,'b+')
      [xj, yj] = part_euler_mejorado(f, p, interv(2), iter);
      plot(xj,yj,'r+')
    elseif (p(1) >= interv(2))
      [xi, yi] = part_euler(f, p, interv(1), iter);
      plot(xi,yi,'b+')
      [xj, yj] = part_euler_mejorado(f, p, interv(1), iter);
      plot(xj,yj,'r+')
    else
      [xi1, yi1] = part_euler(f, p, interv(1), iter);
      [xi2, yi2] = part_euler(f, p, interv(2), iter);
      xi = cat(2, xi1, xi2);
      yi = cat(2, yi1, yi2);
      plot(xi,yi,'b+')

      [xi1, yi1] = part_euler_mejorado(f, p, interv(1), iter);
      [xi2, yi2] = part_euler_mejorado(f, p, interv(2), iter);
      xj = cat(2, xi1, xi2);
      yj = cat(2, yi1, yi2);
      plot(xj,yj,'r+')
    end
    eul = [xi, yi];
    heun = [xj, yj];
    print_euler(xi, yi, xj, yj);
endfunction
