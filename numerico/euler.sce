function [a, b]=part_euler(f, p, xn, n)
    i = 0;
    h = (xn - p(1)) / n
    printf('n\t X\t\t Y \t\n');
    printf("%2d  \t%f \t%f\t\n", i, p(1), p(2));
    a(1) = p(1);
    b(1) = p(2);
    for i=1:n
        a(i + 1) = a(i) + h;
        x=a(i)
        y=b(i)
        b(i+1)=b(i)+h*eval(f);
        printf("%2d \t%f \t%f\t\n",i,a(i+1),b(i+1));
    end
    plot(a,b,'b')
endfunction

function [xi, yi]=euler(f, p, interv, iter)
    if (p(1) < interv(1))
      [xi, yi] = part_euler(f, p, interv(2), iter);
      plot(xi,yi,'b')
    elseif (p(1) > interv(2))
      [xi, yi] = part_euler(f, p, interv(1), iter);
      plot(xi,yi,'b')
    else
      [xi1, yi1] = part_euler(f, p, interv(1), iter);
      [xi2, yi2] = part_euler(f, p, interv(2), iter);
      xi = cat(2, xi1, xi2);
      yi = cat(2, yi1, yi2);
      plot(xi,yi,'b')
    end
endfunction
