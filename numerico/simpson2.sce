function y=sumatoria_uno_38(a, n, h, F)
  valor = 0;
  for(i = 1:3:n-2)
    x = a+((i)*h);
    aux=eval(F);
    valor = valor+aux;
  end
  y = valor;
endfunction

function y=sumatoria_dos_38(a, n, h, F)
  valor = 0;
  for(i = 2:3:n-1)
    x = a+((i)*h);
    aux=eval(F);
    valor = valor+aux;
  end
  y = valor;
endfunction

function y=sumatoria_tres_38(a, n, h, F)
  valor = 0;
  t=0;
  for(i = 3:3:n-3)
//      t=multiplo_tres(a);
//    if(t==0)
        x = a+((i)*h);
        tmp=eval(F)
        valor = valor+tmp;
//    end
  end
  y = valor;
endfunction

function y=simpson38(a, b, n, F)
  h = (b-a)/(n);  
  d = (3*h)/8;
  x=a;
  e = eval(F);
  x=b;  
  f = eval(F);  
  g = sumatoria_uno_38(a, n, h, F);
  h = sumatoria_dos_38(a, n, h, F);
  t = sumatoria_tres_38(a,n,h,F);

  y = (d)*(e+f+(3*g)+(3*h)+(2*t));
endfunction
