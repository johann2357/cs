
function y=f(x)
    y = x + 3;
endfunction

function w=test(funcion, vector)
    w = eval(funcion, vector);
endfunction

function [K] = fibonacci(n)
    //function [K] = fibonacci(n)
    //Gives the n-th term of the Fibonacci sequence 0,1,1,2,3,5,8,13,...
    if n==1
      K = 0;
    elseif n==2
      K = 1;
    elseif n>2 & int(n)==n  // check if n is an integer greater than 2
      K = fibonacci(n-1) + fibonacci(n-2);
    else
      disp('error! -- input is not a positive integer');
    end
endfunction



function res=fact(num)
    tmp = 1;
    while num > 1
        tmp = tmp * num;
        num = num - 1;
    end
    res = tmp;
endfunction


function ans=taylor(val, acur)
    absolute = sin(val);
    tmp = 0;
    prev = %inf;
    n = 0;
    while abs(prev-tmp) > acur
        prev = tmp;
        tmp = tmp + (((-1)^n)*(val^(2*n))) / (factorial(2*n + 1));
        disp(tmp);
        n = n + 1;
    end
    ans = tmp;
endfunction

// coseno 
// error de propagacion usando derivadas parciales
//    f('funcion', 'vector con valores', 'vector con variaciones')
//        funcion = '3x + 5y = 3z'
//        vector = [1, 2, 7]
//        variacion = [ 0.01, 0.002, 0.005]























