//ejercicio_1
exec("bisection.sce");
ejercicio_1 = "%e^x*sin(log(x-3))+x^2-1/(x-2)-4";
x = bisection(ejercicio_1, [3.5, 4], 1*10^-8);
eval(ejercicio_1);

//ejercicio_2
ec_1 = "";
ec_2 = "";

//ejercicio_3
exec("bisection.sce");
exec("trapecio.sce");
ejercicio_3 = "cos(x-3)/(x^2-1)+x-1.4-1.58";
punto1 = bisection(ejercicio_3, [-1.001, -0.5], 1*10^-8);
punto2 = bisection(ejercicio_3, [0.5, 1.001], 1*10^-8);
area(ejercicio_3, punto1, punto2, 100);

//ejercicio_4
exec("lagrange.sce");
exec("euler.sce");
xs = [3, 3.2, 3.4, 3.6, 3.8, 4]
ys = [4, 5.1, 1.2, 0.2, 0.01, 0.00023]
ejercicio_4 = lagrange(xs, ys);
euler(ejercicio_4, [1 1.4], [-5 10], 100);
