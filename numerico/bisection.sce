//eq = "x*sin(x)+log(x)/x-3";
//vec = [26, 30];
//err = 0.000001;
//eq1 = "x^2-sin(x-1)-3*x";
//vec1 = [2, 4];
//err1 = 0.001;
//eq2 = "sin(x)/(x-3)";
//vec2 = [3.01, 6];
//eq3 = "x*sin(%e^x)";
//vec3 = [11.38806, 11.38808];
function [result]=bisection(f_expression, a_b, err)
    mprintf('n \t a \t\t b \t\t xn \t\t f(a)*f(xn) \t ER\n');
    //err = err * 5;
    x = a_b(2);
    fxn = evstr(f_expression);
    x = a_b(1);
    fa = evstr(f_expression);
    fafxn = fxn*fa;
    if (fafxn >= 0) then
        result = 'no result in interval given';
    else
        exact_solution = 'notfound';
        i = 0;
        current_error = %inf;
        xr_new =(a_b(2) + a_b(1))/2;
        x = xr_new;
        fxn = evstr(f_expression);
        fafxn = fa * fxn;
        mprintf('%i \t %f \t %f \t %f \t %f \t %f\n', i, a_b(1), a_b(2), xr_new, fafxn, current_error);
        //---------------------------------------------------------------------
        while current_error >= err & exact_solution == 'notfound',
            x = xr_new;
            fxn = evstr(f_expression);
            x = a_b(1);
            fa = evstr(f_expression);
            fafxn = fxn*fa;
            //
            if(fafxn < 0) then
                a_b(2) = xr_new;
            elseif(fafxn > 0) then
                a_b(1) = xr_new;
            elseif(fafxn == 0) then
                result = xr_new;
                exact_solution = 'found';
            end
            //
            xr_old = xr_new;
            xr_new =(a_b(2) + a_b(1))/2;
            current_error = abs(xr_new - xr_old);
            i = i + 1;
            mprintf('%i \t %f \t %f \t %f \t %f \t %f\n', i, a_b(1), a_b(2), xr_new, fafxn, current_error);
            x = xr_new;
            current_error = abs(evstr(f_expression));
        end
        //---------------------------------------------------------------------
        if (exact_solution == 'notfound') then
            mprintf("Result %.10f", xr_new);
            result = xr_new;
        end
    end
endfunction

//eq4 = "x*sin(x)+log(x)/x-3";
//vec4 = [26, 30];
//err4 = 0.000001;
//eq1 = "x^2-sin(x-1)-3*x";
//vec1 = [2, 4];
//err1 = 0.001;
//eq2 = "sin(x)/(x-3)";
//vec2 = [3.01, 6];
//eq3 = "x*sin(%e^x)";
//vec3 = [11.38806, 11.38808];
