eq = "x/(x^2-1)-2";
veca = [-0.8, -0.7];
vecb = [1.2, 1.3];
err = 0.00000001;
//
eq2 = "log(abs(x))/(x^3-1)+x^2-3";
vec2 = [-1.5, 2.00736];
//
vec2a = [-0.1, -0.01];
vec2b = [0.01, 0.1];
vec2c = [1.6, 1.7];
err2 = 0.01;
erra = 0.001;
errb = 0.0001;
errc = 0.00001;
errd = 0.000001;
function [result]=false_position(f_expression, a_b, err)
    mprintf('n \t a \t\t b \t\t xn \t\t f(b)*f(xn) \t ER\n');
    //err = err * 5;
    x = a_b(1);
    fa = evstr(f_expression);
    x = a_b(2);
    fb = evstr(f_expression);
    if (fa*fb >= 0) then
        result = 'no result in interval given';
    else
        exact_solution = 'notfound';
        i = 0;
        current_error = %inf;
        xr_new = a_b(1) - fa * (a_b(2) - a_b(1)) / (fb - fa);
        x = xr_new;
        fxn = evstr(f_expression);
        fbfxn = fb * fxn;
        mprintf('%i \t %f \t %f \t %f \t %f \t %f\n', i, a_b(1), a_b(2), xr_new, fbfxn, current_error);
        //---------------------------------------------------------------------
        while current_error >= err & exact_solution == 'notfound',
            if(fbfxn < 0) then
                a_b(1) = xr_new;
            elseif(fbfxn > 0) then
                a_b(2) = xr_new;
            elseif(fbfxn == 0) then
                result = xr_new;
                exact_solution = 'found';
            end
            current_error = xr_new;
            x = a_b(1);
            fa = evstr(f_expression);
            x = a_b(2);
            fb = evstr(f_expression);
            //
            xr_new = a_b(1) - fa * (a_b(2) - a_b(1)) / (fb - fa);
            x = xr_new;
            fxn = evstr(f_expression);
            fbfxn = fxn*fb;
            current_error = abs(current_error - xr_new);
            //
            i = i + 1;
            mprintf('%i \t %f \t %f \t %f \t %f \t %f\n', i, a_b(1), a_b(2), xr_new, fbfxn, current_error);
            x = xr_new;
            current_error = abs(evstr(f_expression));
        end
        //---------------------------------------------------------------------
        if (exact_solution == 'notfound') then
            mprintf("Result %.10f", xr_new);
            result = xr_new;
        end
    end
endfunction
