function y = integral_superior(a, n, h, F)
  valor = 0;
  i = 0;
  while (i <= n-1)
    x = a + (i * h)
    valor = valor + eval(F);
    i = i + 1;
  end
  y = valor;
endfunction

function y = integral_inferior(a, n, h,F)
  valor = 0;
  i = 1;
  while (i <= n)
    x = a + (i * h);
    valor = valor + eval(F);
    i = i + 1;
  end
  y = valor;
  
endfunction

function y = trapecio(F, a, b, n)
  h = (b - a) / n;
  y = (h*integral_superior(a, n, h, F) + h*integral_inferior(a, n, h, F)) / 2;
endfunction


function y = area(F, a, b, n)
  F = "abs(" + F + ")"
  h = (b - a) / n;
  y = (h*integral_superior(a, n, h, F) + h*integral_inferior(a, n, h, F)) / 2;
endfunction


//trapecio(0,1,10,'x^2')
