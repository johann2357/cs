function J=jacobiant(f1,x,t)
  n=length(x);
  J=zeros(n,n);
  fx=f1(x,t);
  for i=1:n
    x1=x;
    dx=0.00001*x1(i)+1e-10;
    x1(i)=x1(i)+dx;
    J(:,i)=(f1(x1,t)-fx)/dx;
  end
endfunction  

function[] = Newton(n,s)
   
    x = zeros(n,1) 
    xd = zeros(n,1) 
    X = zeros(n,1)  
    Xf = zeros(n,1) 
    Fs = zeros(n,1) 
    F = zeros(n,1) 
    J = zeros(n,n)  
    
    Fs = string(Fs)
   
    for (i=1:n)
        if (i == 1) then
            Fs(i) = //input("Escribe la primera ecuacion: ", 'string')
        else
            Fs(i) = //input("Escribe la siguiente ecuacion: ", 'string')
        end
    end
    
    for (i=1:n)
        X(i)=s(i)
        x(i)=X(i)
    end
    //end
    printf('It.\t\t X\t\t Y    \t\t  Z   \t\t Error  \n')
    c = 0
    e = 0
    printf('%2d \t %11.7f \t %11.7f \t %11.7f \t %11.7f \n',c,x(1),x(2),x(3),e)
    for(i=1:100)
       
        for(i=1:n)
            for(j=1:n)
                xd(j) = x(j) + 0.000000001
                s = "x("+string(j)+")"
                sd = "xd("+string(j)+")"
                J(i,j) = (eval(strsubst(Fs(i), s, sd))- eval(Fs(i)))/0.000000001
            end
         end
           
        Ji = inv(J)
       
        F = eval(Fs)
       
        Xf = X - Ji*F
       
        e = abs(X - Xf)
        for(i=1:n)
            r = e(1)
        end
 
        X = Xf
        x = X
        c = c + 1
        printf('%2d \t %11.7f \t %11.7f \t %11.7f \t %11.7f \n',c,x(1),x(2),x(3),r);
        if (r < 0.00000001) then
            break
        end
    end
   
    for(i=1:n)
        disp("El valor de x(" + string(i)+ ") es " + string(Xf(i)))
    end
endfunction
//[y]=Newton(3,[1.8,2.2,2.6])
//x+y+z=4               x(1)+x(2)+x(3)-4
//x-z=1                   x(1)-x(3)-1
//y-z=0                     x(2)-x(3)
//2 1 1
