function result=derivative_x(f_expression, variable)
    h = 0.00000000001;
    x = variable + h;
    ans = evstr(f_expression);
    x = variable - h;
    ans2 = evstr(f_expression);
    result = (ans - ans2) / (2 * h);
endfunction

//newton("x^4-6*x^2+9", "4*x^3-12*x", 0, 0.0001)

function [result]=newton(f, fd, xn, err)
    x = xn;
    y = evstr(f);
    f1 = y;
    x = 0.0;
    if f1 <> 0 then
        mprintf("n \t xn \t\t error \n");
        for n = 0:100
            f1 = 0.0;
            f2 = 0.0;
            x = xn;
            y = evstr(f);
            f1 = y;
            y = evstr(fd);
            f2 = y;
            //printf("f1:%1.5f \n", f2);
            if f2 == 0.0 then
                x = x + 0.0001; 
                y = evstr(fd);
                f2 = y;
            end                                
            xn = xn - f1 / f2;                
            if n > 0 then
                mprintf("%d \t %f \t %f \n", n, xn, abs(x - xn));
            else
                mprintf("%d \t %f \t - \n", n, xn);
            end
            if abs(x - xn) < err then
                break;
            end
        end
    end
    mprintf("Result %.10f\n", x);
    result = x;
endfunction

// ========================================================================== //
// ========================================================================== //
// ========================================================================== //

function [result]=newton2(f, xn, err)
    x = xn;
    y = evstr(f);
    f1 = y;
    x = 0.0;
    if f1 <> 0 then
        mprintf("n \t xn \t\t error \n");
        for n = 0:100
            f1 = 0.0;
            f2 = 0.0;
            if n > 0 then
                x = xn;
                y = evstr(f);
                f1 = y;
                y = derivative_x(f, x);
                f2 = y;
                //printf("f1:%1.5f \n", f2);
                if f2 == 0 then
                    x = x + 0.0001; 
                    y = derivative_x(f, x);
                    f2 = y;
                end
                xn = xn - f1 / f2;
                mprintf("%d \t %f \t %f \n", n, xn, abs(x - xn));
            else
                mprintf("%d \t %f \t - \n", n, xn);
            end
            if abs(x - xn) < err then
                break;
            end
        end
    end
    mprintf("Result %.10f\n", x);
    result = x;
endfunction

