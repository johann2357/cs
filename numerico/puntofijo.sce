function [result]=puntofijo(f, x, er)
  printf("  n \t        x \t      error \t\n");
  n = 0;
  printf('%3d \t %11.7f \t  \t \t\t\n',n,x);
  
  x = eval(f);
  e = abs(eval(f) - x);
  n = n + 1;
  printf('%3d \t %11.7f \t %11.7f\n',n,x,e);
  
  while(e > er)
    x = eval(f);
    e = abs(eval(f)-x);
    n = n + 1;
    printf('%3d \t %11.7f \t %11.7f\n',n,x,e);
  end
  
  tmp = eval(f);
  mprintf("Result %.10f\n", tmp);
  result = tmp;
endfunction
//puntofijo("(x^2-sin(x-1))/3",3,0.0001)
