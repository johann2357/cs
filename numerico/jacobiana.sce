//JACOBIANA
// g(x, y) = (f1, f2)
//f1(x, y) = x**2 + y**2
//f2(x, y) = 3*x + 4*y

// Jg(x0) = (
//    df1/dx (x0)     df1/dy (x0) 
//    df2/dx (x0)     df2/dy (x0) 
// )
//
// x0 = (x0, y0) .... n dimensional
//
// 3*x^2 
// 6*y
// 7*z
// jacobiana(['3*x^2', '6*y', '7*z'], ['x', 'y', 'z'], [2, 1, 0])
function [result]=partial_deriv(fun, var, vec_vars, vec_vals)
    h = 0.00000001;
    idx = 1;
    for i = vec_vars
        tmp = i + '=' + string(vec_vals(idx));
        execstr(tmp);
        idx = idx + 1;
    end
    execstr(var + '=' + var + '+' + string(h));
    ans = evstr(fun);
    execstr(var + '=' + var + '-' + string(h));
    ans2 = evstr(fun);
    result = (ans - ans2) / (2 * h);
endfunction
//
function result=jacobiana(vec_fun, vec_vars, vec_vals)
    h = 0.00000001;
    idx = 1;
    for i = vec_vars
        tmp = i + '=' + string(vec_vals(idx));
        execstr(tmp);
        idx = idx + 1;
    end
    len = length(vec_vals);
    res = zeros(len,len);
    for j=1:1:len
        for k=1:1:len
            execstr(vec_vars(k) + '=' + vec_vars(k) + '+' + string(h));
            tmp1 = eval(vec_fun(j));
            execstr(vec_vars(k) + '=' + vec_vars(k) + '+' + string(h));
            tmp2 = eval(vec_fun(j));
            res(j, k) = (tmp1 - tmp2) / (2 * h);
            printf(string(((tmp1 - tmp2) / (2 * h))));
            execstr(vec_vars(k) + '=' + string(vec_vals(k)));
        end
    end
    result = res;
endfunction
//
