//secante1("x^2-sin(x-1)-3*x",3,0.00001,0.000001)
//secante2("x^2-sin(x-1)-3*x",3,3.01,0.000001)

//secante1("x^4-6*x^2+9", 0, 0.00001)
//secante2("x^4-6*x^2+9", 0, 1, 0.00001)

function [result]=secante1(F, X0, E1)
    mprintf('n \t xn \t\t err \n');
    err = %inf;
    Xn = 0;
    i = 0;
    h = E1 * 0.1;
    while  err >= E1
        x = X0;
        ini = x;
        f_x0 = evstr(F);
        x = X0 + h;
        f_x1 = evstr(F);
        Xn = ini - ((f_x0 * h) / (f_x1 - f_x0));
        er = abs(Xn - ini);
        mprintf('%2d \t %.7f \t %.7f \n', i, ini, er);
        X0 = Xn;
        err = er;
        i = i + 1;
    end
    mprintf("Result %.10f\n", Xn);
    result = Xn;
endfunction

function [result]=secante2(F, X0, Xp, E1) 
    mprintf('n \t xn \t\t err \n');
    Xn = 0;
    err = %inf;
    i = 0;
    while  err >= E1
        x = X0;    
        ini = x;  
        f_x0 = evstr(F);       
        x = Xp;
        f_x1 = evstr(F);
        Xn = ini - (((X0 - Xp) * (f_x0)) / (f_x0 - f_x1));
        er = abs(Xn - ini);
        mprintf('%2d \t %.7f \t %.7f \n', i, ini, er);
        Xp = X0;
        X0 = Xn;
        err = er;
        i = i + 1;
    end
    mprintf("Result %.10f\n", Xn);
    result = Xn;
endfunction
