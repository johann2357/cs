test = 'x^x+y^x-z^y+log(x*y*z)';
variables = [3, 2, 7];
variations = [0.03, 0.05, 0.001];


// ========================================================================= //
// ===========================1 VARIABLES (x)=============================== //
// ========================================================================= //

function result=partial(f_expression, variables, h)
    x = variables(1) + h;
    ans = evstr(f_expression);
    x = variables(1) - h;
    ans2 = evstr(f_expression);
    result = (ans - ans2) / (2 * h);
endfunction

// ERROR PROP 1 VAR
function result=error_prop(f_expression, variables, variations)
    h = 0.0000000001;
    der = partial(f_expression, variables, h);
    result = abs(der * variations(1));
endfunction

// ========================================================================= //
// ===========================2 VARIABLES (x, y)============================ //
// ========================================================================= //

function result=partial_x(f_expression, variables, h)
    y = variables(2);
    x = variables(1) + h;
    ans = evstr(f_expression);
    x = variables(1) - h;
    ans2 = evstr(f_expression);
    result = (ans - ans2) / (2 * h);
endfunction

function result=partial_y(f_expression, variables, h)
    x = variables(1);
    y = variables(2) + h;
    ans = evstr(f_expression);
    y = variables(2) - h;
    ans2 = evstr(f_expression);
    result = (ans - ans2) / (2 * h);
endfunction


// ERROR PROP 2 VAR
function result=error_prop2(f_expression, variables, variations)
    h = 0.0000000001;
    der = partial_x(f_expression, variables, h);
    acum = abs(der * variations(1));
    der2 = partial_y(f_expression, variables, h);
    acum2 = abs(der2 * variations(2));
    result = acum + acum2;
endfunction

// ========================================================================= //
// =========================3 VARIABLES (x, y, z)=========================== //
// ========================================================================= //

function result=partialx(f_expression, variables, h)
    z = variables(3);
    y = variables(2);
    x = variables(1) + h;
    ans = evstr(f_expression);
    x = variables(1) - h;
    ans2 = evstr(f_expression);
    result = (ans - ans2) / (2 * h);
endfunction

function result=partialy(f_expression, variables, h)
    z = variables(3);
    x = variables(1);
    y = variables(2) + h;
    ans = evstr(f_expression);
    y = variables(2) - h;
    ans2 = evstr(f_expression);
    result = (ans - ans2) / (2 * h);
endfunction

function result=partialz(f_expression, variables, h)
    x = variables(1);
    y = variables(2);
    z = variables(3) + h;
    ans = evstr(f_expression);
    z = variables(3) - h;
    ans2 = evstr(f_expression);
    result = (ans - ans2) / (2 * h);
endfunction


// ERROR PROP 3 VAR
function result=error_prop3(f_expression, variables, variations)
    h = 0.00001;
    der = partialx(f_expression, variables, h);
    acum = abs(der * variations(1));
    der2 = partialy(f_expression, variables, h);
    acum2 = abs(der2 * variations(2));
    der3 = partialz(f_expression, variables, h);
    acum3 = abs(der3 * variations(3));
    result = acum + acum2 + acum3;
endfunction

// ========================================================================= //
// ========================================================================= //
