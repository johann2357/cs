from sympy import diff
import math


safe_list = [
    'acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 'cosh',
    'degrees', 'e', 'exp', 'fabs', 'floor', 'fmod', 'frexp', 'hypot',
    'ldexp', 'log', 'log10', 'modf', 'pi', 'pow', 'radians', 'sin', 'sinh',
    'sqrt', 'tan', 'tanh'
]
safe_math = dict([
    (k, getattr(math, k)) for k in safe_list
])


def error_prop(func, variables, values, variations):
    dd = dict(zip(variables, values))
    acum = 0
    for i in xrange(len(variables)):
        tmp = diff(func, variables[i])
        acum += abs(eval(str(tmp), dd, safe_math) * variations[i])
    return acum


def main():
    func = 'x**x+y**x-z**y+log(x*y*z)'
    variables = ["x", "y", "z", ]
    values = [3, 2, 7, ]
    variations = [0.03, 0.05, 0.001, ]
    print error_prop(func, variables, values, variations)


if __name__ == '__main__':
    main()
