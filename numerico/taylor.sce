function result=to_radians(x)
    result = modulo(x, 360)*%pi/180;
endfunction

function [result]=seno(x, err)
    mprintf('n \t f(x) \t\t ER \t\t EA\n');
    absolute = sin(x);
    f_expression = "(((-1)^n)*(x^(2*n+1)))/(factorial(2*n+1))";
    rel_er = %inf;
    abs_er = %inf;
    acum = 0;
    n = 0;
    while rel_er > err,
        prev = acum;
        acum = acum + evstr(f_expression);
        rel_er = abs(acum - prev);
        abs_er = abs(acum - absolute);
        mprintf('%i \t %f \t %f \t %f\n', n, acum, rel_er, abs_er);
        n = n + 1;
    end
    mprintf('\nreal value: \t%f\n', absolute);
    mprintf('result: \t%f\n', acum);
    result = acum;
endfunction

function [result]=coseno(x, err)
    mprintf('n \t f(x) \t\t ER \t\t EA\n');
    absolute = cos(x);
    f_expression = "(((-1)^n)*(x^(2*n)))/(factorial(2*n))";
    rel_er = %inf;
    abs_er = %inf;
    acum = 0;
    n = 0;
    while rel_er > err,
        prev = acum;
        acum = acum + evstr(f_expression);
        rel_er = abs(acum - prev);
        abs_er = abs(acum - absolute);
        mprintf('%i \t %f \t %f \t %f\n', n, acum, rel_er, abs_er);
        n = n + 1;
    end
    mprintf('\nreal value: \t%f\n', absolute);
    mprintf('result: \t%f\n', acum);
    result = acum;
endfunction
