function y = sum_(V_1, V_2, p, a, b)
    funcprot(0);
    l = "";
    temp = 1;
    for i = a:1:b
        if(i ~= p)
          temp = temp * (V_1(p) - V_1(i));
          if (V_1(i) < 0)
            l = l + "(x+" + string(-V_1(i)) + ")*";
          else
            l = l + "(x-" + string( V_1(i)) + ")*";
          end
        end    
    end 
    l = l + "(" + string(V_2(p)) + "/" + string(temp) + ")";
    y = l;
endfunction
  

// recibe dos vectores V_1, V_2  //
//    retorna un string          //

//          lagrange([1,3,4,6],[2,1,3,-1])


function y = lagrange(V_1, V_2)
    if (length(V_1) ~= length(V_2))
      printf("Los vectores son de tamanho distinto");
      y = "";
    else
      a = 1;
      b = length(V_1);
      l = ""
      for i = a:1:b
          l = l + (sum_(V_1, V_2, i, a, b)) + "+";
      end  
      y = part(l, 1:length(l)-1);
    end
endfunction 
