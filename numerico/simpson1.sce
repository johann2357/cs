function y = sumatoria_cuatro_13(a, n, h, F)
  valor = 0;
  for(i = 1:1:n)
    x = a+(((2*i)-1)*h);
    aux=eval(F)
    valor = valor+aux;
  end
  y = valor;
endfunction

function y = sumatoria_dos_13(a, n, h, F)
  valor = 0;
  for(i = 1:1:n-1)
    x = a+((2*i)*h);
    tmp=eval(F)
    valor = valor+tmp;
  end
  y = valor;
endfunction

function y = simpson13(a, b, n, F)
  h = (b-a)/(2*n);  
  d = h/3;
  //disp(h)
  x=a;
  e = eval(F);
  x=b;  
  f = eval(F);  
  g = sumatoria_cuatro_13(a, n, h, F);
  h = sumatoria_dos_13(a, n, h, F);

  y = (d)*(e+f+(4*g)+(2*h));
endfunction

//metodo_simpson(1,2,3,"x^2")
