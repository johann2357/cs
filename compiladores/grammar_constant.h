
#ifndef COMPILER_GRAMMAR_CONSTANT
#define COMPILER_GRAMMAR_CONSTANT

#include <string>
#include <map>
#include <utility>
#include <vector>

std::map< std::pair<std::string, std::string>, std::vector<std::string> > SYNTACTIC_TABLE {
  {
    std::make_pair("PROGRAM", "NAME"),
    {"BODY","FUNC_DECLARATION"}
  },
  {
    std::make_pair("PROGRAM", "DEF"),
    {"FUNC_DECLARATION"}
  },
  {
    std::make_pair("PROGRAM", "INT"),
    {"BODY","FUNC_DECLARATION"}
  },
  {
    std::make_pair("PROGRAM", "STRING"),
    {"BODY","FUNC_DECLARATION"}
  },
  {
    std::make_pair("PROGRAM", "FLOAT"),
    {"BODY","FUNC_DECLARATION"}
  },
  {
    std::make_pair("PROGRAM", "IF"),
    {"BODY","FUNC_DECLARATION"}
  },
  {
    std::make_pair("PROGRAM", "WHILE"),
    {"BODY","FUNC_DECLARATION"}
  },
  {
    std::make_pair("BODY", "NAME"),
    {"VAR_ASSIGNMENT","EOL","BODY"}
  },
  {
    std::make_pair("BODY", "EOL"),
    {"EOL"}
  },
  {
    std::make_pair("BODY", "DEF"),
    {"FUNC_DECLARATION"}
  },
  {
    std::make_pair("BODY", "INT"),
    {"VAR_DECLARATION","EOL","BODY"}
  },
  {
    std::make_pair("BODY", "STRING"),
    {"VAR_DECLARATION","EOL","BODY"}
  },
  {
    std::make_pair("BODY", "FLOAT"),
    {"VAR_DECLARATION","EOL","BODY"}
  },
  {
    std::make_pair("BODY", "IF"),
    {"IF_EXP","EOL","BODY"}
  },
  {
    std::make_pair("BODY", "WHILE"),
    {"WHILE_EXP","EOL","BODY"}
  },
  {
    std::make_pair("BODY", "$"),
    {"EMPTY"}
  },
  {
    std::make_pair("IND_BODY", "IND"),
    {"IND","IND_BODY_F"}
  },
  {
    std::make_pair("IND_BODY", "$"),
    {"EMPTY"}
  },
  {
    std::make_pair("IND_BODY_F", "NAME"),
    {"VAR_ASSIGNMENT","EOL","IND_BODY"}
  },
  {
    std::make_pair("IND_BODY_F", "INT"),
    {"VAR_DECLARATION","EOL","IND_BODY"}
  },
  {
    std::make_pair("IND_BODY_F", "STRING"),
    {"VAR_DECLARATION","EOL","IND_BODY"}
  },
  {
    std::make_pair("IND_BODY_F", "FLOAT"),
    {"VAR_DECLARATION","EOL","IND_BODY"}
  },
  {
    std::make_pair("IND_BODY_F", "IND"),
    {"IND","IND_BODY_F"}
  },
  {
    std::make_pair("IND_BODY_F", "RETURN"),
    {"RETURN","EXP","EOL"}
  },
  {
    std::make_pair("IND_BODY_F", "IF"),
    {"IF_EXP","EOL","IND_BODY","EOL"}
  },
  {
    std::make_pair("IND_BODY_F", "ELSE"),
    {"EMPTY"}
  },
  {
    std::make_pair("IND_BODY_F", "WHILE"),
    {"WHILE_EXP","EOL","IND_BODY","EOL"}
  },
  {
    std::make_pair("FUNC_DECLARATION", "DEF"),
    {"FUNCTION","FUNC_DECLARATION"}
  },
  {
    std::make_pair("FUNC_DECLARATION", "$"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "INTCONSTANT"),
    {"INTCONSTANT"}
  },
  {
    std::make_pair("FUNC_CALL", "FLOATCONSTANT"),
    {"FLOATCONSTANT"}
  },
  {
    std::make_pair("FUNC_CALL", "STRINGCONSTANT"),
    {"STRINGCONSTANT"}
  },
  {
    std::make_pair("FUNC_CALL", "EOL"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "ASSIGNMENT"),
    {"ASSIGNMENT"}
  },
  {
    std::make_pair("FUNC_CALL", "LPAREN"),
    {"LPAREN","PARAMS","RPAREN"}
  },
  {
    std::make_pair("FUNC_CALL", "RPAREN"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "COMPL"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "COMPLEQ"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "COMPG"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "COMPGEQ"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "COMPEQ"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "OPPLUS"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "OPMINUS"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "OPMULT"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "OPDIVIDE"),
    {"EMPTY"}
  },
  {
    std::make_pair("FUNC_CALL", "$"),
    {"EMPTY"}
  },
  {
    std::make_pair("PARAMS", "INTCONSTANT"),
    {"CONSTANT","PARAMS_F"}
  },
  {
    std::make_pair("PARAMS", "FLOATCONSTANT"),
    {"CONSTANT","PARAMS_F"}
  },
  {
    std::make_pair("PARAMS", "STRINGCONSTANT"),
    {"CONSTANT","PARAMS_F"}
  },
  {
    std::make_pair("PARAMS", "NAME"),
    {"NAME","PARAMS_F"}
  },
  {
    std::make_pair("PARAMS", "RPAREN"),
    {"EMPTY"}
  },
  {
    std::make_pair("PARAMS_F", "RPAREN"),
    {"EMPTY"}
  },
  {
    std::make_pair("PARAMS_F", "COMMA"),
    {"COMMA","PARAMS"}
  },
  {
    std::make_pair("FUNCTION", "DEF"),
    {"DEF","TYPE","NAME","LPAREN","ARGS_NOARGS","RPAREN","COLON","EOL","IND_BODY"}
  },
  {
    std::make_pair("ARGS_NOARGS", "INT"),
    {"ARGS"}
  },
  {
    std::make_pair("ARGS_NOARGS", "STRING"),
    {"ARGS"}
  },
  {
    std::make_pair("ARGS_NOARGS", "FLOAT"),
    {"ARGS"}
  },
  {
    std::make_pair("ARGS_NOARGS", "RPAREN"),
    {"EMPTY"}
  },
  {
    std::make_pair("ARGS", "INT"),
    {"TYPE","NAME","ARGS_F"}
  },
  {
    std::make_pair("ARGS", "STRING"),
    {"TYPE","NAME","ARGS_F"}
  },
  {
    std::make_pair("ARGS", "FLOAT"),
    {"TYPE","NAME","ARGS_F"}
  },
  {
    std::make_pair("ARGS_F", "RPAREN"),
    {"EMPTY"}
  },
  {
    std::make_pair("ARGS_F", "COMMA"),
    {"COMMA","ARGS"}
  },
  {
    std::make_pair("IF_EXP", "IF"),
    {"IF","EXP","COLON","EOL","IND_BODY","ELSE_EXP"}
  },
  {
    std::make_pair("ELSE_EXP", "EOL"),
    {"EMPTY"}
  },
  {
    std::make_pair("ELSE_EXP", "ELSE"),
    {"ELSE","COLON","EOL","IND_BODY"}
  },
  {
    std::make_pair("WHILE_EXP", "WHILE"),
    {"WHILE","EXP","COLON","EOL","IND_BODY"}
  },
  {
    std::make_pair("VAR_ASSIGNMENT", "NAME"),
    {"NAME","ASSIGNMENT","EXP"}
  },
  {
    std::make_pair("VAR_DECLARATION", "INT"),
    {"TYPE","VAR_DECLARATION_F"}
  },
  {
    std::make_pair("VAR_DECLARATION", "STRING"),
    {"TYPE","VAR_DECLARATION_F"}
  },
  {
    std::make_pair("VAR_DECLARATION", "FLOAT"),
    {"TYPE","VAR_DECLARATION_F"}
  },
  {
    std::make_pair("VAR_DECLARATION_F", "NAME"),
    {"NAME","ASSIGNMENT","EXP"}
  },
  {
    std::make_pair("VAR_DECLARATION_F", "COMMA"),
    {"NAMES"}
  },
  {
    std::make_pair("NAMES", "NAME"),
    {"NAME"}
  },
  {
    std::make_pair("NAMES", "COMMA"),
    {"NAMES_F"}
  },
  {
    std::make_pair("NAMES_F", "EOL"),
    {"EMPTY"}
  },
  {
    std::make_pair("NAMES_F", "COMMA"),
    {"COMMA","NAMES"}
  },
  {
    std::make_pair("TYPE", "INT"),
    {"INT"}
  },
  {
    std::make_pair("TYPE", "STRING"),
    {"STRING"}
  },
  {
    std::make_pair("TYPE", "FLOAT"),
    {"FLOAT"}
  },
  {
    std::make_pair("EXP", "INTCONSTANT"),
    {"CONSTANT"}
  },
  {
    std::make_pair("EXP", "FLOATCONSTANT"),
    {"CONSTANT"}
  },
  {
    std::make_pair("EXP", "STRINGCONSTANT"),
    {"CONSTANT"}
  },
  {
    std::make_pair("EXP", "NAME"),
    {"NAME","FUNC_CALL"}
  },
  {
    std::make_pair("EXP", "NOT"),
    {"NOT","EXP"}
  },
  {
    std::make_pair("EXP", "LPAREN"),
    {"LPAREN","EXP","OPERATOR","EXP","RPAREN"}
  },
  {
    std::make_pair("OPERATOR", "COMPL"),
    {"COMPL"}
  },
  {
    std::make_pair("OPERATOR", "COMPLEQ"),
    {"COMPLEQ"}
  },
  {
    std::make_pair("OPERATOR", "COMPG"),
    {"COMPG"}
  },
  {
    std::make_pair("OPERATOR", "COMPGEQ"),
    {"COMPGEQ"}
  },
  {
    std::make_pair("OPERATOR", "COMPEQ"),
    {"COMPEQ"}
  },
  {
    std::make_pair("OPERATOR", "OPPLUS"),
    {"OPPLUS"}
  },
  {
    std::make_pair("OPERATOR", "OPMINUS"),
    {"OPMINUS"}
  },
  {
    std::make_pair("OPERATOR", "OPMULT"),
    {"OPMULT"}
  },
  {
    std::make_pair("OPERATOR", "OPDIVIDE"),
    {"OPDIVIDE"}
  },
  {
    std::make_pair("CONSTANT", "INTCONSTANT"),
    {"INTCONSTANT"}
  },
  {
    std::make_pair("CONSTANT", "FLOATCONSTANT"),
    {"FLOATCONSTANT"}
  },
  {
    std::make_pair("CONSTANT", "STRINGCONSTANT"),
    {"STRINGCONSTANT"}
  }
};

#endif
