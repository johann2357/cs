#ifndef COMPILER_TOKENIZER
#define COMPILER_TOKENIZER

#include <string>
#include <vector>
#include <stdio.h>
#include "debug.h"
#include "constants.h"
#include "utils.h"

struct Tokenizer {

  /**
   * Check if it is an string
   * Check if first char or last char is a quote
  **/
  bool check_string(std::string s) {
    return (
      (s.front() == '"') &&
      (s.back() == '"')
    );
  }

  /**
   * Check if it is a number
  **/
  bool check_number(std::string s) {
    return std::all_of(s.begin(), s.end(), ::isdigit);
  }

  /**
   * Check if it is a float
  **/
  bool check_float(std::string s) {
    std::vector<std::string> tmp = split(s, '.');
    if (tmp.size() != 2) {
      return false;
    }
    if (tmp[1].back() != 'f') {
      return false;
    }
    tmp[1] = tmp[1].substr(0, tmp[1].length() - 1);
    return (
      this->check_number(tmp[0]) && this->check_number(tmp[1])
    );
  }

  /**
   * Check if it can be a valid name for a function or variable
  **/
  bool check_name(std::string s) {
    if (
        s.substr(0, 1).find_first_of(
          VALID_FIRST_CHAR_NAME.c_str()
        ) == std::string::npos) {
      return false;
    }
    for (int i = 1; i < s.length(); ++i) {
      if (
          s.substr(i, 1).find_first_of(
            VALID_NAME.c_str()
          ) == std::string::npos) {
        return false;
      }
    }
    return true;
  }

  /**
   * Get the corresponding token
   * UNKNOWN if no token is match (ERROR)
  **/
  std::string get_token(std::string s) {
    std::string token;
    if (s.compare(0, EOL.length(), EOL) == 0) {
      token = "EOL";
    } else if (s.compare(0, IND.length(), IND) == 0) {
      token = "IND";
    } else if (s.compare(0, NOT.length(), NOT) == 0) {
      token = "NOT";
    } else if (s.compare(0, DEF.length(), DEF) == 0) {
      token = "DEF";
    } else if (s.compare(0, INT.length(), INT) == 0) {
      token = "INT";
    } else if (s.compare(0, STRING.length(), STRING) == 0) {
      token = "STRING";
    } else if (s.compare(0, FLOAT.length(), FLOAT) == 0) {
      token = "FLOAT";
    } else if (s.compare(0, RETURN.length(), RETURN) == 0) {
      token = "RETURN";
    } else if (s.compare(0, MAIN_FUNC.length(), MAIN_FUNC) == 0) {
      token = "NAME";
    } else if (s.compare(0, COLON.length(), COLON) == 0) {
      token = "COLON";
    } else if (s.compare(0, OPPLUS.length(), OPPLUS) == 0) {
      token = "OPPLUS";
    } else if (s.compare(0, OPMINUS.length(), OPMINUS) == 0) {
      token = "OPMINUS";
    } else if (s.compare(0, OPMULT.length(), OPMULT) == 0) {
      token = "OPMULT";
    } else if (s.compare(0, OPDIVIDE.length(), OPDIVIDE) == 0) {
      token = "OPDIVIDE";
    } else if (s.compare(0, COMPLEQ.length(), COMPLEQ) == 0) {
      token = "COMPLEQ";
    } else if (s.compare(0, COMPGEQ.length(), COMPGEQ) == 0) {
      token = "COMPGEQ";
    } else if (s.compare(0, COMPEQ.length(), COMPEQ) == 0) {
      token = "COMPEQ";
    } else if (s.compare(0, COMPL.length(), COMPL) == 0) {
      token = "COMPL";
    } else if (s.compare(0, COMPG.length(), COMPG) == 0) {
      token = "COMPG";
    } else if (s.compare(0, ASSIGNMENT.length(), ASSIGNMENT) == 0) {
      token = "ASSIGNMENT";
    } else if (s.compare(0, IF.length(), IF) == 0) {
      token = "IF";
    } else if (s.compare(0, ELSE.length(), ELSE) == 0) {
      token = "ELSE";
    } else if (s.compare(0, WHILE.length(), WHILE) == 0) {
      token = "WHILE";
    } else if (s.compare(0, RPAREN.length(), RPAREN) == 0) {
      token = "RPAREN";
    } else if (s.compare(0, LPAREN.length(), LPAREN) == 0) {
      token = "LPAREN";
    } else if (s.compare(0, COMMA.length(), COMMA) == 0) {
      token = "COMMA";
    } else if (this->check_number(s)) {
      token = "INTCONSTANT";
    } else if (this->check_float(s)) {
      token = "FLOATCONSTANT";
    } else if (this->check_name(s)) {
      token = "NAME";
    } else if (this->check_string(s)) {
      token = "STRINGCONSTANT";
    } else {
      token = "UNKNOWN";
      LOG("\nERROR: unknown lexem %s\n", s.c_str());
    }
    return token;
  }

  /**
   * After reduce and trim
   * Separate every lexem correctly
  **/
  std::vector<std::string> split_into_lexems(
      std::string &s, std::string &tokens) {
    std::vector<std::string> result = split(s, tokens[0]);
    for (int i = 1; i < tokens.size(); ++i) {
      std::vector<std::string> new_result;
      for(
            std::vector<std::string>::iterator it = result.begin();
            it != result.end();
            ++it
          ) {
        std::vector<std::string> tmp = split(
          (*it),
          tokens[i]
        );
        new_result.insert(new_result.end(), tmp.begin(), tmp.end());
      }
      result = new_result;
    }
    return this->split_into_lexems_fixer(result);
  }

  /**
   * By default strings and <= >= == are separated
   * Gather the those into one lexem
  **/
  std::vector<std::string> split_into_lexems_fixer(
      std::vector<std::string> &v) {
    std::vector<std::string> result;
    std::string tmp;
    bool is_string = false;
    for (int i = 0; i < v.size(); ++i) {
      // check for string
      if (
          v[i].substr(0, 1).find_first_of(
            COMPOUND_COMPARISON_OPERATOR.c_str()
          ) != std::string::npos) {
        if (v[i + 1].front() == '=') {
          result.push_back(v[i] + v[i + 1]);
          ++i;
        } else {
          result.push_back(v[i]);
        }
      } else if (v[i].front() == '"') {
        tmp = std::string(v[i].c_str());
        is_string = true;
        if (v[i].back() == '"') {
          result.push_back(tmp);
          is_string = false;
        }
      } else if (is_string) {
        tmp.append(" " + v[i]);
        if (v[i].back() == '"') {
          is_string = false;
          result.push_back(tmp);
        }
      } else {
        result.push_back(v[i]);
      }
    }
    return result;
  }

  /**
   * Transform all the lexems into tokens
  **/
  std::vector<std::string> split_into_tokens(
      std::vector<std::string> lexem_vector) {
    std::vector<std::string> result;
    for(
        std::vector<std::string>::iterator it = lexem_vector.begin();
        it != lexem_vector.end(); ++it
       ) {
        result.push_back(this->get_token(*it));
    }
    return result;
  }
};

#endif // COMPILER_TOKENIZER
