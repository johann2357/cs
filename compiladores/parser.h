#ifndef COMPILER_PARSER
#define COMPILER_PARSER

#include <algorithm>
#include <string>
#include <vector>
#include <stack>
#include <stdio.h>
#include "debug.h"
#include "constants.h"
#include "scanner.h"
#include "utils.h"

struct Parser {
  TOKEN_VECTOR tokens;
  std::string file;
  PARSER_TABLE parser_table;

  Parser() {
  }

  Parser(std::string file_) {
    this->file = file_;
  }

  int run(std::string file_) {
    this->file = file_;
    return this->run();
  }
  
  int run() {
    this->run_scanner();
    if (this->run_checker() != -1) {
      LOG("SUCCESS: Parsing OK without errors\n");
      return 0;
    }
    return -1;
  }

  int run_checker() {

    // fill code stack
    std::stack<std::string> code_stack;
    for (int i = tokens.size() - 1; i >= 0; --i) {
      code_stack.push(tokens[i]);
    }
    
    // fill checker stack
    std::stack<std::string> checker_stack;
    checker_stack.push("PROGRAM");

    // production and token variables
    std::string production;
    std::string look_for;

    bool new_token = true;
    int status = 1;

    while ((! code_stack.empty()) && (! checker_stack.empty())) {
      production = checker_stack.top();
      checker_stack.pop();
      if (production == "EMPTY") {
        production = checker_stack.top();
        checker_stack.pop();
      }
      if (new_token) {
        code_stack.pop();
        if (code_stack.empty()) {
          return status;
        }
        look_for = code_stack.top();
        new_token = false;
      }
      DEBUG(
        "                                       (%s, %s)\n",
        production.c_str(),
        look_for.c_str()
      );
      if (! in(production, TERMINALS)) {
        std::vector<std::string> tmp = SYNTACTIC_TABLE[
          std::make_pair(production, look_for)];
        if (tmp.empty() && (! checker_stack.empty())) {
          LOG(
            "ERROR: < %s > expected, < %s > found \n",
            production.c_str(),
            look_for.c_str()
          );
          status = -1;
        }
        for (int j = tmp.size() - 1; j >= 0; --j) {
          checker_stack.push(tmp[j]);
        }
      } else {
        DEBUG(
          "                                    >>>>> %s >>>>> %s\n",
          look_for.c_str(),
          production.c_str()
        );
        if (production != look_for) {
          LOG(
            "ERROR: < %s > expected, < %s > found \n",
            production.c_str(),
            look_for.c_str()
          );
          status = -1;
        }
        new_token = true;
      }
    }
    return status;
  }

  void run_scanner() {
    Scanner scanner(this->file);
    scanner.init();
    LEXICAL_TABLE v = scanner.lexem_table;
    LEXICAL_TABLE v2 = scanner.token_table;
    INDENTATION_MAP indentation = scanner.indentation_map;
    DEBUG("\n\n===\nLEXEM\t\t\tTOKEN\t\t\tLINE\t\t\tINDENTATION LEVEL\n");
    for(int it = 0; it < v.size(); ++it) {
      for(int it2 = 0; it2 < v[it].size(); ++it2) {
        parser_table.push_back({
          v[it][it2].c_str(),
          v2[it][it2].c_str(),
          it,
          indentation[it]
        });
        DEBUG(
          "%-20s\t%-24s%d\t\t\t%d\n",
          v[it][it2].c_str(),
          v2[it][it2].c_str(),
          it,
          indentation[it]
        );
        this->tokens.push_back(v2[it][it2].c_str());
      }
    }
    DEBUG("===\n");
  }
};

#endif // COMPILER_PARSER
