#ifndef COMPILER_DEBUG
#define COMPILER_DEBUG

#include <stdio.h>

// LOG LEVEL
//#define DEBUG_LEVEL
#define LOG_LEVEL

typedef int (*printf_alias)(const char*, ...);

#ifdef LOG_LEVEL
printf_alias LOG = printf;
#else
#define LOG(...)
#endif

#ifdef DEBUG_LEVEL
printf_alias DEBUG = printf;
#else
#define DEBUG(...)
#endif

#endif
