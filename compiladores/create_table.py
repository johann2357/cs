GRAMMAR = 'std::map< std::pair<std::string, std::string>, std::vector<std::string> > SYNTACTIC_TABLE {\n'
GRAMMAR_FILE = """
#ifndef COMPILER_GRAMMAR_CONSTANT
#define COMPILER_GRAMMAR_CONSTANT

#include <string>
#include <map>
#include <utility>
#include <vector>

%s

#endif
"""

with open('grammar.csv') as f:
    headers = f.readline()
    headers = headers.strip()
    headers = headers.split(',')[1:]

    result = []
    for line in f:
        row = line.split(',')
        row[-1] = row[-1].strip()

        id_row = row[0]
        row = row[1:]

        for idx, elem in enumerate(row):
            if elem:
                tmp = '  {\n'
                tmp += '    std::make_pair("%s", "%s"),\n' % (
                    id_row, headers[idx])
                tmp += '    {'
                tmp += ','.join([
                    str('"' + token + '"') for token in elem.split()
                ])
                tmp += '}\n'
                tmp += '  }'
                result.append(tmp)

    GRAMMAR += ',\n'.join(elm for elm in result)

    GRAMMAR += '\n};'


with open('grammar_constant.h', 'w') as gf:
    gf.write(GRAMMAR_FILE % (GRAMMAR, ))
