#ifndef COMPILER_SCANNER
#define COMPILER_SCANNER

#include <algorithm>
#include <string>
#include <vector>
#include <stdio.h>
#include "constants.h"
#include "reader.h"
#include "tokenizer.h"
#include "types.h"
#include "utils.h"

struct Scanner {
  INDENTATION_MAP indentation_map;
  LINES_VECTOR lines;
  LINES_VECTOR lines_no_spaces;
  LINES_VECTOR lines_no_comments;
  LEXEM_TABLE lexem_table;
  LEXEM_TABLE token_table;
  LEXICAL_TABLE lexical_table;
  Tokenizer tokenizer;

  Scanner(std::string file) {
    this->lines = reader(file);
  }

  void init() {
    this->tokenizer_magic();
  }

  int count_spaces(std::string line) {
    return line.find_first_not_of(" ");
  }

  void tokenizer_magic() {
    for (int i = 0; i < this->lines.size(); ++i) {
      this->indentation_map[i] = this->count_spaces(this->lines[i]);
      std::string tmp = this->lines[i];
      for (int j = 1; j < SPLIT_TOKENS.size(); ++j) {
        size_t index = 0;
        while (true) {
          /* Locate the substring to replace. */
          index = tmp.find(SPLIT_TOKENS[j], index);
          if (index == std::string::npos) break;

          /* Make the replacement. */
          std::string new_substr = std::string(" ") + SPLIT_TOKENS[j] + std::string(" ");
          tmp.replace(index, 1, new_substr.c_str());

          /* Advance index forward so the next iteration doesn't pick it up as well. */
          index += 3;
        }
      }
      std::string to_reduce;
      for (int k = 0; k < (this->indentation_map[i] / 4); ++k) {
        to_reduce += "__ind__ ";
      }
      to_reduce += tmp;
      to_reduce += " __eol__";
      to_reduce = reduce(to_reduce.c_str());

      if (to_reduce == "__eol__") {
        to_reduce = "";
      }

      this->lines_no_spaces.push_back(to_reduce);
      std::vector<std::string> lexems = tokenizer.split_into_lexems(
        lines_no_spaces[i],
        SPLIT_TOKENS
      );
      this->lexem_table.push_back(lexems);
      this->token_table.push_back(tokenizer.split_into_tokens(lexems));
    }
  }
};

#endif // COMPILER_SCANNER
