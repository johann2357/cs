#ifndef COMPILER_UTILS
#define COMPILER_UTILS

#include <algorithm>
#include <string>
#include <sstream>

/**
 * Trim the string
 * Delete spaces from at the beginning and at the end
**/
std::string trim(
    const std::string& str,
    const std::string& whitespace = " \t"
  ) {
  const auto strBegin = str.find_first_not_of(whitespace);
  if (strBegin == std::string::npos) {
    return ""; // no content
  }
  const auto strEnd = str.find_last_not_of(whitespace);
  const auto strRange = strEnd - strBegin + 1;
  return str.substr(strBegin, strRange);
}

/**
 * Delete the extra spaces
 * Return string with single spacing
**/
std::string reduce(
    const std::string& str,
    const std::string& fill = " ",
    const std::string& whitespace = " \t"
  ) {
  // trim first
  auto result = trim(str, whitespace);
  // replace sub ranges
  auto beginSpace = result.find_first_of(whitespace);
  while (beginSpace != std::string::npos) {
    const auto endSpace = result.find_first_not_of(whitespace, beginSpace);
    const auto range = endSpace - beginSpace;
    result.replace(beginSpace, range, fill);
    const auto newStart = beginSpace + fill.length();
    beginSpace = result.find_first_of(whitespace, newStart);
  }
  return result;
}

/**
 * Used by split
 * Split an string by a delim char into a vector
 * Receive a vector as a parameter
**/
std::vector<std::string> &split(
    const std::string &s, char delim, std::vector<std::string> &elems) {
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    if (item.empty()) {
      elems.push_back(std::string(&delim).c_str());
    } else {
      elems.push_back(item);
    }
  }
  return elems;
}

/**
 * Used by split
 * Split an string by a delim char into a vector
**/
std::vector<std::string> split(const std::string &s, char delim) {
  std::vector<std::string> elems;
  split(s, delim, elems);
  return elems;
}

/**
 * Check if s <string> is in vec <string vector>
**/
bool in(std::string s, std::vector<std::string> &vec) {
  return (
    std::find(vec.begin(), vec.end(), s.c_str()) != vec.end()
  );
}

#endif // COMPILER_UTILS
