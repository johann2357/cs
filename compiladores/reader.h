#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include "debug.h"
#include "types.h"

/**
 * Just read the file and put every line in a vector
**/
LINES_VECTOR reader(std::string fn) {
  LINES_VECTOR lines;
  std::ifstream file(fn.c_str(), std::ifstream::in);
  std::string line; 
  while (std::getline(file, line)) {
    std::istringstream iss(line);
    lines.push_back(line);
    DEBUG("%s\n", line.c_str());
  }
  return lines;
}
