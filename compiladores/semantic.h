#ifndef COMPILER_SEMANTIC
#define COMPILER_SEMANTIC

#include <string>
#include "parser.h"
#include "types.h"
#include "utils.h"
#include "debug.h"
#include "constants.h"


struct Semantic {
  std::string file;
  Parser parser;
  SEMANTIC_TABLE semantic_table;

  Semantic(std::string file_) {
    this->file = file_;
  }
  
  int run() {
    if (this->parser.run(this->file) == 0) {
      if (this->run_analyzer() == 0) {
        LOG("SUCCESS: Semantic Analysis OK without errors\n");
        return 0;
      }
    } else {
      LOG("ERROR: Please fix the parsing errors\n");
    }
    return -1;
  }

  bool is_definition(int name_location) {
    if (name_location > 0) {
      return (
        TYPES.count(this->parser.parser_table[name_location - 1].token) != 0
      );
    }
    return false;
  }

  std::string get_type(int name_location) {
    return this->parser.parser_table[name_location - 1].token;
  }

  bool is_function(int name_location) {
    if (name_location > 1) {
      return (
        this->parser.parser_table[name_location - 2].token == "DEF"
      );
    }
    return false;
  }

  bool is_defined(std::string name, int scope) {
    int count = 0;
    if (scope != 0) {
      count += this->semantic_table.count({name, 0});
    }
    return (
       (this->semantic_table.count({name, scope}) + count) != 0
    );
  }

  void define(int name_location, int scope) {
    PARSER_TABLE_ENTRY entry, prev_entry;
    bool is_func = this->is_function(name_location);
    std::vector<std::string> params;
    if (is_func) {
      int end_scope = name_location;
      do {
        ++end_scope;
        entry = this->parser.parser_table[end_scope];
        prev_entry = this->parser.parser_table[end_scope - 1];
        if (entry.token == "NAME") {
          this->semantic_table[{entry.lexem, scope}] = {
            prev_entry.token,
            entry.line,
            false,
            {}
          };
          params.push_back(prev_entry.token);
        }
      } while (entry.token != "EOL");
      scope = 0;
    }
    entry = this->parser.parser_table[name_location];
    prev_entry = this->parser.parser_table[name_location - 1];
    this->semantic_table[{entry.lexem, scope}] = {
      prev_entry.token,
      entry.line,
      is_func,
      params
    };
  }

  SEMANTIC_TABLE_ENTRY get_var_attrs(std::string name, int scope) {
    if (this->semantic_table.count({name, scope})) {
      return this->semantic_table[{name, scope}];
    }
    return this->semantic_table[{name, 0}];
  }

  std::string get_definition_type(std::string name, int scope) {
    return this->get_var_attrs(name, scope).type;
  }

  int check_assignment(int name_location, int end, int scope) {
    PARSER_TABLE_ENTRY entry, next_entry;
    for (int i = name_location + 1; i < end; ++i) {
      entry = this->parser.parser_table[i];
      if (entry.token == "NAME") {
        if (
            ! (
              (this->is_defined(entry.lexem, scope)) ||
              (this->is_defined(entry.lexem, 0))
            )
        ) {
          LOG(
            "ERROR:statement %d: <%s> was not declared\n",
            entry.line,
            entry.lexem.c_str()
          );
          return -1;
        }

        if (
          this->get_definition_type(entry.lexem, scope) !=
          this->get_definition_type(
            this->parser.parser_table[name_location].lexem, scope)
        ) {
          LOG(
            "ERROR:statement %d: <%s> type does not match <%s> type\n",
            entry.line,
            entry.lexem.c_str(),
            this->parser.parser_table[name_location].lexem.c_str()
          );
          return -1;
        }

        SEMANTIC_TABLE_ENTRY s_entry = this->get_var_attrs(entry.lexem, scope);
        if (s_entry.is_function) {
          next_entry = this->parser.parser_table[i + 1];
          if (next_entry.token != "LPAREN") {
            LOG(
              "ERROR:statement %d: <%s> is a function and it must be called\n",
              entry.line,
              entry.lexem.c_str()
            );
            return -1;
          }
          int k = 0;
          do {
            ++i;
            next_entry = this->parser.parser_table[i];
            // TODO: CHECK PARAMS OF A FUNCTION WHEN IT IS CALLED
            if (k < s_entry.params.size()) {
              if (next_entry.token == "NAME") {
                SEMANTIC_TABLE_ENTRY tmp_entry = this->get_var_attrs(
                  next_entry.lexem,
                  scope
                );
                if (tmp_entry.is_function) {
                  LOG(
                    "ERROR:statement %d: in <%s> expected %s got function %s instead\n",
                    entry.line,
                    entry.lexem.c_str(),
                    s_entry.params[k].c_str(),
                    next_entry.lexem.c_str()
                  );
                  return -1;
                }
                if (tmp_entry.type != s_entry.params[k]) {
                  LOG(
                    "ERROR:statement %d: in <%s> expected %s got %s\n",
                    entry.line,
                    entry.lexem.c_str(),
                    s_entry.params[k].c_str(),
                    tmp_entry.type.c_str()
                  );
                  return -1;
                }
              // if it is calling a function
              // if it is a variable
              } else if (CONSTANTS.count(next_entry.token) != 0) {
                if (CONSTANT_TYPE[next_entry.token] != s_entry.params[k]) {
                  LOG(
                    "ERROR:statement %d: in <%s> expected %s got %s\n",
                    entry.line,
                    entry.lexem.c_str(),
                    s_entry.params[k].c_str(),
                    CONSTANT_TYPE[next_entry.token].c_str()
                  );
                  return -1;
                }
              }
            }
            if ((PARAMS.count(next_entry.token) != 0)) {
              ++k;
            }
          } while ((i < end) && (next_entry.token != "RPAREN"));
          if (k != s_entry.params.size()) {
            LOG(
              "ERROR:statement %d: <%s> takes %d argument(s) %d given\n",
              entry.line,
              entry.lexem.c_str(),
              s_entry.params.size(),
              k
            );
            return -1;
          }
        } else {
          next_entry = this->parser.parser_table[i + 1];
          if (next_entry.token == "LPAREN") {
            LOG(
              "ERROR:statement %d: <%s> is not a function\n",
              entry.line,
              entry.lexem.c_str()
            );
            return -1;
          }
        }

      // CHECK CONSTANTS
      } else if (CONSTANTS.count(entry.token) != 0) {
        std::string type;
        if (this->is_definition(name_location)) {
          type = this->get_type(name_location);
        } else {
          type = this->get_definition_type(
            this->parser.parser_table[name_location].lexem, scope
          );
        }
        if (CONSTANT_TYPE[entry.token] != type) {
          LOG(
            "ERROR:statement %d: <%s> does not match <%s> type\n",
            entry.line,
            entry.lexem.c_str(),
            this->parser.parser_table[name_location].lexem.c_str()
            //type.c_str()
          );
          return -1;
        }
      }
    }
    return 0;
  }

  int check_return(int name_location, std::string type) {
    PARSER_TABLE parser_table = this->parser.parser_table;
    while (parser_table[name_location].token != "DEF") {
      --name_location;
    }
    return (parser_table[name_location + 1].token == type);
  }

  int run_analyzer() {
    PARSER_TABLE parser_table = this->parser.parser_table;
    int status = 0;
    int table_size = parser_table.size();
    int scope = 0;
    int start_scope = 0;
    int end_scope = table_size - 1;
    for (int i = 0; i < table_size; ++i) {
      int name_location = i;
      if (parser_table[name_location].token == "EOL") {
        start_scope = i + 1;
        end_scope = i + 1;
      } else if (parser_table[name_location].token == "DEF") {
        ++scope;
      } else if (CONSTANTS.count(parser_table[name_location].token) != 0) {
        if (parser_table[name_location - 1].token == "RETURN") {
          if (! check_return(
              name_location - 2,
              CONSTANT_TYPE[parser_table[name_location].token])) {
            LOG(
              "ERROR:statement %d: return type %s does not match function\n",
              parser_table[name_location].line,
              parser_table[name_location].token.c_str()
              //name.c_str()
            );
            status = -1;
          }
        }
      } else if (parser_table[name_location].token == "NAME") {
        end_scope = name_location + 1;
        while (parser_table[end_scope].token != "EOL") {
          ++end_scope;
        }
        i = end_scope;

        // CHECK DEFINITION AND REDEFINITION
        std::string type;
        std::string name = parser_table[name_location].lexem;
        if (this->is_definition(name_location)) {
          type = this->get_type(name_location);
          if (! this->is_defined(name, scope)) {
            this->define(name_location, scope);
          } else {
            LOG(
              "ERROR:statement %d: %s is already defined\n",
              parser_table[name_location].line,
              name.c_str()
            );
            status = -1;
          }
          // CHECK ASSIGNMENT
          if (!this->is_function(name_location)) {
            if (this->check_assignment(name_location, end_scope, scope) != 0) {
              status = -1;
            }
          }
        } else {
          // get from table
          if (! this->is_defined(name, scope)) {
            LOG(
              "ERROR:statement %d: %s was not declared\n",
              parser_table[name_location].line,
              name.c_str()
            );
            status = -1;
          } else {
            type = this->get_definition_type(
              parser_table[name_location].lexem,
              scope
            );
          }
          // CHECK ASSIGNMENT
          if (this->check_assignment(name_location, end_scope, scope) != 0) {
            status = -1;
          }
        }
        if (parser_table[name_location - 1].token == "RETURN") {
          if (! check_return(name_location - 2, type)) {
            LOG(
              "ERROR:statement %d: return type %s does not match function\n",
              parser_table[name_location].line,
              name.c_str()
            );
            status = -1;
          }
        }


        DEBUG(">>> %s <<<\n", parser_table[name_location].lexem.c_str());

        start_scope = end_scope + 1;
        i = end_scope;
      }
    }
    DEBUG("!!\n");
    this->print_table();
    return status;
  }

  void print_table() {
    DEBUG("<NAME, SCOPE> \t-> { TYPE, LINE, IS_FUNCTION, [PARAMS] }\n");
    for(
        auto it = this->semantic_table.begin();
        it != this->semantic_table.end();
        ++it
    ) {
      DEBUG(
        "<%s, %d> \t-> { %s, %d, %d, [",
        it->first.first.c_str(),
        it->first.second,
        it->second.type.c_str(),
        it->second.line,
        it->second.is_function
      );
      if (it->second.is_function) {
        for (int i = 0; i < it->second.params.size(); ++i) {
          DEBUG("%s,", it->second.params[i].c_str());
        }
      }
      DEBUG("] }\n");
    }
  }
};

#endif // COMPILER_SEMANTIC
