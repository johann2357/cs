#ifndef COMPILER_TYPES
#define COMPILER_TYPES

#include <string>
#include <vector>
#include <map>
#include <utility>

typedef typename std::vector< std::vector<std::string> > LEXICAL_TABLE;
typedef typename std::vector< std::vector<std::string> > LEXEM_TABLE;
typedef typename std::vector< std::string > LINES_VECTOR;
typedef typename std::vector< std::string > TOKEN_VECTOR;
typedef typename std::map<std::string, std::string> TOKEN_MAP;
typedef typename std::map<int, int> INDENTATION_MAP;
typedef typename std::map<std::string, std::string> LEXEM_TOKEN_MAP;
typedef typename std::map< std::string, std::vector<std::string> > PAIR_VECTOR_TABLE;

typedef struct {
  std::string lexem;
  std::string token;
  int line;
  int indentation;
} PARSER_TABLE_ENTRY;

typedef typename std::vector< PARSER_TABLE_ENTRY > PARSER_TABLE;

typedef struct {
  std::string type;
  int line;
  bool is_function;
  std::vector<std::string> params;
} SEMANTIC_TABLE_ENTRY;

// PAIR < NAME, SCOPE >
typedef typename std::map< std::pair<std::string, int>, SEMANTIC_TABLE_ENTRY > SEMANTIC_TABLE;

#endif
