#include "semantic.h"

int main() {
  Semantic semantic("test.pycc");
  return semantic.run();
}
