#ifndef COMPILER_CONSTANTS
#define COMPILER_CONSTANTS

#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include "types.h"
#include "utils.h"
#include "grammar_constant.h"

std::string SPLIT_TOKENS(
  " ,:+-*/=)("
);
std::string VALID_FIRST_CHAR_NAME (
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
);
std::string VALID_NAME (
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
);
std::string COMPOUND_COMPARISON_OPERATOR ("><=");

std::string EOL("__eol__");
std::string IND("__ind__");
std::string NOT("not");
std::string DEF("def");
std::string INT("int");
std::string FLOAT("float");
std::string STRING("string");
std::string RETURN("return");
std::string MAIN_FUNC("__main__");
std::string COLON(":");
std::string ASSIGNMENT("=");
std::string OPPLUS("+");
std::string OPMINUS("-");
std::string OPMULT("*");
std::string OPDIVIDE("/");
std::string IF("if");
std::string ELSE("else");
std::string WHILE("while");
std::string LPAREN("(");
std::string RPAREN(")");
std::string COMMA(",");
std::string COMPL("<");
std::string COMPLEQ("<=");
std::string COMPG(">");
std::string COMPGEQ(">=");
std::string COMPEQ("==");

std::map <std::string, std::set<std::string> > TYPE_VALUES = {
  {"INT", {"INT", "INTCONSTANT"}},
  {"FLOAT", {"FLOAT", "FLOATCONSTANT"}},
  {"STRING", {"STRING", "STRINGCONSTANT"}}
};

std::set<std::string> TYPES = {
  "INT",
  "FLOAT",
  "STRING"
};

std::set<std::string> CONSTANTS = {
  "INTCONSTANT",
  "FLOATCONSTANT",
  "STRINGCONSTANT"
};

std::set<std::string> PARAMS = {
  "NAME",
  "INTCONSTANT",
  "FLOATCONSTANT",
  "STRINGCONSTANT"
};

std::map<std::string, std::string> CONSTANT_TYPE = {
  {"INTCONSTANT", "INT"},
  {"FLOATCONSTANT", "FLOAT"},
  {"STRINGCONSTANT", "STRING"}
};

std::vector<std::string> TERMINALS = {
  "EMPTY",
  "NOT",
  "DEF",
  "INT",
  "STRING",
  "FLOAT",
  "RETURN",
  "MAIN_FUNC",
  "COLON",
  "LPAREN",
  "RPAREN",
  "IF",
  "ELSE",
  "WHILE",
  "COMMA",
  "IND",
  "COMPL",
  "COMPLEQ",
  "COMPG",
  "COMPGEQ",
  "COMPEQ",
  "OPPLUS",
  "OPMINUS",
  "OPMULT",
  "OPDIVIDE",
  "EOL",
  "NAME",
  "ASSIGNMENT",
  "INTCONSTANT",
  "FLOATCONSTANT",
  "STRINGCONSTANT",
  "RETURN"
};

#endif
