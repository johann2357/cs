pgsql2shp -f /home/johann/Documents/cursos/topicosdb/gis/peru/peru.shp -h localhost -u johann -P johann topicosdb_gis "select name, way from public.planet_osm_polygon where admin_level='4'"
pgsql2shp -f /home/johann/Documents/cursos/topicosdb/gis/peru/peru2.shp -h localhost -u johann -P johann topicosdb_gis "select name, way from public.planet_osm_polygon where admin_level='2'"
pgsql2shp -f /home/johann/Documents/cursos/topicosdb/gis/peru/peru6.shp -h localhost -u johann -P johann topicosdb_gis "select name, way from public.planet_osm_polygon where admin_level='6'"
pgsql2shp -f /home/johann/Documents/cursos/topicosdb/gis/peru/peru8.shp -h localhost -u johann -P johann topicosdb_gis "select name, way from public.planet_osm_polygon where admin_level='8'"

# topicosdb_gis=> select distinct admin_level from planet_osm_polygon;
#  admin_level
# -------------
#
#  8
#  9
#  2
#  4
#  6
#  10
# (7 rows)


# topicosdb_gis=> select name from planet_osm_polygon where admin_level='9';
#       name
# -----------------
#  Rodrigues Alves
# (1 row)

# topicosdb_gis=> select name from planet_osm_polygon where admin_level='8';
#                      name
# -----------------------------------------------
#  La Brea
#  Lobitos
#  Pariñas
#  El Alto
#  Vichayal
# (1879 rows)


# topicosdb_gis=> select distinct name from planet_osm_polygon where admin_level='4';
#         name
# ---------------------
#
#  Cangallo
#  Socos
#  Ica
#  Lima
#  Ancash
#  La Libertad
#  Cusco
#  Tacna
#  Cajamarca
#  Apurímac
#  Tumbes
#  Amazonas
#  Callao
#  Pasco
#  Junín
#  San Martín
#  Huánuco
#  San José de Ticllas
#  Puno
#  Ucayali
#  Madre de Dios
#  Loreto
#  Moquegua
#  Ayacucho
#  Arequipa
#  Piura
#  Lambayeque
#  Huancavelica
# (29 rows)

# topicosdb_gis=> select name from planet_osm_polygon where admin_level='2';
#  name
# ------
#  Perú
# (1 row)

#  topicosdb_gis=> select name from planet_osm_polygon where admin_level='6';
#             name
#  ---------------------------
#   Talara
#   Paita
#   Sechura
#   Contralmirante Villar
#   Sullana
#   Piura
#   Tumbes
#   Lambayeque
#   Cantón Zapotillo
#   Zarumilla
#   Morropón
#   Ayabaca
#   Cantón Huaquillas
#   Chiclayo
#   Ferreñafe
#   Huancabamba
#   Chepén
#   Yunguyo
#  (215 rows)

# topicosdb_gis=> select name from planet_osm_polygon where admin_level='10';
#             name
# -----------------------------
#  Urbanización Pedro Portillo
# (1 row)
