CREATE SCHEMA tdb_postgis_test;
CREATE TABLE tdb_postgis_test.places(id serial, name varchar);
CREATE EXTENSION postgis;
SELECT AddGeometryColumn(
  'tdb_postgis_test', 'places', 'geom', 4326, 'POINT', 2
);
\d tdb_postgis_test.places

INSERT INTO tdb_postgis_test.places (name, geom) VALUES (
  'catedral', ST_GeomFromText('POINT(-16.398059 -71.536692)',4326)
);
INSERT INTO tdb_postgis_test.places (name, geom) VALUES (
  'museo de la catedral', ST_GeomFromText('POINT(-16.398119 -71.537012)',4326)
);
INSERT INTO tdb_postgis_test.places (name, geom) VALUES (
  'capriccio', ST_GeomFromText('POINT(-16.398445 -71.535900)',4326)
);
INSERT INTO tdb_postgis_test.places (name, geom) VALUES (
  'iglesia de la compania', ST_GeomFromText('POINT(-16.399798 -71.536497)',4326)
);
INSERT INTO tdb_postgis_test.places (name, geom) VALUES (
  'iglesia de santo domingo', ST_GeomFromText('POINT(-16.400648 -71.534093)',4326)
);
INSERT INTO tdb_postgis_test.places (name, geom) VALUES (
  'mcdonalds', ST_GeomFromText('POINT(-16.398419 -71.536056)',4326)
);


SELECT name FROM tdb_postgis_test.places ORDER BY geom <-> ST_GeomFromText('POINT(-16.398445 -71.535900)',4326);
