$(document).ready(function() {
  $('#doquery').submit(function(event) {
    var input_string = $('#word').val();
    $.ajax({
      url : '/search',
      data: {q: input_string},
      type : 'GET',
      dataType: 'html',
      success : function(data) {
        if (data == '') {
          data = '<li><h6>No results :(</h6></li>';
        }
        $('#result').html(data);
      },
    });
    event.preventDefault();
  });
});
