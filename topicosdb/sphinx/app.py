import sphinxapi
import MySQLdb

from flask import (
    Flask,
    render_template,
    request,
)

app = Flask(__name__, static_folder='static/', static_url_path='/static')
app.secret_key = 'A2Zr39j/9yX-R~dyH!jmNxLWX/,?RT'

client = sphinxapi.SphinxClient()
client.SetServer('127.0.0.1', 9312)

db = MySQLdb.connect(host="localhost",      # your host, usually localhost
                     user="root",           # your username
                     passwd="",             # your password
                     db="test_sphinx")      # name of the data base


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/search', methods=['GET'])
def query():
    searchword = request.args.get('q', None)
    if searchword is None:
        return ''
    result = client.Query(searchword)
    data = dict()
    data_ids = []
    for article in result['matches']:
        data[article['id']] = dict(w=article['weight'])
        data_ids.append(article['id'])

    if data_ids:
        sql = 'SELECT id, title, content FROM dummy WHERE id IN (%s)'
        in_p = ', '.join(map(lambda x: str(x), data_ids))

        cursor = db.cursor()
        cursor.execute(sql % in_p)
        for (pk, title, content) in cursor.fetchall():
            data[pk]['title'] = title
            data[pk]['content'] = content

    response = ''
    for pk in data_ids:
        response += '<li><h3>%s</h3><h6>Weight: %d</h6><p>%s</p></li>' % (
            data[pk]['title'],
            data[pk]['w'],
            data[pk]['content'],
        )
    return response


if __name__ == '__main__':
    # FUTURE REFERENCE: http://sphinxsearch.com/blog/2014/11/05/sphinx-search-quick-tour-using-a-mysql-datasource/
    app.run()
