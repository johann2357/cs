% listas
profes([paul_nina, luz_vasquez, luis_diaz, christian_ortiz, anita_mamani]). 

%
% ?- profes([X,Y|_]).
% X = paul_nina,
% Y = luz_vasquez.
% 
% ?- profes([X|[Y|_]]).
% X = paul_nina,
% Y = luz_vasquez.
%

primero([X|_],X).

%
% ?- primero([2,3,4,5], Z).
% Z = 2.
% 

eliminar([_|X], X).

%
% ?- eliminar([luis,rosa,jose,johann], Z).
% Z = [rosa, jose, johann].
%

comparar([X|[X|_]]). %el primero y segundo iguales

% 
% ?- comparar([1,3,2]).
% false.
% 

buscar([X|_], X).                  % analisis de la cabeza
buscar([_|Y], X) :- buscar(Y, X).  % analisis de la cola

% 
% ?- buscar([2,3,4,5,6,7], 6).
% true .
% 
% ?- buscar([2,3,4,5,6,7], 666).
% false.
%

%len([], 0).
%len([_|Y], N) :- len(Y, M), N is M + 1.

len([], 0).
len([_|Y], X) :- len(Y, R),  X is R + 1.

eliminar(X, [X|Y], Y).      % tratamiento de la cabeza
eliminar(X, [Y|Z], [Y|W]) :- eliminar(X,Z,W).
