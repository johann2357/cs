% estructuras
% nombre1(a, b, nombre2())

nace(johann, fecha(31,10,1994)).
nace(diana, fecha(20,10,1990)).
nace(vera, fecha(16,3,1990)).
nace(irene, fecha(28,9,1995)).
nace(carlos, fecha(20,5,2002)).
nace(manuel, fecha(20,5,2002)).
nace(susan, fecha(11,7,2001)).
nace(mark, fecha(21,9,2000)).
nace(juan, fecha(21,8,2004)).
nace(adolfo, fecha(11,12,1999)).

edad(X) :-
  nace(X,fecha(D,M,Y)),
  (
    (
      (
        (M > 9);
        ((M >= 9), (D > 25))
      ),
      EDAD is 2014 - Y - 1
    );
    EDAD is 2014 - Y
  ),
  (
    (
      EDAD >= 18,
      write('eres mayor de edad')
    );
    (
      YY is 18 - EDAD,
      ( 
        (
          M - 9 > 0,
          MM is M - 9,
          YY is YY - 1
        );
        (
          M - 9 < 1,
          MM is 9 - M
        )
      ),
      ( 
	DD is 25 - D 
      ),
      write(X),nl,
      write('edad: '),write(EDAD),nl,
      write('Para ser mayor de edad le faltan'),nl,
      write(DD),write(' dias'),nl,
      write(MM),write(' meses'),nl,
      write(YY),write(' anios')
    )
  ).
