% tarea b) averiguar signos del zodiaco

nace(johann, fecha(31,10,1994)).
nace(diana, fecha(20,10,1990)).
nace(vera, fecha(16,3,1990)).
nace(irene, fecha(28,9,1995)).
nace(carlos, fecha(20,5,2002)).
nace(manuel, fecha(23,3,2003)).
nace(susan, fecha(11,7,2001)).
nace(mark, fecha(21,9,2000)).
nace(juan, fecha(21,8,2004)).
nace(adolfo, fecha(11,12,1999)).

% Aries: March 21 - April 19
aries(fecha(D, M, Y)) :-
  (
    M =:= 3,
    D > 20
  );
  (
    M =:= 4,
    D < 20 
  ).

% Taurus: April 20 - May 20
tauro(fecha(D, M, Y)) :-
  (
    M =:= 4,
    D > 19
  );
  (
    M =:= 5,
    D < 21 
  ).

% Gemini: May 21 - June 20
geminis(fecha(D, M, Y)) :-
  (
    M =:= 5,
    D > 20 
  );
  (
    M =:= 6,
    D < 21 
  ).

% Cancer: June 21 - July 22
cancer(fecha(D, M, Y)) :-
  (
    M =:= 6,
    D > 20 
  );
  (
    M =:= 7,
    D < 23 
  ).

% Leo: July 23 - August 22
leo(fecha(D, M, Y)) :-
  (
    M =:= 7,
    D > 22 
  );
  (
    M =:= 8,
    D < 23 
  ).

% Virgo: August 23 - Sept. 22
virgo(fecha(D, M, Y)) :-
  (
    M =:= 8,
    D > 22 
  );
  (
    M =:= 9,
    D < 23 
  ).

% Libra: Sept. 23 - October 22
libra(fecha(D, M, Y)) :-
  (
    M =:= 9,
    D > 22 
  );
  (
    M =:= 10,
    D < 23 
  ).

% Scorpio: October 23 - Nov. 21
scorpio(fecha(D, M, Y)) :-
  (
    M =:= 10,
    D > 22 
  );
  (
    M =:= 11,
    D < 22 
  ).

% Sagittarius: Nov. 22 - Dec. 21
sagitario(fecha(D, M, Y)) :-
  (
    M =:= 11,
    D > 21 
  );
  (
    M =:= 12,
    D < 22 
  ).

% Capricorn: Dec. 22 - Jan. 19
capricornio(fecha(D, M, Y)) :-
  (
    M =:= 12,
    D > 21 
  );
  (
    M =:= 1,
    D < 20 
  ).

% Aquarius: Jan. 20 - Feb. 18
aquario(fecha(D, M, Y)) :-
  (
    M =:= 1,
    D > 19 
  );
  (
    M =:= 2,
    D < 19 
  ).

% Pisces: Feb. 19 - March 20
piscis(fecha(D, M, Y)) :-
  (
    M =:= 2,
    D > 18 
  );
  (
    M =:= 3,
    D < 21
  ).

% signo(X) : averiguar el signo de X
signo(X) :-
  nace(X, fecha(D,M,Y)),
  write('el signo de '), write(X), write(' es: '),
  (
    (
      aries(fecha(D,M,Y)), write('Aries')
    );
    (
      tauro(fecha(D,M,Y)), write('Tauro')
    );
    (
      geminis(fecha(D,M,Y)), write('Geminis')
    );
    (
      cancer(fecha(D,M,Y)), write('Cancer')
    );
    (
      leo(fecha(D,M,Y)), write('Leo')
    );
    (
      virgo(fecha(D,M,Y)), write('Virgo')
    );
    (
      libra(fecha(D,M,Y)), write('Libra')
    );
    (
      scorpio(fecha(D,M,Y)), write('Escorpio')
    );
    (
      sagitario(fecha(D,M,Y)), write('Sagitario')
    );
    (
      capricornio(fecha(D,M,Y)), write('Capricornio')
    );
    (
      aquario(fecha(D,M,Y)), write('Aquario')
    );
    (
      piscis(fecha(D,M,Y)), write('Piscis')
    )
  ).

