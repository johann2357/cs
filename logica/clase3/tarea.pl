% datos

entrada(tamal).
entrada(ensalada).
entrada(papa_a_la_huancaina).
postre(pie_de_limon).
postre(helado).
carnes(chuleta_de_cerdo).
carnes(pollo_al_vino).
pasta(pastel_de_tallarin).

% reglas

plato_de_fondo(X) :-
  carnes(X); pasta(X).

comida(X, Y, Z) :-
  entrada(X), postre(Y), plato_de_fondo(Z).

alguna_comida(X) :-
  entrada(X); postre(X); plato_de_fondo(X).

ordenar_menu(Respuesta) :-
  write('Desea ordenar algo? (si/no)'), nl, read(Orden),
  (
    (Orden = si, Respuesta is 1);
    (Orden = no, nl, write('Gracias, hasta luego.'), Respuesta is 0);
    ordenar_menu(Respuesta)
  ).

verificar_comida(Orden, Tipo) :-
  (
    (
      alguna_comida(Orden),
      write(Orden),
      write(' no es un '),
      write(Tipo), nl
    );
    (
      write('    disculpe en este momento no tenemos '),
      write(Orden), nl
    )
  ).

ordenar_entrada(Respuesta) :-
  write('  Que desea como plato de entrada?'), nl, read(Orden),
  (
    (entrada(Orden), Respuesta = Orden);
    (
      verificar_comida(Orden, 'plato de entrada'),
      ordenar_entrada(Respuesta)
    )
  ).

ordenar_plato_de_fondo(Respuesta) :-
  write('  Que desea como plato de fondo?'), nl, read(Orden),
  (
    (plato_de_fondo(Orden), Respuesta = Orden);
    (
      verificar_comida(Orden, 'plato de fondo'),
      ordenar_plato_de_fondo(Respuesta)
    )
  ).

ordenar_postre(Respuesta) :-
  write('  Que desea como postre?'), nl, read(Orden),
  (
    (postre(Orden), Respuesta = Orden);
    (
      verificar_comida(Orden, 'postre'),
      ordenar_postre(Respuesta)
    )
  ).

consultar_menu :- 
  ordenar_menu(Respuesta),
  (
    (Respuesta =:= 0);
    (
      ordenar_entrada(Entrada),
      ordenar_plato_de_fondo(Plato_de_fondo),
      ordenar_postre(Postre),
      %
      write('Usted ordeno: '), nl,
      write('   Entrada: '),
      write(Entrada), nl,
      write('   Plato de fondo: '),
      write(Plato_de_fondo), nl,
      write('   Postre: '),
      write(Postre), nl,
      write(' Gracias por su preferencia')
    )
  ).
