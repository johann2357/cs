ejemplo :- 
  write('ingresar nombre '), read(Name),
  write('tu edad es '), read(Edad),
  (
    (Edad>=18, write(Name), nl, write('presente su solicitud'));
    (write(Name), nl, write('eres menor de edad'))
  ).

ej6 :-
  write('MENU:'), nl, write('(1) SUMA'), nl, write('(2) PRODUCTO'), nl,
  write(' Num1: '), read(Num1), write(' Num2: '), read(Num2), 
  write(' Op: '), read(Op),
  (
    (Op =:= 1, Z is Num1 + Num2);
    (Op =:= 2, Z is Num1 * Num2);
    (Z is 0, write(' Op is not an option'))
  ),
  write(' resultado: '), write(Z).


ej7 :- 
  write('horas '),read(X),W is X*15,
  write('sueldo '), write(W), nl,
  (
    (
      X >= 26, W2 is W * 0.15,
      write('desc '), write(W2)
    );
    (
      X >= 21, W1 is W * 0.1,
      write('desc '), write(W1)
    );
    write('desc 0.0')
  ).
