% 1
% datos(nombre, apellido, ingreso(dia, mes, anho))

datos(johann, nunez, ingreso(5, 1, 2013).
datos(ernesto, cuadros, ingreso(1, 6, 1995).
datos(jose, penarrieta, ingreso(9, 10, 2005).

ejercicio1 :- (
  write('Ingrese nombre o apellido:'),
  read(X),
  write('Ingrese el sueldo mensual:'),
  read(S),
  write('La bonificacion es:'),
  (
    (
      datos(X, _, ingreso(D, M, A)),
      (
        (
          (
            M < 11,
            AT is 2014 - A
          );
          (
            M == 11,
            D < 27,
            AT is 2014 - A
          );
          (
            AT is 2014 - A - 1
          )
        ),
        (
          (
            AT < 8,
            B is S * AT * 12 * 0.2,
            write(B)
          );
          (
            AT >= 8,
            AT < 15,
            B is S * AT * 12 * 0.4,
            write(B)
          );
          (
            AT > 15,
            B is S * AT * 12 * 0.6,
            write(B)
          )
        ),
        nl, write('por '), write(AT), write(' anhos.')
      )
    );
    (
      datos(N, X, ingreso(D, M, A)),
      (
        (
          (
            M < 11,
            AT is 2014 - A
          );
          (
            M == 11,
            D < 27,
            AT is 2014 - A
          );
          (
            AT is 2014 - A - 1
          )
        ),
        (
          (
            AT < 8,
            B is S * AT * 12 * 0.2,
            write(B)
          );
          (
            AT >= 8,
            AT < 15,
            B is S * AT * 12 * 0.4,
            write(B)
          );
          (
            AT > 15,
            B is S * AT * 12 * 0.6,
            write(B)
          )
        ),
        nl, write('por '), write(AT), write(' anhos.')
      )
    )
  )
).

% 2

tamanho([], 0).
tamanho([_|Cola], X) :-
  tamanho(Cola, R),
  X is R + 1.

enumerar([], _).
    
enumerar([Cabeza|Cola], X) :-
  write(X), write('. '),
  X2 is X + 1,
  write(Cabeza), nl,
  enumerar(Cola, X2).

pertenece(X, [Cabeza|Cola]) :-
  X = Cabeza; pertenece(X, Cola).

borrar_repetidos([], _, []) :- !.
borrar_repetidos([Cabeza|Cola], Y, Z) :-
  pertenece(Cabeza, Y), !,
  borrar_repetidos(Cola, Y, Z).
borrar_repetidos([Cabeza|Cola], Y, [Cabeza|Cola2]) :-
  borrar_repetidos(Cola, Y, Cola2).

ejercicio2(L1, L2) :-
  borrar_repetidos(L1, L2, RESULTADO),
  borrar_repetidos(L2, L1, RESULTADO2),
  enumerar(RESULTADO, 1),
  tamanho(RESULTADO, T1),
  T2 is T1 + 1,
  enumerar(RESULTADO2, T2).

% consultas
  % ?- ejercicio2([jose, pedro, ana], [pedro, luz, luis]).
  % 1. jose
  % 2. ana
  % 3. luz
  % 4. luis
  % true.

  % ?- ejercicio2([jose, pedro, ana], [luis, johann]).
  % 1. jose
  % 2. pedro
  % 3. ana
  % 4. luis
  % 5. johann
  % true.
