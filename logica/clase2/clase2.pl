% hechos

pais(argentina).
pais(peru).
pais(brasil).
pais(unitedstates).
pais(mexico).
pais(panama).
pais(alemania).
pais(china).
pais(camerun).
pais(belgica).
capital(lima, peru).
capital(brasilia, brasil).
capital(buanosaires, argentina).
capital(unitedstates, washington).
capital(mexicocity, mexico).
capital(panamacity, panama).
capital(berlin, alemania).
capital(beijing, china).
capital(yaounde, camerun).
capital(bruselas, belgica).
continente(argentina, americasur).
continente(peru, americasur).
continente(brasil, americasur).
continente(unitedstates, americanorte).
continente(mexico, americanorte).
continente(panama, americanorte).
continente(alemania, europa).
continente(china, asia).
continente(camerun, africa).
continente(belgica, europa).

% 1.
%   a.
% ?- pais(X).
% X = argentina ;
% X = peru ;
% X = brasil ;
% X = unitedstates ;
% X = mexico ;
% X = panama ;
% X = alemania ;
% X = china ;
% X = camerun ;
% X = belgica.

%   b.
% ?- capital(CAPITAL, PAIS).
% CAPITAL = lima,
% PAIS = peru ;
% CAPITAL = brasilia,
% PAIS = brasil ;
% CAPITAL = buanosaires,
% PAIS = argentina ;
% CAPITAL = unitedstates,
% PAIS = washington ;
% CAPITAL = mexicocity,
% PAIS = mexico ;
% CAPITAL = panamacity,
% PAIS = panama ;
% CAPITAL = berlin,
% PAIS = alemania ;
% CAPITAL = beijing,
% PAIS = china ;
% CAPITAL = yaounde,
% PAIS = camerun ;
% CAPITAL = bruselas,
% PAIS = belgica.

%   c.
%
pais_capital(P,C,CT):-(continente(P, CT),capital(C, P)).
% ?- pais_capital(PAIS,CAPITAL,americasur).
% PAIS = argentina,
% CAPITAL = buanosaires ;
% PAIS = peru,
% CAPITAL = lima ;
% PAIS = brasil,
% CAPITAL = brasilia.

% 2.
hombre(luis).
hombre(cristian).
hombre(matias).
hombre(michael).
hombre(peter).
hombre(tonny).
mujer(irma).
mujer(karelia).
mujer(cinthia).
mujer(valeria).
% x es padre de y ->  padre(x, y)
padre(luis, karelia).
padre(luis, michael).
padre(luis, cinthia).
padre(cristian, matias).
padre(peter, tonny).
padre(peter, valeria).
% x es madre de y ->  madre(x, y)
madre(irma, karelia).
madre(irma, michael).
madre(irma, cinthia).
madre(karelia, matias).
madre(cinthia, valeria).
madre(cinthia, tonny).

%   a.
% x es hermano de y -> hermano(x, y)
hermano(X, Y):-(hombre(X),padre(W, X),padre(W, Y),X\=Y).
% x es hermana de y -> hermana(x, y)
hermana(X, Y):-(mujer(X),padre(W, X),padre(W, Y),X\=Y).

%   b.
% x es abuelo de y -> abuelo(x, y)
abuelo(X, Y):-(hombre(X),(padre(X, W);madre(X, W)),(padre(X, W);madre(X, W))).
% x es abuela de y -> abuela(x, y)
abuela(X, Y):-(mujer(X),(padre(X, W);madre(X, W)),(padre(X, W);madre(X, W))).
% x es abuelo/a de y -> abuelos(x, y)
abuelos(X, Y):-(abuelo(X, Y);abuela(X, Y)).

%   c.
% x es primo de y -> primo(x, y)

