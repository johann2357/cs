% EJERCICIO 5
varon(mario).
varon(francisco).
varon(eduardo).
varon(luis).
mujer(beatriz).
mujer(alicia).
mujer(victoria).
mujer(veronica).
esposos(mario,alicia).
esposos(francisco,victoria).
esposos(eduardo,veronica).
padres(mario,alicia,beatriz).
padres(francisco,victoria,alicia).
padres(francisco,victoria,eduardo).
padres(eduardo,veronica,luis).
% reglas x es hermano de y
hermano(X,Y):-(padres(P,M,X),padres(P,M,Y)),varon(X).
hermana(X,Y):-(padres(P,M,X),padres(P,M,Y)),mujer(X).
% reglas x es hijo de y
hijo(X,Y):-(padres(P,_,X);padres(_,M,X)),varon(X).
hija(X,Y):-(padres(P,_,X);padres(_,M,X)),mujer(X).
