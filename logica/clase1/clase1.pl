% hechos
/*
animal(leon).
animal(loro).
animal(tigre).
animal(oso).
/*

% luis estudia edi
/*
estudia(luis,edi).
estudia(luis,algebra).
estudia(luis,fisica1).
estudia(rosa,edii).
estudia(rosa,ami).
estudia(rosa,fisica1).
estudia(ana,fisica1).
estudia(ana,logica).
estudia(ana,ada).
estudia(jose,logica).
estudia(jose,amiii).
estudia(jose,ediii).
estudia(pedro,fisica1).
estudia(pedro,edi).
estudia(pedro,algebra).
*/

% ejercicio 2
/*
amigo(ernesto,juan).
amigo(miguel,pedro).
amigo(jose,jeison).
amigo(luis,jose).
amigo(adolfo,johann).
amigo(luis,guillermo).
amigo(alexandra,joseline).
amigo(jonathan,joseline).
amigo(joseline,ana).
varon(ernesto).
varon(juan).
varon(jose).
varon(johann).
varon(jonathan).
varon(jeison).
varon(miguel).
varon(pedro).
varon(adolfo).
varon(luis).
varon(guillermo).
mujer(alexandra).
mujer(joseline).
*/
%regla amigos(x,y): los amigos de x son y
/*
amigos(X,Y):-(amigo(X,Y);amigo(Y,X)),varon(Y).
amigas(X,Y):-(amigo(X,Y);amigo(Y,X)),mujer(Y).
*/

% Ejercicio 5
varon(mario).
varon(francisco).
varon(eduardo).
varon(luis).
mujer(beatriz).
mujer(alicia).
mujer(victoria).
mujer(veronica).
esposos(mario,alicia).
esposos(francisco,victoria).
esposos(eduardo,veronica).
padres(mario,alicia,beatriz).
padres(francisco,victoria,alicia).
padres(francisco,victoria,eduardo).
padres(eduardo,veronica,luis).
% reglas x es hermano de y
hermano(X,Y):-(padres(P,M,X),padres(P,M,Y)),varon(X).


% animal(X).
% animal(_).
% Condic: A->B
% B:-A
% 
% EJERCICIO 1
% estudia(X,logica).
% estudia(ana,edii).
% estudia(pedro,X).
% estudia(_,fisica1).
% 
% EJERCICIO 2

