% 1 Eliminar todos los elementos de la lista1 que esten en la lista2

pertenece(X, [Cabeza|Cola]) :-
    X = Cabeza; pertenece(X, Cola).

borrar_repetidos([], _, []).
borrar_repetidos([Cabeza|Cola], Y, Z) :-
    pertenece(Cabeza, Y),
    borrar_repetidos(Cola, Y, Z).
borrar_repetidos([Cabeza|Cola], Y, [Cabeza|Cola2]) :-
    borrar_repetidos(Cola, Y, Cola2).

  % borrar_repetidos(lista1, lista2, Resultado)

% Consultas
  % ?- borrar_repetidos([1,2,3], [1,3], Resultado).
  % Resultado = [2] .

  % ?- borrar_repetidos([a,b,c,d,e,f,g,g,d,d], [d,g], Resultado).
  % Resultado = [a, b, c, e, f] .


% 2 Suma los pares

suma_pares([], 0).
suma_pares([X|T], N) :- 
  (
    suma_pares(T, M),
    (
      (
        0 is X mod 2,
        N is M + X
      );
      N is M
    )
  ).

  % suma_pares(lista, Resultado)

% Consultas
  % ?- suma_pares([2,3,4,5,6], Resultado).
  % Resultado = 12 .

  % ?- suma_pares([1,3,5,7], Resultado).
  % Resultado = 0.


% 3 Hacer una regla que sume los elementos pares que tenga una lista
%   numerica

fibo(X, Y, Respuesta) :-
    Respuesta < X,
    Temporal is Respuesta + Y,
    write(Respuesta), tab(2),
    fibo(X, Respuesta, Temporal).

fibonnaci(Num) :-
    write(0), tab(2),
    fibo(Num, 0, 1).

% Consultas
  % ?- fibonnaci(3).
  % 0  1  1  2 

  % ?- fibonnaci(4).
  % 0  1  1  2  3  


% 4 amistades

tamanho([], 0).
tamanho([_|Y], X) :-
    tamanho(Y, T),
    X is T + 1.

juntar([], Lista, Lista).
juntar([Cabeza|Cola], Lista, [Cabeza|Cola2]) :-
    juntar(Cola, Lista, Cola2).

invitar_total(X, Y, T, Z) :- 
    borrar_repetidos(X, Y, L),
    juntar(Y, L, T),
    tamanho(T, Z).

  % invitar_total(Lista1, Lista2, ListaDeInvitados, Total).

% Consultas
  % ?- invitar_total([a, b, c, d], [d, b], Invitar, Total).
  % Invitar = [d, b, a, c],
  % Total = 4 .

  % ?- invitar_total([a, e], [d, b, f, g], Invitar, Total).
  % Invitar = [d, b, f, g, a, e],
  % Total = 6.
