% Recursividad

hijo(b, a).
hijo(c, a).
hijo(g, c).
hijo(d, b).
hijo(e, b).
hijo(f, e).
hijo(h, d).
hijo(j, d).


hijo(q, p).
hijo(r, q).
hijo(m, q).

% regla desc(x,y): x es descendiente de y
% caso base
%        Y
%     Z
%  X

des(X, Y) :- hijo(X, Y).

des(X, Y) :- hijo(X, Z), des(Z, Y).


% 2do ejercicio

range(M, N, P) :- 
	M =< N,
	write(M),nl,
  M2 is M + P,
  range(M2, N, P).

enum(M, N) :- 
  range(M, N, 1).

% 3er ejercicio

factorial(N, ACUM) :-
	(
    (
       N =< 1,
			 write(ACUM)
    );
    (
       N2 is N - 1,
       ACUM2 is ACUM * N,
       factorial(N2, ACUM2)
    )
  ).

factorial(N) :-
  factorial(N, 1).
  
% 4to ejercicio

potencia(N, X, Y, COMP) :-
  (
      (
         N =:= 0,
				 Y =:= COMP
      );
      (
				 N > 0,
         N2 is N - 1,
         ACUM is Y * X,
         potencia(N2, X, ACUM, COMP)
      )
  ).

potencia(N, X, Y) :-
  potencia(N, X, 1, Y).

% 8vo ejercicio

suma_pares(N, ACUM) :- 
	(
    (
       N < 1,
       write(ACUM)
    );
    (
       M is N mod 2,
       M =:= 1
    );
    (
       N2 is N - 2,
       ACUM2 is ACUM + N,
       factorial(N2, ACUM2)
    )
  ).

suma_pares(N) :-
  suma_pares(N, 0).
  
