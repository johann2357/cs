from uri_data import URI_DATA
from local_settings import USER_DATA

'''
SONGS SOURCE: http://pandorasongs.oliverzheng.com/
URI FROM SONGS NAME: http://www.playlist-converter.net/
API DOCS: https://developer.spotify.com/web-api/console/post-playlist-tracks/
'''

playlist_url = 'https://api.spotify.com/v1/users/%s/playlists/%s/tracks?uris=' % (
    USER_DATA['username'], USER_DATA['playlist_id']
)


def create_curls(data, size=100):
    chunks = [
        data[x: x + size]
        for x in xrange(0, len(data), size)
    ]
    result = []
    for chunk in chunks:
        result.append(' '.join([
            'curl',
            '-X',
            'POST',
            '"%s%s"' % (playlist_url, ','.join(chunk), ),
            '-H',
            '"Accept: application/json"',
            '-H',
            '"Authorization: Bearer %s"' % (USER_DATA['token'], ),
        ]))

    return result


for call in create_curls(URI_DATA):
    print call
