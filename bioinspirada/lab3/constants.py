import math


def obj2(x, y):
    return - x ** 2 - y ** 2


def obj(x, y):
    top = (math.sin(math.sqrt(x ** 2 + y ** 2))) ** 2 - 0.5
    bottom = (1.0 + 0.001 * (x ** 2 + y ** 2)) ** 2
    return 0.5 - top / bottom

#################
# 20 iterations #
#################

CONF1 = [
    # pc=65% pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=65% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=65% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=65% pm=0.8%',
        iterations=20
    ),
]

CONF2 = [
    # pc=85% pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.85,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=85% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.85,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=85% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.85,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=85% pm=0.8%',
        iterations=20
    ),
]

CONF3 = [
    # pc=55% pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.55,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=55% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.55,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=55% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.55,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=55% pm=0.8%',
        iterations=20
    ),
]

CONF4 = [
    # pc=25% pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.25,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=25% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.25,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=25% pm=0.8%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.25,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=25% pm=0.8%',
        iterations=20
    ),
]

CONF5 = [
    # pc=65% pm=0.4%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.004,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=65% pm=0.4%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.004,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=65% pm=0.4%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.004,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=65% pm=0.4%',
        iterations=20
    ),
]

CONF6 = [
    # pc=65% pm=1.0%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.01,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=65% pm=1.0%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.01,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=65% pm=1.0%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.01,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=65% pm=1.0%',
        iterations=20
    ),
]

CONF7 = [
    # pc=65% pm=2.0%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.02,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=65% pm=2.0%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.02,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=65% pm=2.0%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.02,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=65% pm=2.0%',
        iterations=20
    ),
]

CONF8 = [
    # pc=65% pm=5.0%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.05,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=65% pm=5.0%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.05,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Two Point p=100 g=40 pc=65% pm=5.0%',
        iterations=20
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='uniform',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.05,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='Uniform p=100 g=40 pc=65% pm=5.0%',
        iterations=20
    ),
]

###################
# 1000 iterations #
###################

CONFIG1 = [
    # individuos=10  gen=400  pc=65%  pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=10,
        generations=400,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=10 g=400 pc=65% pm=0.8% i=1000',
        iterations=1000
    ),
]

CONFIG2 = [
    # individuos=100  gen=40  pc=65%  pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=65% pm=0.8% i=1000',
        iterations=1000
    ),
]

CONFIG3 = [
    # individuos=1000  gen=4  pc=65%  pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='one_point',
        p_size=1000,
        generations=4,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=1000 g=4 pc=65% pm=0.8% i=1000',
        iterations=1000
    ),
]

###########################
# 1000 ITERATIONS + OTHER #
###########################

CONFIGURATION = [
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='No normalization No elitism Two Point p=100 g=40 pc=65% pm=0.8% i=1000',
        iterations=1000
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=10,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='No normalization With elitism 10% Two Point p=100 g=40 pc=65% pm=0.8% i=1000',
        iterations=1000
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=10,
        normalize=True,
        v_min=10,
        v_max=60,
        title='Normalization 10-60 With elitism 10% Two Point p=100 g=40 pc=65% pm=0.8% i=1000',
        iterations=1000
    ),
    dict(
        objective=obj,
        gen_params=[(-50, 50, 7), (-50, 50, 7)],
        s_method='roulette',
        c_method='two_point',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=10,
        normalize=True,
        v_min=1,
        v_max=200,
        title='Normalization 1-200 With elitism 10% Two Point p=100 g=40 pc=65% pm=0.8% i=1000',
        iterations=1000
    ),
]

PROCESAR1 = CONFIG1 + [CONFIGURATION[0]]
PROCESAR2 = CONFIG2 + [CONFIGURATION[1]]
PROCESAR3 = CONFIG3
PROCESAR4 = [CONFIGURATION[2]]
PROCESAR5 = [CONFIGURATION[3]]
