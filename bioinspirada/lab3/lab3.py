import math
import random

import matplotlib.pyplot as plt
import numpy as np

from constants import (
    PROCESAR5,
)


class GeneticAlgorithm(object):

    def __init__(self, objective, gen_params, *args, **kwargs):
        self.objective = objective
        self.gen_params = gen_params
        self.population_size = kwargs.pop('p_size', 100)
        self.mutation_rate = kwargs.pop('m_rate', 0.08)
        self.crossover_rate = kwargs.pop('co_rate', 0.65)
        self.generations = kwargs.pop('generations', 40)
        self.selection_method = kwargs.pop('s_method', 'roulette')
        self.crossover_method = kwargs.pop('c_method', 'one_point')
        self.elitism = kwargs.pop('elitism', 0)
        self.normalize = kwargs.pop('normalize', False)
        self.vmin = kwargs.pop('v_min', 0)
        self.vmax = kwargs.pop('v_max', 100)
        self.population = self.make_initial_population()
        self.average_data = []
        self.best_so_far_data = []
        self.online_data = []
        self.offline_data = []

    def __str__(self):
        return str(self.population)

    def show(self):
        result = ''
        for c in self.population:
            result += str(c)
        return '[\n%s]' % result

    def make_initial_population(self):
        return [
            Chromosome(data=self.gen_params, objective=self.objective)
            for x in xrange(self.population_size)
        ]

    def calc_weighted_probability(self):
        self.population.sort(key=lambda c: c.evaluate())
        if self.normalize:
            partial = []
            for i in xrange(1, self.population_size + 1):
                partial.append(float(
                    self.vmin + (i - 1) * (self.vmax - self.vmin) / float(
                        self.population_size - 1
                    )
                ))
            partial = np.array(partial)
        else:
            partial = np.array([c.evaluate() for c in self.population])
            partial = partial + abs(partial.min())
        total = abs(partial).sum()
        self.weighted_probability = partial / total

        return self.weighted_probability

    def get_weighted_probability(self):
        return self.weighted_probability

    def select_chromosome_roulette(self):
        return np.random.choice(
            self.population,
            1,
            p=self.get_weighted_probability()
        )[0]

    def select_chromosome_tournament(self):
        cs = random.sample(self.population, 2)
        if cs[0].evaluate() > cs[1].evaluate():
            return cs[0]
        return cs[1]

    def select_chromosome(self):
        if self.selection_method == 'tournament':
            return self.select_chromosome_tournament()
        return self.select_chromosome_roulette()

    def make_children(self, c1, c2):
        if self.crossover_method == 'uniform':
            result = c1.uniform_crossover(
                c2
            )
        elif self.crossover_method == 'two_point':
            result = c1.two_point_crossover(
                c2,
                [
                    random.randint(0, len(c1.data) - 1),
                    random.randint(0, len(c1.data) - 1)
                ]
            )
        else:
            result = c1.one_point_crossover(
                c2,
                random.randint(0, len(c1.data) - 1)
            )
        return [
            c1.create(result[0]),
            c2.create(result[1])
        ]

    def gen_child(self):
        while True:
            c1 = self.select_chromosome()
            c2 = self.select_chromosome()
            while id(c1) == id(c2):
                c2 = self.select_chromosome()

            parents = [c1, c2]
            if random.uniform(0, 1) < self.crossover_rate:
                children = self.make_children(*parents)
            else:
                children = parents

            for c in children:
                if random.uniform(0, 1) < self.mutation_rate:
                    yield c.create(c.mutate())
                else:
                    yield c

    def best_so_far(self):
        self.best_so_far_data.append(
            max(self.population, key=lambda c: c.evaluate()).evaluate()
        )

    def online(self, t):
        self.average_data.append(
            sum(
                map(lambda c: c.evaluate(), self.population)
            ) / float(self.population_size)
        )
        self.online_data.append(
            (1 / float(t + 1)) * (
                sum(self.average_data[:t + 1])
            )
        )

    def offline(self, t):
        self.offline_data.append(
            (1 / float(t + 1)) * (
                sum(self.best_so_far_data[:t + 1])
            )
        )

    def run(self):
        for gidx in xrange(self.generations):
            print '\tGeneration %d' % gidx
            new_population = []
            if self.selection_method == 'roulette':
                self.calc_weighted_probability()

            if self.elitism:
                values = np.array([c.evaluate() for c in self.population])
                for cidx in np.argsort(-values)[:self.elitism]:
                    new_population.append(self.population[cidx])

            child_generator = self.gen_child()
            for cidx in xrange(self.population_size - self.elitism):
                new_population.append(
                    next(child_generator)
                )

            self.population = new_population
            self.best_so_far()
            self.online(gidx)
            self.offline(gidx)


class GeneticFunctions(object):

    def __init__(self, *args, **kwargs):
        super(GeneticFunctions, self).__init__(*args, **kwargs)

    def one_point_crossover(self, obj, position):
        result = (
            np.concatenate([
                self.data[:position],
                obj.data[position:]
            ]),
            np.concatenate([
                obj.data[:position],
                self.data[position:]
            ]),
        )
        return result

    def two_point_crossover(self, obj, position):
        position.sort()
        result = (
            np.concatenate([
                self.data[:position[0]],
                obj.data[position[0]:position[1]],
                self.data[position[1]:]
            ]),
            np.concatenate([
                obj.data[:position[0]],
                self.data[position[0]:position[1]],
                obj.data[position[1]:]
            ]),
        )
        return result

    def uniform_crossover(self, obj):
        r1 = self.data.copy()
        r2 = obj.data.copy()
        for idx in xrange(self.data.size):
            if random.uniform(0, 1) < 0.5:
                r1[idx], r2[idx] = r2[idx], r1[idx]
        return (r1, r2)

    def evaluate(self):
        return self.objective(*self.values())

    def mutate(self):
        index = random.randint(0, len(self.data) - 1)
        result = self.data.copy()
        result[index] = int(not result[index])
        return result


class Gene(object):

    def __init__(self, left, right, p, *args, **kwargs):
        self.left = left
        self.right = right
        self.p = p
        self.size = self.calculate_size()
        self.data = kwargs.pop(
            'data', np.random.randint(2, size=(self.size, 1))
        )
        super(Gene, self).__init__(*args, **kwargs)

    def __str__(self):
        result = ''
        for x in self.data:
            result += '%d' % x[0]
        return u'%s -> %f \tlimits:[%f:%f] \tprecision:%d' % (
            result,
            self.decode(),
            self.left,
            self.right,
            self.p
        )

    def __repr__(self):
        return '<Gene: %f [%f:%f] %d>' % (
            self.decode(),
            self.left,
            self.right,
            self.p,
        )

    def calculate_size(self):
        return (
            math.log(self.right - self.left, 2) +
            self.p * math.log(10, 2)
        )

    def decode(self):
        acum = 0
        for idx, elem in enumerate(self.data):
            acum += 2 ** idx * elem[0]
        return (
            self.left +
            acum * ((self.right - self.left) / (2 ** self.size - 1))
        )


class Chromosome(GeneticFunctions):

    def __init__(self, data, *args, **kwargs):
        base = kwargs.pop('instance', None)
        if base is None:
            self.objective = kwargs.pop('objective')
            self.genes = [Gene(*gene) for gene in data]
        else:
            self.objective = base.objective
            self.genes = []
            begin = 0
            end = 0
            for g in base.genes:
                end = begin + g.size
                self.genes.append(Gene(
                    left=g.left,
                    right=g.right,
                    p=g.p,
                    data=data[begin: end]
                ))
                begin = end
        self.data = self.gather()
        super(Chromosome, self).__init__(*args, **kwargs)

    def __str__(self):
        result = ''
        for g in self.genes:
            result += '  %s,\n' % str(g)
        return ' %f: [\n%s ],\n' % (
            self.evaluate(),
            result,
        )

    def __repr__(self):
        return '<Chromosome: %f %s>' % (
            self.evaluate(),
            str(self.genes)
        )

    def values(self):
        return [x.decode() for x in self.genes]

    def gather(self):
        return np.concatenate([gene.data for gene in self.genes])

    def create(self, new_data):
        return Chromosome(
            instance=self,
            data=new_data,
        )


def run_config(config):
    _title = config.pop('title', '')
    _iterations = config.pop('iterations', 0)

    xaxis = np.array(range(config['generations']))
    bsf = np.array([0])
    onl = np.array([0])
    off = np.array([0])

    for i in xrange(_iterations):
        print 'Iteration %d' % i
        ga = GeneticAlgorithm(**config)
        ga.run()
        bsf = np.array(ga.best_so_far_data) + bsf
        onl = np.array(ga.online_data) + onl
        off = np.array(ga.offline_data) + off

    fig = plt.figure()
    fig.suptitle(_title, fontsize=20)
    plt.plot(
        xaxis, bsf / float(_iterations), 'g-',
        xaxis, onl / float(_iterations), 'r-',
        xaxis, off / float(_iterations), 'b-'
    )

    plt.plot(
        xaxis, bsf / float(_iterations), 'g-', label='Best so far')
    plt.plot(
        xaxis, onl / float(_iterations), 'r-', label='Online')
    plt.plot(
        xaxis, off / float(_iterations), 'b-', label='Offline')

    plt.legend(loc='lower right', shadow=True, fontsize='small')
    plt.xlabel('Generations')
    plt.ylabel('Function Value')
    # plt.show()
    fig.savefig(_title + '.jpg')


if __name__ == '__main__':

    configs = PROCESAR5
    for conf in configs:
        run_config(conf)
