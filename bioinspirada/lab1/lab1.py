# -*- coding: utf-8 -*-
import numpy as np
import random
from math import log

TEST_FILES = dict(
    SPANISH_TEXTS=[
        'tests/spanish1.txt',
        'tests/spanish2.txt',
    ],
    LIPOGRAM_TEXTS=[
        'tests/lipogram1.txt',
        'tests/lipogram2.txt',
    ],
    RANDOM_TEXTS=[
        'tests/random1.txt',
        'tests/random2.txt',
    ],
    SEMI_RANDOM_TEXTS=[
        'tests/srandom1.txt',
        'tests/srandom2.txt',
    ],
    PERMUTED_TEXTS=[
        'tests/permuted1.txt',
        'tests/permuted2.txt',
    ],
)

TEXT_SIZE = 7584

ALLOWED_CHARS = (
    ' ',
    'á',
    'é',
    'í',
    'ó',
    'ú',
    'ñ',
)

class EntropyMeasures(object):

    @staticmethod
    def shannon(probability_array):
        """
        Calculates the Shannon Measure from an array
        """
        result = 0
        for p in probability_array:
            if p:
                result -= p * log(p, 2)
        return result


    @staticmethod
    def hartley(probability_array):
        """
        Calculates the Hartley Measure from an array
        """
        return log(probability_array.size, 2)


class EntropyUtils(object):

    @staticmethod
    def file_to_list(filename):
        """
        Reads the filename and store all the valid characters in a list
        Also convert all characters to lowercase
        """
        data = ''
        with open(filename) as f:
            content = f.readlines()
            data = data.join(content)
            data = data.lower()
        cleaned = []
        for char in data:
            if char in ALLOWED_CHARS or (
                    ord(char) >= ord('a') and ord(char) <= ord('z')):
                cleaned.append(char)
        return cleaned

    @staticmethod
    def get_data(data):
        """
        Receives a list of characters
        It calculates the frequency, probabilities and other useful information
        and store it in a dictionary
        """
        base_chars = set(data)
        counter = dict(zip(base_chars, [0] * len(base_chars)))
        for c in data:
            counter[c] += 1
        keys = []
        values = []
        for key in counter:
            keys.append(key)
            values.append(counter[key])
        values = np.array(values)
        return dict(
            original_data=data,
            keys=keys,
            frequencies=values,
            probabilities=values / float(values.sum()),
            count=values.size,
            size=len(data),
        )

    @staticmethod
    def standard_choices():
        """
        Generates a list of valid characters
        """
        return list(ALLOWED_CHARS) + [
            chr(x) for x in xrange(ord('a'), ord('z') + 1)]

    @staticmethod
    def weighted_choices(ddata):
        """
        Generates a list of valid weighted character choices based on ddata
        """
        result = []
        for idx, val in enumerate(ddata['probabilities'] * 1000):
            result += [ddata['keys'][idx]] * val
        return result

    @staticmethod
    def generate_data(filename, size, choices):
        """
        Generates a random text based on the choices and saves it 
        """
        result = [random.choice(choices) for x in xrange(size)]
        with open(filename, "w") as txt:
            txt.write(''.join(result))
        return EntropyUtils.get_data(result)

    @staticmethod
    def generate_permuted_data(filename, ddata):
        """
        Generates a permuted text based on the choices and saves it
        """
        result = list(ddata['original_data'])
        random.shuffle(result)
        with open(filename, "w") as txt:
            txt.write(''.join(result))
        return EntropyUtils.get_data(result)


class EntropyProcessing(object):

    @staticmethod
    def show_entropy(ddata):
        """
        Show the Shannon and Hartley of a given ddata 
        """
        print 'SHANNON:', EntropyMeasures.shannon(ddata['probabilities'])
        print 'HARTLEY:', EntropyMeasures.hartley(ddata['probabilities'])

    @staticmethod
    def file_entropy(fn, verbose=True):
        """
        Calculates the entropy of a txt file 
        """
        ddata = EntropyUtils.get_data(
            EntropyUtils.file_to_list(fn)
        )
        if verbose:
            EntropyProcessing.show_entropy(ddata)
        return ddata

    @staticmethod
    def random_entropy(fn, size, verbose=True):
        """
        Generates a random text, saves it and calculate the entropy
        """
        ddata = EntropyUtils.generate_data(
            fn, size, EntropyUtils.standard_choices()
        )
        if verbose:
            EntropyProcessing.show_entropy(ddata)
        return ddata

    @staticmethod
    def semi_random_entropy(fn, size, ddata, verbose=True):
        """
        Generates a random text based on the weighted choices
        Save it and calculate the entropy
        """
        ddata = EntropyUtils.generate_data(
            fn, size, EntropyUtils.weighted_choices(ddata)
        )
        if verbose:
            EntropyProcessing.show_entropy(ddata)
        return ddata

    @staticmethod
    def permutation(fn, ddata, verbose=True):
        """
        Generates a permuted text based on the given ddata
        Save it and calculate the entropy
        """
        ddata = EntropyUtils.generate_permuted_data(
            fn, ddata 
        )
        if verbose:
            EntropyProcessing.show_entropy(ddata)
        return ddata


class Exercises(object):

    @staticmethod
    def exercise2():
        print "<<< EXERCISE 2 >>>"
        for fn in TEST_FILES['SPANISH_TEXTS']:
            print '===== %s =====' % fn
            EntropyProcessing.file_entropy(fn)

    @staticmethod
    def exercise3():
        print "<<< EXERCISE 3 >>>"
        for fn in TEST_FILES['LIPOGRAM_TEXTS']:
            print '===== %s =====' % fn
            EntropyProcessing.file_entropy(fn)

    @staticmethod
    def exercise4():
        print "<<< EXERCISE 4 >>>"
        for fn in TEST_FILES['RANDOM_TEXTS']:
            print '===== %s =====' % fn
            EntropyProcessing.random_entropy(fn, TEXT_SIZE)
        for fn in TEST_FILES['SEMI_RANDOM_TEXTS']:
            tmp = EntropyProcessing.file_entropy(
                random.choice(TEST_FILES['SPANISH_TEXTS']), False
            )
            print '===== %s =====' % fn
            EntropyProcessing.semi_random_entropy(
                fn, TEXT_SIZE, tmp
            )

    @staticmethod
    def exercise5():
        print "<<< EXERCISE 5 >>>"
        for fn in TEST_FILES['PERMUTED_TEXTS']:
            tmp_fn = random.choice(TEST_FILES['SPANISH_TEXTS'])
            print '===== %s =====' % tmp_fn
            tmp_ddata = EntropyProcessing.file_entropy(tmp_fn)
            print '===== %s =====' % fn
            EntropyProcessing.permutation(
                fn, tmp_ddata
            )



if __name__ == '__main__':
    Exercises.exercise2()
    # <<< EXERCISE 2 >>>
    # ===== tests/spanish1.txt =====
    # SHANNON: 4.01611969223
    # HARTLEY: 4.75488750216
    # ===== tests/spanish2.txt =====
    # SHANNON: 4.04306254199
    # HARTLEY: 4.75488750216

    Exercises.exercise3()
    # <<< EXERCISE 3 >>>
    # ===== tests/lipogram1.txt =====
    # SHANNON: 3.88039943575
    # HARTLEY: 4.64385618977
    # ===== tests/lipogram2.txt =====
    # SHANNON: 3.87524188079
    # HARTLEY: 4.58496250072

    Exercises.exercise4()
    # <<< EXERCISE 4 >>>
    # ===== tests/random1.txt =====
    # SHANNON: 5.04167776864
    # HARTLEY: 5.04439411936
    # ===== tests/random2.txt =====
    # SHANNON: 5.04057133766
    # HARTLEY: 5.04439411936
    # ===== tests/srandom1.txt =====
    # SHANNON: 3.99394745599
    # HARTLEY: 4.75488750216
    # ===== tests/srandom2.txt =====
    # SHANNON: 3.99077896911
    # HARTLEY: 4.75488750216

    Exercises.exercise5()
    # <<< EXERCISE 5 >>>
    # ===== tests/spanish1.txt =====
    # SHANNON: 4.01611969223
    # HARTLEY: 4.75488750216
    # ===== tests/permuted1.txt =====
    # SHANNON: 4.01611969223
    # HARTLEY: 4.75488750216
    # ===== tests/spanish2.txt =====
    # SHANNON: 4.04306254199
    # HARTLEY: 4.75488750216
    # ===== tests/permuted2.txt =====
    # SHANNON: 4.04306254199
    # HARTLEY: 4.75488750216
