import random
import pprint


class Markov(object):

    def __init__(self, size, *args, **kwargs):
        self.size = size
        self.alfa = kwargs.pop('alfa', 0.5)
        self.gamma = kwargs.pop('gamma', 0.5)
        self.iterations = kwargs.pop('iterations', size * size)
        self.end_state = (size - 1, size - 1)
        self.memory = dict()

    def update_q(self, start, end, new_val):
        if self.get_q(start, end) < new_val:
            self.memory[(start, end)] = new_val

    def get_q(self, start, end):
        return self.memory.get((start, end), 0)

    def get_r(self, start, end):
        if end == self.end_state:
            return 1
        return 0

    def calculate_q(self, start, end):
        old_q = self.get_q(start, end)
        reward = self.get_r(start, end)
        new_q = (
            old_q + self.alfa * (
                reward +
                self.gamma * self.max_next_state(end) -
                old_q
            )
        )
        self.update_q(start, end, new_q)
        return new_q

    def get_next_states(self, current):
        x, y = current
        return [
            ((x - 1 + self.size) % self.size, y),  # Left
            ((x + 1 + self.size) % self.size, y),  # Right
            (x, (y + 1) % self.size),              # Down
            (x, (y - 1) % self.size),              # Up
        ]

    def max_next_state(self, current):
        choices = self.get_next_states(current)
        values = map(lambda x: self.get_q(current, x), choices)
        values.sort()
        return values[-1]

    def choose_next_state(self, current):
        choices = self.get_next_states(current)
        choices.sort(key=lambda x: self.calculate_q(current, x))
        if self.calculate_q(current, choices[-1]):
            return choices[-1]
        return random.choice(choices)

    def run(self):
        states = []
        current_state = (
            random.randint(0, self.size - 1),
            random.randint(0, self.size - 1)
        )
        for x in xrange(self.iterations):
            if current_state == self.end_state:
                return states
            current_state = self.choose_next_state(current_state)
            states.append(current_state)
        return states


if __name__ == '__main__':
    SIZE = 10
    MAX_ITER = 1000
    ITERATIONS = 100
    ALFA = 0.75
    GAMMA = 0.25
    test = Markov(
        size=SIZE,
        iterations=MAX_ITER,
        alfa=ALFA,
        gamma=GAMMA)
    for x in xrange(ITERATIONS):
        print '<<<<< TRY %d >>>>>' % x
        print 'STEPS'
        pprint.pprint(test.run())
        print 'Q MATRIX'
        pprint.pprint(test.memory)
        print '==================\n'
