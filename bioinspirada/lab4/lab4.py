import random

import matplotlib.pyplot as plt
import numpy as np

from constants import (
    CONF,
)


class GeneticAlgorithm(object):

    def __init__(self, objective, gen_params, *args, **kwargs):
        self.objective = objective
        self.gen_params = gen_params
        self.population_size = kwargs.pop('p_size', 100)
        self.mutation_rate = kwargs.pop('m_rate', 0.08)
        self.crossover_rate = kwargs.pop('co_rate', 0.65)
        self.generations = kwargs.pop('generations', 40)
        self.selection_method = kwargs.pop('s_method', 'roulette')
        self.crossover_method = kwargs.pop('c_method', 'arithmetic_crossover')
        self.mutation_method = kwargs.pop('m_method', 'uniform_mutation')
        self.elitism = kwargs.pop('elitism', 0)
        self.normalize = kwargs.pop('normalize', False)
        self.vmin = kwargs.pop('v_min', 0)
        self.vmax = kwargs.pop('v_max', 100)
        self.no_uniform_mutation_beta = kwargs.pop('numut_beta', 2)
        self.gidx = 0
        self.population = self.make_initial_population()
        self.average_data = []
        self.best_so_far_data = []
        self.online_data = []
        self.offline_data = []

    def __str__(self):
        return str(self.population)

    def show(self):
        result = ''
        for c in self.population:
            result += str(c)
        return '[\n%s]' % result

    def make_initial_population(self):
        return [
            Chromosome(data=self.gen_params, objective=self.objective)
            for x in xrange(self.population_size)
        ]

    def calc_weighted_probability(self):
        self.population.sort(key=lambda c: c.evaluate())
        if self.normalize:
            partial = []
            for i in xrange(1, self.population_size + 1):
                partial.append(float(
                    self.vmin + (i - 1) * (self.vmax - self.vmin) / float(
                        self.population_size - 1
                    )
                ))
            partial = np.array(partial)
        else:
            partial = np.array([c.evaluate() for c in self.population])
            partial = partial + abs(partial.min())
        total = abs(partial).sum()
        self.weighted_probability = partial / total

        return self.weighted_probability

    def get_weighted_probability(self):
        return self.weighted_probability

    def select_chromosome_roulette(self):
        return np.random.choice(
            self.population,
            1,
            p=self.get_weighted_probability()
        )[0]

    def select_chromosome_tournament(self):
        cs = random.sample(self.population, 2)
        if cs[0].evaluate() > cs[1].evaluate():
            return cs[0]
        return cs[1]

    def select_chromosome(self):
        if self.selection_method == 'tournament':
            return self.select_chromosome_tournament()
        return self.select_chromosome_roulette()

    def make_children(self, c1, c2):
        if self.crossover_method == 'arithmetic_crossover':
            result = c1.arithmetic_crossover(
                c2
            )
        elif self.crossover_method == 'simple_crossover':
            result = c1.simple_crossover(
                c2,
                random.randint(0, len(c1.genes) - 1)
            )
        else:
            result = c1.heuristic_crossover(
                c2,
                random.randint(0, len(c1.genes) - 1)
            )
        return [
            c.create(res)
            for c, res in zip([c1, c2], result)
        ]

    def mutate(self, c):
        if self.mutation_method == 'uniform_mutation':
            result = c.uniform_mutation()
        elif self.mutation_method == 'frontier_mutation':
            result = c.frontier_mutation()
        else:
            result = c.no_uniform_mutation()
        return result

    def gen_child(self):
        while True:
            c1 = self.select_chromosome()
            c2 = self.select_chromosome()
            while id(c1) == id(c2):
                c2 = self.select_chromosome()

            parents = [c1, c2]
            if random.uniform(0, 1) < self.crossover_rate:
                children = self.make_children(*parents)
            else:
                children = parents

            for c in children:
                if random.uniform(0, 1) < self.mutation_rate:
                    yield c.create(self.mutate(c))
                else:
                    yield c

    def best_so_far(self):
        self.best_so_far_data.append(
            max(self.population, key=lambda c: c.evaluate()).evaluate()
        )

    def online(self, t):
        self.average_data.append(
            sum(
                map(lambda c: c.evaluate(), self.population)
            ) / float(self.population_size)
        )
        self.online_data.append(
            (1 / float(t + 1)) * (
                sum(self.average_data[:t + 1])
            )
        )

    def offline(self, t):
        self.offline_data.append(
            (1 / float(t + 1)) * (
                sum(self.best_so_far_data[:t + 1])
            )
        )

    def run(self):
        for gidx in xrange(self.generations):
            print '\tGeneration %d' % gidx
            self.gidx = gidx
            new_population = []
            if self.selection_method == 'roulette':
                self.calc_weighted_probability()

            if self.elitism:
                values = np.array([c.evaluate() for c in self.population])
                for cidx in np.argsort(-values)[:self.elitism]:
                    new_population.append(self.population[cidx])

            child_generator = self.gen_child()
            for cidx in xrange(self.population_size - self.elitism):
                new_population.append(
                    next(child_generator)
                )

            self.population = new_population
            self.best_so_far()
            self.online(gidx)
            self.offline(gidx)


class GeneticFunctions(object):

    def __init__(self, *args, **kwargs):
        super(GeneticFunctions, self).__init__(*args, **kwargs)

    def uniform_mutation(self):
        index = random.randint(0, len(self.genes) - 1)
        result = list(self.genes)
        result[index] = random.uniform(
            *self.intervals[index]
        )
        return result

    def frontier_mutation(self):
        index = random.randint(0, len(self.genes) - 1)
        result = list(self.genes)
        result[index] = random.choice(
            self.intervals[index]
        )
        return result

    def no_uniform_mutation(self):
        index = random.randint(0, len(self.genes) - 1)
        result = list(self.genes)

        def delta(t, y):
            return y * random.uniform(0, 1) * (
                1 - t / float(self.generations)
            ) ** self.numut_beta

        if random.choice((True, False)):
            result[index] += delta(
                self.gidx, self.intervals[index][1] - result[index]
            )
        else:
            result[index] -= delta(
                self.gidx, result[index] - self.intervals[index][0]
            )
        return result

    def arithmetic_crossover(self, obj):
        x1 = np.array(self.genes)
        x2 = np.array(obj.genes)
        alpha = random.uniform(0, 1)
        result = (
            list(alpha * x1 + (1 - alpha) * x2),
            list(alpha * x2 + (1 - alpha) * x1)
        )
        return result

    def simple_crossover(self, obj, position):
        leftx1 = self.genes[:position]
        leftx2 = obj.genes[:position]
        rightx1 = obj.genes[position:]
        rightx2 = self.genes[position:]
        alpha = 1
        while alpha < 0.01:
            new_rightx1 = [
                yk * alpha + xk * (1 - alpha)
                for xk, yk in zip(rightx1, rightx2)
            ]
            new_rightx2 = [
                xk * alpha + yk * (1 - alpha)
                for xk, yk in zip(rightx1, rightx2)
            ]
            for idx, (xk, yk) in enumerate(zip(new_rightx1, new_rightx2)):
                left, right = self.intervals[position + idx]
                if not (left <= xk <= right and left <= yk <= right):
                    # alpha reduction rate
                    alpha -= alpha / 10.0
                    break
            else:
                alpha = 0

        if alpha == 0:
            new_rightx1 = self.genes
            new_rightx2 = obj.genes

        result = (
            leftx1 + new_rightx1,
            leftx2 + new_rightx2,
        )
        return result

    def heuristic_crossover(self, obj, position):
        # FUTURE: add if maximizer or minimizer condition
        x1 = self.genes
        x2 = obj.genes
        if self.evaluate() > obj.evaluate():
            x2 = self.genes
            x1 = obj.genes
        x1 = np.array(x1)
        x2 = np.array(x2)
        x3 = []

        rho = 1
        while rho < 0.01:
            x3 = rho * (x2 - x1) + x2
            for idx, xk in enumerate(x3):
                left, right = self.intervals[position + idx]
                if not (left <= xk <= right):
                    # rho reduction rate
                    rho -= rho / 10.0
                    break
            else:
                rho = 0

        if rho == 0:
            x3 = self.genes

        return list(x3)

    def evaluate(self):
        return self.objective(*self.values())


class Chromosome(GeneticFunctions):

    def __init__(self, data, *args, **kwargs):
        instance = kwargs.pop('instance', None)
        if instance is None:
            self.objective = kwargs.pop('objective')
            self.intervals = data
            self.genes = [
                random.uniform(left, right) for left, right in data
            ]
        else:
            self.objective = instance.objective
            self.intervals = instance.intervals
            self.genes = list(data)
        super(Chromosome, self).__init__(*args, **kwargs)

    def __str__(self):
        return ' %f: [\n%s ],\n' % (
            self.evaluate(),
            ', '.join(self.genes)[1:],
        )

    def __repr__(self):
        return '<Chromosome: %f %s>' % (
            self.evaluate(),
            str(self.genes)
        )

    def values(self):
        return tuple(self.genes)

    def create(self, new_data):
        return Chromosome(
            instance=self,
            data=new_data,
        )


def run_config(config):
    _title = config.pop('title', '')
    _iterations = config.pop('iterations', 0)

    xaxis = np.array(range(config['generations']))
    bsf = np.array([0])
    onl = np.array([0])
    off = np.array([0])

    for i in xrange(_iterations):
        print 'Iteration %d' % i
        ga = GeneticAlgorithm(**config)
        ga.run()
        bsf = np.array(ga.best_so_far_data) + bsf
        onl = np.array(ga.online_data) + onl
        off = np.array(ga.offline_data) + off

    fig = plt.figure()
    fig.suptitle(_title, fontsize=20)
    plt.plot(
        xaxis, bsf / float(_iterations), 'g-',
        xaxis, onl / float(_iterations), 'r-',
        xaxis, off / float(_iterations), 'b-'
    )

    plt.plot(
        xaxis, bsf / float(_iterations), 'g-', label='Best so far')
    plt.plot(
        xaxis, onl / float(_iterations), 'r-', label='Online')
    plt.plot(
        xaxis, off / float(_iterations), 'b-', label='Offline')

    plt.legend(loc='lower right', shadow=True, fontsize='small')
    plt.xlabel('Generations')
    plt.ylabel('Function Value')
    # plt.show()
    fig.savefig(_title + '.jpg')


if __name__ == '__main__':

    configs = CONF
    for conf in configs:
        run_config(conf)
