import math


def obj2(x, y):
    return - x ** 2 - y ** 2


def obj(x, y):
    top = (math.sin(math.sqrt(x ** 2 + y ** 2))) ** 2 - 0.5
    bottom = (1.0 + 0.001 * (x ** 2 + y ** 2)) ** 2
    return 0.5 - top / bottom

#################
# 20 iterations #
#################

CONF = [
    # pc=65% pm=0.8%
    dict(
        objective=obj,
        gen_params=[(-50, 50), (-50, 50)],
        s_method='roulette',
        c_method='arithmetic_crossover',
        m_method='uniform_mutation',
        p_size=100,
        generations=40,
        co_rate=0.65,
        m_rate=0.008,
        elitism=0,
        normalize=False,
        v_min=0,
        v_max=2000,
        title='One Point p=100 g=40 pc=65% pm=0.8%',
        iterations=20
    ),
]
