#define GLUT_DISABLE_ATEXIT_HACK
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <GL/glut.h>
using namespace std;

/**
 * Author
 *   Johann Nunez Garcia
**/

#define RED 0
#define GREEN 0
#define BLUE 0
#define ALPHA 1

#define ECHAP 27
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);

float ANGLE = 0.0f;
float ANGLE_THORUS = 0.0f;
float POSITION = 0.0f;
float INCREMENT = 0.1f;

float ANGLE_EARTH = 0.0f;
float ANGLE_MOON = 0.0f;
float ANGLE_MARS = 0.0f;
float ROTATE_SUN = 0.0f;
float ROTATE_EARTH = 0.0f;
float ROTATE_MOON = 0.0f;
float ROTATE_MARS = 0.0f;

void ex1() {
  ANGLE += 1.0f;
  if (ANGLE > 360) {
    ANGLE = 0.0f;
  }
  glRotatef(ANGLE, 0, 0, 1);
  glTranslatef(10.0f, 0, 0);
  glutSolidTeapot(1);
}

void ex2() {
  if ((POSITION > 8) || (POSITION < -8)) {
    INCREMENT *= -1;
  }
  POSITION += INCREMENT;
  glTranslatef(POSITION, 0, 0);
  glutSolidSphere(0.5f, 8, 8);

  ex1();
}

void ex3() {
  ANGLE_THORUS += 3.0f;
  if (ANGLE_THORUS > 360) {
    ANGLE_THORUS = 0.0f;
  }
  if ((POSITION > 11) || (POSITION < -11)) {
    INCREMENT *= -1;
  }
  POSITION += INCREMENT;
  glTranslatef(POSITION, 0, 0);
  glutSolidSphere(0.5f, 8, 8);

  glPushMatrix();
    glRotatef(ANGLE_THORUS, 1, 0, 0);
    glTranslatef(0, 0, 4);
    glutSolidCube(2);
  glPopMatrix();

  ex1();
  glRotatef(ANGLE_THORUS, 0, 1, 0);
  glTranslatef(0, 0, 3);
  glutSolidTorus(0.2f, 0.5f, 2.0f, 50);
}

void ex4() {
  float SPEED = 1.0f;

  ANGLE_EARTH += SPEED;
  if (ANGLE_EARTH > 360) {
    ANGLE_EARTH = 0.0f;
  }
  ANGLE_MOON += 2 * SPEED;
  if (ANGLE_MOON > 360) {
    ANGLE_MOON = 0.0f;
  }
  ANGLE_MARS += SPEED;
  if (ANGLE_MARS > 360) {
    ANGLE_MARS = 0.0f;
  }

  ROTATE_SUN += SPEED;
  if (ROTATE_SUN > 360) {
    ROTATE_SUN = 0.0f;
  }
  ROTATE_EARTH += 3 * SPEED;
  if (ROTATE_EARTH > 360) {
    ROTATE_EARTH = 0.0f;
  }
  ROTATE_MOON += 1.5f * SPEED;
  if (ROTATE_MOON > 360) {
    ROTATE_MOON = 0.0f;
  }
  ROTATE_MARS += SPEED;
  if (ROTATE_MARS > 360) {
    ROTATE_MARS = 0.0f;
  }

  glPushMatrix();
    // SUN
    glRotatef(ROTATE_SUN, 0, 1, 0);
    glutSolidSphere(2.0f, 32, 32);
  glPopMatrix();

  glPushMatrix();
    // EARTH
    glRotatef(ANGLE_EARTH, 0, 1, 0);
    glTranslatef(10, 0, 0);
    glPushMatrix();
      glRotatef(ROTATE_EARTH, 0, 1, 0);
      glutSolidSphere(1.0f, 8, 8);
    glPopMatrix();

    // MOON
    glRotatef(ANGLE_MOON, 0, 1, 0);
    glTranslatef(2, 0, 0);
    glRotatef(ROTATE_MOON, 0, 1, 0);
    glutSolidSphere(0.25f, 8, 8);
  glPopMatrix();


  glPushMatrix();
    // MARS
    glRotatef(ANGLE_MARS, 0, 1, 0);
    glTranslatef(18, 0, 0);
    glRotatef(ROTATE_MARS, 0, 1, 0);
    glutSolidSphere(1.0f, 8, 8);
  glPopMatrix();

}

//function called on each frame
GLvoid window_idle();

int main(int argc, char **argv)
{
  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);


  glutInitWindowSize(800, 800);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("TP 2 : Transformaciones");


  initGL();
  init_scene();

  glutDisplayFunc(&window_display);

  glutReshapeFunc(&window_reshape);

  glutKeyboardFunc(&window_key);

  //function called on each frame
  glutIdleFunc(&window_idle);

  glutMainLoop();

  return 1;
}



GLvoid initGL()
{
  GLfloat position[] = { 0.0f, 5.0f, 10.0f, 0.0 };

  //enable light : try without it
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  glEnable(GL_LIGHTING);
  //light 0 "on": try without it
  glEnable(GL_LIGHT0);

  //shading model : try GL_FLAT
  glShadeModel(GL_SMOOTH);

  glEnable(GL_DEPTH_TEST);

  //enable material : try without it
  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  glClearColor(RED, GREEN, BLUE, ALPHA);
}



GLvoid window_display()
{

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);

  //glColor3d(255, 0, 0);
  glBegin(GL_LINES);
      glVertex3f(0, 0, 0);
      glVertex3f(0, 25, 0);
  glEnd();
  //glColor3d(0, 255, 0);
  glBegin(GL_LINES);
      glVertex3f(0, 0, 0);
      glVertex3f(25, 0, 0);
  glEnd();
  //glColor3d(0, 0, 255);
  glBegin(GL_LINES);
      glVertex3f(0, 0, 0);
      glVertex3f(0, 0, 25);
  glEnd();

  /* dibujar aqui */
  // ex1();
  // ex2();
  // ex3();
  ex4();

  glutSwapBuffers();

  glFlush();
}

GLvoid window_reshape(GLsizei width, GLsizei height)
{
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);

  glMatrixMode(GL_MODELVIEW);
}



void init_scene()
{

}

GLvoid window_key(unsigned char key, int x, int y)
{
  switch (key) {
    case ECHAP:
      exit(1);
      break;

    default:
      printf("La touche %d non active.\n", key);
      break;
  }
}


//function called on each frame
GLvoid window_idle()
{


  glutPostRedisplay();
}
