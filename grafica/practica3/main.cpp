#define GLUT_DISABLE_ATEXIT_HACK
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <GL/glut.h>
using namespace std;

/**
 * Author
 *   Johann Nunez Garcia
**/

#define RED 0
#define GREEN 0
#define BLUE 0
#define ALPHA 1

#define ECHAP 27
void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);
GLvoid callback_motion(int x, int y);
GLvoid callback_mouse(int button, int state, int x, int y);
GLvoid callback_special(int key, int x, int y);

//function called on each frame
GLvoid window_idle();

float RADIUS = 3;
float ANGLE = 0.0f;
float ANGLE_THORUS = 0.0f;
float POSITION = 0.0f;
float INCREMENT = 0.1f;

float ANGLE_EARTH = 0.0f;
float ANGLE_MOON = 0.0f;
float ANGLE_MARS = 0.0f;
float ROTATE_SUN = 0.0f;
float ROTATE_EARTH = 0.0f;
float ROTATE_MOON = 0.0f;
float ROTATE_MARS = 0.0f;

float TRANSLATE_X = 0;
float TRANSLATE_Y = 0;
float TRANSLATE_Z = 0;

float PREVIOUS_X = 0;
float PREVIOUS_Y = 0;

void draGizmo() {
  glColor3d(255, 0, 0);
  glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(25, 0, 0);
  glEnd();
  glColor3d(0, 255, 0);
  glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 25, 0);
  glEnd();
  glColor3d(0, 0, 255);
  glBegin(GL_LINES);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 25);
  glEnd();
  glColor3d(255, 255, 255);
}

void planetas() {
  float angle, x, y, z;
  float SPEED = 0.02f;

  ANGLE_EARTH += SPEED;
  if (ANGLE_EARTH > 360) {
    ANGLE_EARTH = 0.0f;
  }
  ANGLE_MOON += 2 * SPEED;
  if (ANGLE_MOON > 360) {
    ANGLE_MOON = 0.0f;
  }
  ANGLE_MARS += SPEED;
  if (ANGLE_MARS > 360) {
    ANGLE_MARS = 0.0f;
  }

  SPEED = 1.25f;

  ROTATE_SUN += SPEED;
  if (ROTATE_SUN > 360) {
    ROTATE_SUN = 0.0f;
  }
  ROTATE_EARTH += 3 * SPEED;
  if (ROTATE_EARTH > 360) {
    ROTATE_EARTH = 0.0f;
  }
  ROTATE_MOON += 1.5f * SPEED;
  if (ROTATE_MOON > 360) {
    ROTATE_MOON = 0.0f;
  }
  ROTATE_MARS += SPEED;
  if (ROTATE_MARS > 360) {
    ROTATE_MARS = 0.0f;
  }

  glPushMatrix();
    // SUN
    glRotatef(ROTATE_SUN, 0, 1, 0);
    glutSolidSphere(2.0f, 32, 32);
  glPopMatrix();

  glPushMatrix();
    // EARTH
    angle = ANGLE_EARTH;
    x = 10 * sin(angle);
    z = 10 * cos(angle);
    glTranslatef(x, 0, z);
    glPushMatrix();
      glRotatef(ROTATE_EARTH, 0, 1, 0);
    glPopMatrix();
    glutSolidSphere(1.0f, 8, 8);

    // MOON
    angle = ANGLE_MOON;
    x = 2 * sin(angle);
    z = 2 * cos(angle);
    glTranslatef(x, 0, z);
    glRotatef(ROTATE_MOON, 0, 1, 0);
    glutSolidSphere(0.25f, 8, 8);
  glPopMatrix();


  glPushMatrix();
    // MARS
    angle = ANGLE_MARS;
    x = 18 * sin(angle);
    z = 18 * cos(angle);
    glTranslatef(x, 0, z);
    glPushMatrix();
      glRotatef(ROTATE_MARS, 0, 1, 0);
    glPopMatrix();
    glutSolidSphere(1.0f, 8, 8);
  glPopMatrix();

}

void ex1() {
  draGizmo();
  glTranslatef(0, 0, -30);
  ANGLE += 0.05f;
  if (ANGLE > 360) {
    ANGLE = 0;
  }
  float angle = ANGLE;
  float x = RADIUS * cos(angle);
  float y = RADIUS * sin(angle);
  glTranslatef(x, y, 0);
  glutSolidTeapot(1);
}
void ex2() {
  glTranslatef(0, 0, -30);
  draGizmo();
  planetas();
}
void ex3() {
  float x = 10 * sin(ANGLE_EARTH);
  float z = 10 * cos(ANGLE_EARTH);
  gluLookAt(
    30, 30, 30,
    x, 0, z,
    0, 1, 0
  );
  draGizmo();
  planetas();
}
void ex4() {
  glTranslatef(0, 0, -30);
  glTranslatef(TRANSLATE_X, TRANSLATE_Y, TRANSLATE_Z);
  draGizmo();
}

int main(int argc, char **argv)
{
  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);


  glutInitWindowSize(800, 800);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("TP 2 : Transformaciones");

  initGL();
  init_scene();

  glutDisplayFunc(&window_display);

  glutReshapeFunc(&window_reshape);

  glutKeyboardFunc(&window_key);

  glutSpecialFunc(&callback_special);
  glutMouseFunc(&callback_mouse);
  glutMotionFunc(&callback_motion);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  //function called on each frame
  glutIdleFunc(&window_idle);

  glutMainLoop();

  return 1;
}



GLvoid initGL()
{
  GLfloat position[] = { 0.0f, 5.0f, 10.0f, 0.0 };

  //enable light : try without it
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  glEnable(GL_LIGHTING);
  //light 0 "on": try without it
  glEnable(GL_LIGHT0);

  //shading model : try GL_FLAT
  glShadeModel(GL_SMOOTH);

  glEnable(GL_DEPTH_TEST);

  //enable material : try without it
  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  glClearColor(RED, GREEN, BLUE, ALPHA);
}



GLvoid window_display()
{

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);
  gluPerspective(80, 4.0/3.0f, 10, 100);


  /* Descomentar el ejercicio a probar */
  //ex1();
  //ex2();
  //ex3();
  ex4();

  glutSwapBuffers();

  glFlush();
}

GLvoid window_reshape(GLsizei width, GLsizei height)
{
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  //glOrtho(-25.0f, 25.0f, -25.0f, 25.0f, -25.0f, 25.0f);
  gluPerspective(100, 16.0/9.0f, 10, 100);

  glMatrixMode(GL_MODELVIEW);
}

void init_scene()
{

}

GLvoid window_key(unsigned char key, int x, int y)
{
  switch (key) {
    case ECHAP:
      exit(1);
      break;

    default:
      printf("La touche %d non active.\n", key);
      break;
  }
}

//function called on each frame
GLvoid window_idle()
{
  glutPostRedisplay();
}

GLvoid callback_special(int key, int x, int y)
{
  switch (key)
  {
    case GLUT_KEY_UP:
      TRANSLATE_Z += 0.25;
      glutPostRedisplay();
      break;

    case GLUT_KEY_DOWN:
      TRANSLATE_Z -= 0.25f;
      glutPostRedisplay();
      break;

    case GLUT_KEY_LEFT:					
      TRANSLATE_X -= 0.1f;
      glutPostRedisplay();
      break;

    case GLUT_KEY_RIGHT:				
      TRANSLATE_X += 0.1f;
      glutPostRedisplay();
      break;
  }
}

GLvoid callback_mouse(int button, int state, int x, int y)
{
  if (state == GLUT_UP && button == GLUT_LEFT_BUTTON)
  {
    PREVIOUS_X = 0;
    PREVIOUS_Y = 0;
  }
}

GLvoid callback_motion(int x, int y)
{
  if (PREVIOUS_X) {
    TRANSLATE_X += (x - PREVIOUS_X) * 0.01f;
  }
  PREVIOUS_X = x;
  if (PREVIOUS_Y) {
    TRANSLATE_Y += (PREVIOUS_Y - y) * 0.01f;
  }
  PREVIOUS_Y = y;
  glutPostRedisplay();						
}
