#define GLUT_DISABLE_ATEXIT_HACK	
#include <math.h>
#include <stdio.h>
#include <GL/glut.h>

#define KEY_ESC 27

/**
 * Author
 *   Johann Nunez Garcia
**/

// ejercicio 1
void ex1(float center[], float size) {
  float left_up[] = {
    center[0] - size / 2, center[1] + size / 2
  };
  float left_bottom[] = {
    center[0] - size / 2, center[1] - size / 2
  };
  float right_bottom[] = {
    center[0] + size / 2, center[1] - size / 2
  };
  float right_top[] = {
    center[0] + size / 2, center[1] + size / 2
  };
  glBegin(GL_LINE_LOOP);
  glColor3d(255, 0, 0);
  glVertex2f(left_up[0], left_up[1]);
  glVertex2f(left_bottom[0], left_bottom[1]);
  glVertex2f(right_bottom[0], right_bottom[1]);
  glVertex2f(right_top[0], right_top[1]);
  glEnd();
}

// ejercicio 2
void ex2(float center[], float radius) {
  float x, y, theta, stop;
  glBegin(GL_LINE_LOOP);
  glColor3d(255, 250, 0);
  stop = 2 * M_PI;
  for (theta = 0; theta < stop;) {
    x = center[0] + radius * cos(theta);
    y = center[1] + radius * sin(theta);
    glVertex2f(x, y);
    theta += 0.1f;
  }
  glEnd();
}

// ejercicio 3
void ex3(int circles, float reduction) {
  float center[] = {0, 0};
  float radius = 45;
  for (int i = 0; i < circles; ++i) {
    ex2(center, radius);
    center[0] -= (radius * reduction);
    radius -= radius * reduction;
  }
}

// ejercicio 4
void ex4(int circles, float reduction) {
  float old_radius;
  float center[] = {-30, 0};
  float radius = 15;
  for (int i = 0; i < circles; ++i) {
    ex2(center, radius);
    old_radius = radius;
    radius -= radius * reduction;
    center[0] += old_radius + radius;
  }
}

// ejercicio 5
void ex5(int circles, float reduction, float theta) {
  float old_radius;
  float center[] = {-30, -30};
  float radius = 15;
  for (int i = 0; i < circles; ++i) {
    ex2(center, radius);
    old_radius = radius;
    radius -= radius * reduction;
    center[0] += (radius + old_radius) * cos(theta * M_PI / 180);
    center[1] += (radius + old_radius) * sin(theta * M_PI / 180);
  }
}

/**
 * Funcion llamada a cada imagen
**/
void glPaint(void) {

  // El fondo de la escena al color initial
  // (R, G, B, transparencia) en este caso un fondo negro
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glLoadIdentity();

  float center[2] = {0, 0};

  // Comentar o descomentar cada ejercicio para probar

  // ejercicio 1
  //ex1(center, 50);

  // ejercicio 2
  //ex2(center, 30);

  // ejercicio 3
  //ex3(5, 0.4f);

  // ejercicio 4
  //ex4(6, 0.3f);

  // ejercicio 5
  ex5(6, 0.30f, 45.0f);

  // doble buffer, mantener esta instruccion al fin de la funcion
  glutSwapBuffers();
}

/**
 * Inicializacion de OpenGL
**/
void init_GL(void) {
  // Color del fondo de la escena
  // (R, G, B, transparencia) en este caso un fondo negro
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  // modo projeccion
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
}

//en el caso que la ventana cambie de tamanio
GLvoid window_redraw(GLsizei width, GLsizei height) {
  glViewport(0, 0, width, height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glOrtho(-50.0f, 50.0f, -50.0f, 50.0f, -1.0f, 1.0f);
  // todas la informaciones previas se aplican al la matrice del ModelView
  glMatrixMode(GL_MODELVIEW);
}

GLvoid window_key(unsigned char key, int x, int y) {
  switch (key) {
    case KEY_ESC:
      exit(0);
      break;
    default:
      break;
  }
}

/**
 * Programa Principal
**/
int main(int argc, char** argv) {

  //Inicializacion de la GLUT
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(600, 600); //tamanio de la ventana
  glutInitWindowPosition(100, 100); //posicion de la ventana
  glutCreateWindow("TP1 OpenGL : hello_world_OpenGL"); //titulo de la ventana

  init_GL(); //funcion de inicializacion de OpenGL

  glutDisplayFunc(glPaint);
  glutReshapeFunc(&window_redraw);
  // Callback del teclado
  glutKeyboardFunc(&window_key);

  glutMainLoop(); //bucle de rendering

  return 0;
}
