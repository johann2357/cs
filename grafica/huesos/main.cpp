#define GLUT_DISABLE_ATEXIT_HACK    

#include <iostream>
#include <math.h>
#include <GL/glut.h>
#include "constants.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "App.h"

App app;

void glPaint(void) {
  app.glPaint();
}

GLvoid window_reshape(GLsizei width, GLsizei height) {
  app.window_reshape(width, height);
}

GLvoid window_key(unsigned char key, int x, int y) {
  app.window_key(key, x, y);
}

GLvoid window_idle() {
  app.window_idle();
}

GLvoid callback_special(int key, int x, int y) {
  app.callback_special(key, x, y);
}

GLvoid callback_mouse(int button, int state, int x, int y) {
  app.callback_mouse(button, state, x, y);
}

GLvoid callback_motion(int x, int y) {
  app.callback_motion(x, y);
}

void runTests() {
  int i = 176;

  cv::Mat image = cv::imread(
    FILES[i],
    CV_LOAD_IMAGE_COLOR
  );
  if(! image.data ) {
    printf("Could not open or find the image %s\n", FILES[i].c_str());
    return;
  }

  for (int row = 0; row < image.rows; ++row) {
    uchar *ptr = image.ptr(row);
    for (int col = 0; col < image.cols; ++col) {
      if (ptr[0] && ptr[1] && ptr[2]) {
        ptr[0] = 255;
        ptr[1] = 255;
        ptr[2] = 255;
      }
      ptr += 3;
    }
  }

  cv::Mat cannyH;
  cv::Canny(image, cannyH, 100, 50);

  cv::namedWindow("CANNY BEFORE", cv::WINDOW_AUTOSIZE);
  cv::imshow("CANNY BEFORE", cannyH);

  for (int row = 0; row < cannyH.rows; ++row) {
    uchar *ptr = cannyH.ptr(row);
    for (int col = 0; col < cannyH.cols; ++col) {
      if (ptr[0] && ptr[1] && ptr[2]) {
        ptr[0] = 255;
        ptr[1] = 255;
        ptr[2] = 255;
      }
      ptr += 3;
    }
  }

  cv::namedWindow("ORIGINAL", cv::WINDOW_AUTOSIZE);
  cv::imshow("ORIGINAL", image);
  cv::namedWindow("CANNY", cv::WINDOW_AUTOSIZE);
  cv::imshow("CANNY", cannyH);
  cv::waitKey(0);
}

void runApp(int argc, char** argv) {
  glutInit(&argc, argv);
  app.init();
  glutDisplayFunc(glPaint);
  glutReshapeFunc(&window_reshape);
  glutKeyboardFunc(&window_key);
  glutSpecialFunc(&callback_special);
  glutMouseFunc(&callback_mouse);
  glutMotionFunc(&callback_motion);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glutIdleFunc(&window_idle);
  glutMainLoop();
}

int main(int argc, char** argv) {
  runApp(argc, argv);
  //runTests();
  return 0;
}
