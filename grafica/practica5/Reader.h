#ifndef READER
#define READER

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include "constants.h" 
#include "utils.h" 

struct Reader {
  /**
   * Just read the file and put every line in a vector
  **/
  static std::vector<std::string> line_by_line(std::string fn) {
    std::vector<std::string> lines;
    std::ifstream file(fn.c_str(), std::ifstream::in);
    std::string line; 
    while (std::getline(file, line)) {
      std::istringstream iss(line);
      lines.push_back(line);
    }
    return lines;
  }
  
  /**
   * Just read the file and split every line in a vector of vector
  **/
  static std::vector< std::vector<std::string> > split_lines(std::string fn) {
    std::vector< std::vector<std::string> > result;
    std::vector<std::string> lines = Reader::line_by_line(fn);
    for (int i = 0; i < lines.size(); ++i) {
      result.push_back(Utils::split(Utils::reduce(lines[i]), ' '));
    }
    return result;
  };

};

#endif
