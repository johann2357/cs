#ifndef TERRAIN
#define TERRAIN

#include <stdlib.h>
#include <vector>

#include "Reader.h"
#include "constants.h"

typedef struct // Definicion de un punto del terreo
{
  GLfloat s, t; // Coordenadas de texturas
  GLfloat nx, ny, nz; // Coordenadas de la normal
  GLfloat x, hauteur, z; // posición del vértice
} TerrainPoint;

class Terrain
{
  public :
    Terrain() {
    };
    ~Terrain() {};
    // carga de un archivo de modelo digital de terreno
    bool load(std::string filename) {
      this->string_terrain = Reader::split_lines(filename);
      this->count = (
        (this->string_terrain.size() - 1) * this->string_terrain[1].size()
      );
      this->lista_puntos = new TerrainPoint[this->count];
      int idx = 0;
      for (int i = 1; i < this->string_terrain.size(); ++i) {
        for (int j = 0; j < this->string_terrain[i].size(); ++j) {
          this->lista_puntos[idx].x = i;
          this->lista_puntos[idx].hauteur = strtof(
            this->string_terrain[i][j].c_str(), NULL);
          this->lista_puntos[idx].z = j;
          ++idx;
        }
      }
    }
      // Visualizacion del terreno
    void display() {
      //glInterleavedArrays(GL_T2F_N3F_V3F, sizeof(TerrainPoint), &lista_puntos[0].s);
      glColor3d(0, 255, 0);
      glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, (void*)lista_indices);
      // for (int i = 1; i < this->count; ++i) {
      //   glColor3d(0, 255, 0);
      //   glBegin(GL_POINTS); // render with points
      //     glVertex3f(
      //       - this->lista_puntos[i].x * 50 * 0.1,
      //       this->lista_puntos[i].hauteur * 0.1,
      //       - this->lista_puntos[i].z * 50 * 0.1
      //     );
      //   glEnd();
      // }
    }
    // calcula las normales de cada vertice
    void computeNormals();
  private:
    int count;
    // quantidad de punto en X y Z
    int nb_pt_x, nb_pt_z;
    // distancia entre dos puntos segun X y Z
    float dist_x, dist_z;
    // Tabla que contiene los puntos del terreno
    TerrainPoint *lista_puntos;
    // Tabla que contiene los indicess
    GLuint *lista_indices ;
    // Matriz de puntos en string
    std::vector< std::vector<std::string> > string_terrain;
};

#endif
