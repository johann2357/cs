#include <GL/glut.h>
#include <stdio.h>
#include "TextureManager.h"
#include "Display.h"
#include "constants.h"

void Display::load() {
  this->terrain = new Terrain();
  this->terrain->load(TERRAIN_FILE);
}

void Display::show() {
  this->terrain->display();
}
