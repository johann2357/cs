#ifndef _DISPLAY_H
#define _DISPLAY_H

#include "Terrain.h"

class Display {
  Terrain* terrain;
 public:
  Display() {}
  ~Display() {}
  void load();
  void show();
};

#endif
