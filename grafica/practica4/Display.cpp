#include <GL/glut.h>
#include <stdio.h>
#include "TextureManager.h"
#include "Display.h"

void Display::load() {
  this->grass_texture = TextureManager::Inst()->LoadTexture(
    "grass.jpg",
    GL_BGR_EXT,
    GL_RGB
  );
  this->house_front_texture = TextureManager::Inst()->LoadTexture(
    "house_front.jpg",
    GL_BGR_EXT,
    GL_RGB
  );
  this->house_back_texture = TextureManager::Inst()->LoadTexture(
    "house_back.jpg",
    GL_BGR_EXT,
    GL_RGB
  );
  this->house_side_texture = TextureManager::Inst()->LoadTexture(
    "house_side.jpg",
    GL_BGR_EXT,
    GL_RGB
  );
  this->house_roof_texture = TextureManager::Inst()->LoadTexture(
    "roof.jpg",
    GL_BGR_EXT,
    GL_RGB
  );
  this->brick_texture = TextureManager::Inst()->LoadTexture(
    "brick.jpg",
    GL_BGR_EXT,
    GL_RGB
  );
}
void Display::show() {
  this->with_texture();
  // this->without_texture();
}

void Display::with_texture() {
  // PISO
  glBindTexture(GL_TEXTURE_2D, this->grass_texture);
  glBegin(GL_QUADS);
    glBegin(GL_QUADS);
      glTexCoord2f(0, 0);
    glVertex3d(-20, 0, -20);
      glTexCoord2f(0.0, 10);
    glVertex3d(-20, 0, 20);
      glTexCoord2f(10, 10);
    glVertex3d(20, 0, 20);
      glTexCoord2f(10, 0);
    glVertex3d(20, 0, -20);
  glEnd();

  // ARBOL
  glPushMatrix();
    glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
    glTranslatef(5, 0, -2);

    gluCylinder(
      gluNewQuadric(), 0.2f, 0.2f, 2.0f, 32, 32);

    gluSphere(
      gluNewQuadric(), 1.0f, 32, 32);
  glPopMatrix();


  // PARED ATRAS
  glBindTexture(GL_TEXTURE_2D, this->house_back_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(-2, 0, -1);
      glTexCoord2f(0.0, 1);
    glVertex3d(-2, 2, -1);
      glTexCoord2f(1, 1);
    glVertex3d(2, 2, -1);
      glTexCoord2f(1, 0.0);
    glVertex3d(2, 0, -1);
  glEnd();

  // PARED DELANTE
  glBindTexture(GL_TEXTURE_2D, this->house_front_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(2, 0, 1);
      glTexCoord2f(0.0, 1.0f);
    glVertex3d(2, 2, 1);
      glTexCoord2f(1.0, 1.0f);
    glVertex3d(-2, 2, 1);
      glTexCoord2f(1.0, 0.0);
    glVertex3d(-2, 0, 1);
  glEnd();

  // PARED IZQUIERDA
  glBindTexture(GL_TEXTURE_2D, this->house_side_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(-2, 2, 1);
      glTexCoord2f(8.0, 0.0);
    glVertex3d(-2, 2, -1);
      glTexCoord2f(8.0, 1.0f);
    glVertex3d(-2, 0, -1);
      glTexCoord2f(0.0, 1.0f);
    glVertex3d(-2, 0, 1);
  glEnd();

  // PARED DERECHA
  glBindTexture(GL_TEXTURE_2D, this->house_side_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(2, 0, 1);
      glTexCoord2f(8.0, 0.0);
    glVertex3d(2, 0, -1);
      glTexCoord2f(8.0, 1.0f);
    glVertex3d(2, 2, -1);
      glTexCoord2f(0.0, 1.0f);
    glVertex3d(2, 2, 1);
  glEnd();

  // TECHO IZQUIERDA
  glBindTexture(GL_TEXTURE_2D, this->house_side_texture);
  glBegin(GL_TRIANGLES);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(-2, 2, -1);
      glTexCoord2f(8.0, 0.0);
    glVertex3d(-2, 2, 1);
      glTexCoord2f(8.0, 1.0f);
    glVertex3d(-2, 3, 0);
  glEnd();

  // TECHO DERECHA
  glBindTexture(GL_TEXTURE_2D, this->house_side_texture);
  glBegin(GL_TRIANGLES);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(2, 2, 1);
      glTexCoord2f(8.0, 0.0);
    glVertex3d(2, 2, -1);
      glTexCoord2f(8.0, 1.0f);
    glVertex3d(2, 3, 0);
  glEnd();

  // CHIMENEA ATRAS
  glBindTexture(GL_TEXTURE_2D, this->brick_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3f(1, 2.75, -0.25);
      glTexCoord2f(0.0, 1.0f);
    glVertex3f(1, 3.5, -0.25);
      glTexCoord2f(8.0, 1.0f);
    glVertex3f(1.5, 3.5, -0.25);
      glTexCoord2f(8.0, 0.0);
    glVertex3f(1.5, 2.75, -0.25);
  glEnd();

  // CHIMENEA DELANTE
  glBindTexture(GL_TEXTURE_2D, this->brick_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3f(1.5, 2.75, 0.25);
      glTexCoord2f(0.0, 1.0f);
    glVertex3f(1.5, 3.5, 0.25);
      glTexCoord2f(8.0, 1.0f);
    glVertex3f(1, 3.5, 0.25);
      glTexCoord2f(8.0, 0.0);
    glVertex3f(1, 2.75, 0.25);
  glEnd();

  // CHIMENEA IZQUIERDA
  glBindTexture(GL_TEXTURE_2D, this->brick_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3f(1, 3.5, 0.25);
      glTexCoord2f(0.0, 1.0f);
    glVertex3f(1, 3.5, -0.25);
      glTexCoord2f(8.0, 1.0f);
    glVertex3f(1, 2.75, -0.25);
      glTexCoord2f(8.0, 0.0);
    glVertex3f(1, 2.75, 0.25);
  glEnd();

  // CHIMENEA DERECHA
  glBindTexture(GL_TEXTURE_2D, this->brick_texture);
  glBegin(GL_QUADS);
      glTexCoord2f(1, 0);
    glVertex3f(1.5, 2.75, -0.25);
      glTexCoord2f(1, 1);
    glVertex3f(1.5, 3.5, -0.25);
      glTexCoord2f(0, 1);
    glVertex3f(1.5, 3.5, 0.25);
      glTexCoord2f(0, 0);
    glVertex3f(1.5, 2.75, 0.25);
  glEnd();

  // CHIMENEA SUPERIOR
  glBegin(GL_QUADS);
    glVertex3d(1, 3.5, -0.25);
    glVertex3d(1, 3.5, 0.25);
    glVertex3d(1.5, 3.5, 0.25);
    glVertex3d(1.5, 3.5, -0.25);
  glEnd();

  // TECHO DELANTE
  glBindTexture(GL_TEXTURE_2D, this->house_roof_texture);
  glColor3d(0.4, 0.4, 0.4);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(2, 3, 0);
      glTexCoord2f(8.0, 0.0);
    glVertex3d(-2, 3, 0);
      glTexCoord2f(8.0, 1.0f);
    glVertex3d(-2, 2, 1);
      glTexCoord2f(0.0, 1.0f);
    glVertex3d(2, 2, 1);
  glEnd();

  // TECHO ATRAS
  glBindTexture(GL_TEXTURE_2D, this->house_roof_texture);
  glColor3d(0.2, 0.2, 0.2);
  glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);
    glVertex3d(-2, 2, -1);
      glTexCoord2f(0.0, 1.0f);
    glVertex3d(-2, 3, 0);
      glTexCoord2f(8.0, 1.0f);
    glVertex3d(2, 3, 0);
      glTexCoord2f(8.0, 0.0);
    glVertex3d(2, 2, -1);
  glEnd();
}

void Display::without_texture() {
  // Set material properties
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, this->qaGreen);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, this->qaGreen);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, this->qaWhite);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 20);

  // PISO
  glBegin(GL_QUADS);
    glVertex3d(-20, 0, -20);
    glVertex3d(-20, 0, 20);
    glVertex3d(20, 0, 20);
    glVertex3d(20, 0, -20);
  glEnd();

  // ARBOL
  glPushMatrix();
    glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
    glTranslatef(5, 0, -2);

    gluCylinder(
      gluNewQuadric(), 0.2f, 0.2f, 2.0f, 32, 32);

    gluSphere(
      gluNewQuadric(), 1.0f, 32, 32);
  glPopMatrix();

  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, this->qaRed);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, this->qaRed);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, this->qaWhite);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 20);

  // PARED ATRAS
  glBegin(GL_QUADS);
    glVertex3d(-2, 0, -1);
    glVertex3d(-2, 2, -1);
    glVertex3d(2, 2, -1);
    glVertex3d(2, 0, -1);
  glEnd();

  // PARED DELANTE
  glBegin(GL_QUADS);
    glVertex3d(2, 0, 1);
    glVertex3d(2, 2, 1);
    glVertex3d(-2, 2, 1);
    glVertex3d(-2, 0, 1);
  glEnd();

  // PARED IZQUIERDA
  glBegin(GL_QUADS);
    glVertex3d(-2, 2, 1);
    glVertex3d(-2, 2, -1);
    glVertex3d(-2, 0, -1);
    glVertex3d(-2, 0, 1);
  glEnd();

  // PARED DERECHA
  glBegin(GL_QUADS);
    glVertex3d(2, 0, 1);
    glVertex3d(2, 0, -1);
    glVertex3d(2, 2, -1);
    glVertex3d(2, 2, 1);
  glEnd();

  // TECHO IZQUIERDA
  glColor3d(0.4, 0, 0);
  glBegin(GL_TRIANGLES);
    glVertex3d(-2, 2, -1);
    glVertex3d(-2, 2, 1);
    glVertex3d(-2, 3, 0);
  glEnd();

  // TECHO DERECHA
  glColor3d(0.1, 0, 0);
  glBegin(GL_TRIANGLES);
    glVertex3d(2, 2, 1);
    glVertex3d(2, 2, -1);
    glVertex3d(2, 3, 0);
  glEnd();

  // CHIMENEA ATRAS
  glBegin(GL_QUADS);
    glVertex3f(1, 2.75, -0.25);
    glVertex3f(1, 3.5, -0.25);
    glVertex3f(1.5, 3.5, -0.25);
    glVertex3f(1.5, 2.75, -0.25);
  glEnd();

  // CHIMENEA DELANTE
  glBegin(GL_QUADS);
    glVertex3f(1.5, 2.75, 0.25);
    glVertex3f(1.5, 3.5, 0.25);
    glVertex3f(1, 3.5, 0.25);
    glVertex3f(1, 2.75, 0.25);
  glEnd();

  // CHIMENEA IZQUIERDA
  glBegin(GL_QUADS);
    glVertex3f(1, 3.5, 0.25);
    glVertex3f(1, 3.5, -0.25);
    glVertex3f(1, 2.75, -0.25);
    glVertex3f(1, 2.75, 0.25);
  glEnd();

  // CHIMENEA DERECHA
  glBegin(GL_QUADS);
    glVertex3f(1.5, 2.75, 0.25);
    glVertex3f(1.5, 2.75, -0.25);
    glVertex3f(1.5, 3.5, -0.25);
    glVertex3f(1.5, 3.5, 0.25);
  glEnd();

  // CHIMENEA SUPERIOR
  glBegin(GL_QUADS);
    glVertex3d(1, 3.5, -0.25);
    glVertex3d(1, 3.5, 0.25);
    glVertex3d(1.5, 3.5, 0.25);
    glVertex3d(1.5, 3.5, -0.25);
  glEnd();

  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, this->qaGrey);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, this->qaGrey);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, this->qaWhite);
  glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 20);

  // TECHO DELANTE
  glColor3d(0.4, 0.4, 0.4);
  glBegin(GL_QUADS);
    glVertex3d(2, 3, 0);
    glVertex3d(-2, 3, 0);
    glVertex3d(-2, 2, 1);
    glVertex3d(2, 2, 1);
  glEnd();

  // TECHO ATRAS
  glColor3d(0.2, 0.2, 0.2);
  glBegin(GL_QUADS);
    glVertex3d(-2, 2, -1);
    glVertex3d(-2, 3, 0);
    glVertex3d(2, 3, 0);
    glVertex3d(2, 2, -1);
  glEnd();
}
