#ifndef _DISPLAY_H
#define _DISPLAY_H

class Display {
  GLint grass_texture;
  GLint brick_texture;
  GLint house_roof_texture;
  GLint house_front_texture;
  GLint house_back_texture;
  GLint house_side_texture;
  GLfloat qaBrown[4] = {0.6, 0.36, 0.15, 1.0}; // Brown Color
  GLfloat qaGrey[4] = {0.5, 0.5, 0.5, 1.0}; // Grey Color
  GLfloat qaBlack[4] = {0.0, 0.0, 0.0, 1.0}; // Black Color
  GLfloat qaGreen[4] = {0.0, 1.0, 0.0, 1.0}; // Green Color
  GLfloat qaWhite[4] = {1.0, 1.0, 1.0, 1.0}; // White Color
  GLfloat qaRed[4] = {1.0, 0.0, 0.0, 1.0};   // Red Color
 public:
  Display() {}
  ~Display() {}
  void load();
  void show();
  void without_texture();
  void with_texture();
};

#endif
