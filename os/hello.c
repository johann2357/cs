#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>		/* Needed for the macros */

#define DRIVER_AUTHOR "Johann Nunez <johan2357@gmail.com>"
#define DRIVER_DESC   "Testing linux module"

static char *name = "Anonymous";
module_param(name, charp, 0000);
MODULE_PARM_DESC(name, "A character string");

static int hello_init(void) {
  printk(KERN_ALERT "TEST: Insertando el modulo\n");
  printk(KERN_INFO "TEST: Hola %s\n", name);
  return 0;
}

static void hello_exit(void) {
  printk(KERN_INFO "TEST: Chau %s\n", name);
  printk(KERN_ALERT "TEST: quitando el modulo\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);	/* Who wrote this module? */
MODULE_DESCRIPTION(DRIVER_DESC);	/* What does this module do */
