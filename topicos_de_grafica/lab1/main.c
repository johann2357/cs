#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bmp24.h"

#define IMAGEN_TRATADA "tratada.bmp"  // Ruta y nombre del archivo de la imagen de salida BMP
#define ITERATIONES_LUMINANCIA 600    // Numero de iteraciones para luminancia


void to_grayscale(BMP *imagen) {
  int i, j;
  for (i = 0; i < imagen->alto; i++) {
    for (j = 0; j < imagen->ancho; j++) {  
      // Conversión a escala de grises
      // unsigned char resultado = (unsigned char) (
      //   (
      //    imagen->pixel_r[i][j] +
      //    imagen->pixel_g[i][j] +
      //    imagen->pixel_b[i][j]
      //   ) / 3
      // );
      unsigned char luminancia = (unsigned char) (
        (imagen->pixel_r[i][j] * 0.299) +
        (imagen->pixel_g[i][j] * 0.587) +
        (imagen->pixel_b[i][j] * 0.114)
      );
      imagen->pixel_r[i][j] = luminancia;
      imagen->pixel_g[i][j] = luminancia;
      imagen->pixel_b[i][j] = luminancia;
    }
  }
}

void luminancia_multiple(BMP *imagen) {
  int i;
  for (i = 0; i < ITERATIONES_LUMINANCIA; i++) {
    to_grayscale(imagen);
  }
}

void espejo(BMP *imagen) {
  int i, j, opuesto;
  unsigned char tmp;
  printf("\n!!! %d !!!\n", imagen->ancho / 2);

  for (i = 0; i < imagen->alto; i++) {
    for (j = 0; j < imagen->ancho / 2; j++) {  
      opuesto = imagen->ancho - j - 1;
      // Red
      tmp = imagen->pixel_r[i][opuesto];
      imagen->pixel_r[i][opuesto] = imagen->pixel_r[i][j];
      imagen->pixel_r[i][j] = tmp;
      // Green
      tmp = imagen->pixel_g[i][opuesto];
      imagen->pixel_g[i][opuesto] = imagen->pixel_g[i][j];
      imagen->pixel_g[i][j] = tmp;
      // Blue
      tmp = imagen->pixel_b[i][opuesto];
      imagen->pixel_b[i][opuesto] = imagen->pixel_b[i][j];
      imagen->pixel_b[i][j] = tmp;
    }
  }
}

void reduccion(BMP *imagen) {
  int i, j;
  for (i = 0; i < imagen->alto / 2; i++) {
    for (j = 0; j < imagen->ancho / 2; j++) {
      // Red
      imagen->pixel_r[i][j] = imagen->pixel_r[i * 2][j * 2];
      // Green
      imagen->pixel_g[i][j] = imagen->pixel_g[i * 2][j * 2];
      // Blue
      imagen->pixel_b[i][j] = imagen->pixel_b[i * 2][j * 2];
    }
  }
  for (i = 0; i < imagen->alto / 2; i++) {
    for (j = imagen->ancho / 2; j < imagen->ancho; j++) {
      // Red
      imagen->pixel_r[i][j] = (unsigned char) (0);
      // Green
      imagen->pixel_g[i][j] = (unsigned char) (0);
      // Blue
      imagen->pixel_b[i][j] = (unsigned char) (0);
    }
  }
  for (i = imagen->alto / 2; i < imagen->alto; i++) {
    for (j = 0; j < imagen->ancho; j++) {
      // Red
      imagen->pixel_r[i][j] = (unsigned char) (0);
      // Green
      imagen->pixel_g[i][j] = (unsigned char) (0);
      // Blue
      imagen->pixel_b[i][j] = (unsigned char) (0);
    }
  }
}

// * - * - * - * - * -
// - - - - - - - - - -
// * - * - * - * - * -
// - - - - - - - - - -
// * - * - * - * - * -
// - - - - - - - - - -
// * - * - * - * - * -
// - - - - - - - - - -

int main(int argc, char* argv[]) {       
  BMP img;          // Estructura de tipo imágen
  char IMAGEN[45];  // Almacenará la ruta de la imagen

  //******************************************************************  
  // Si no se introdujo la ruta de la imagen BMP 
  //******************************************************************  
  // Si no se introduce una ruta de imágen
  if (argc != 2) {
    printf("\nIndique el nombre del archivo a codificar - Ejemplo: [user@equipo]$ %s imagen.bmp\n",argv[0]);
    exit(1);
  } 
  // Almacenar la ruta de la imágen
  strcpy(IMAGEN,argv[1]);

  //***************************************************************************************************************************
  // 0 Abrir la imágen BMP de 24 bits, almacenar su cabecera en la estructura img y colocar sus pixeles en el arreglo img.pixel[][]
  //*************************************************************************************************************************** 
  abrir_imagen(&img, IMAGEN);
  printf("\n***********************");
  printf("\nIMAGEN: %s", IMAGEN);
  printf("\n***********************");
  printf("\nDimensiones de la imágen:\tAlto=%d\tAncho=%d\n", img.alto, img.ancho);
  printf("Otros:\n");
  printf("\tTamanho=%d", img.tamano);
  printf("\tReservado=%d\n", img.reservado);
  printf("\tOffset=%d\n", img.offset);
  printf("\tTamanho metadatos=%d\n", img.tamanoMetadatos);
  printf("\tNum Planos=%d\n", img.numeroPlanos);
  printf("\tProf Color=%d\n", img.profundidadColor);
  printf("\tTipo Comp=%d\n", img.tipoCompresion);
  printf("\tPix x mh=%d\n", img.pxmh);
  printf("\tPix x mv=%d\n", img.pxmv);
  printf("\tColores usados=%d\n", img.coloresUsados);
  printf("\tColores importantes=%d\n", img.coloresImportantes);

  //*************************************************************
  // 1 Tratamiento de los pixeles
  //*************************************************************
  to_grayscale(&img);
  // luminancia_multiple(&img);
  // espejo(&img);
  // reduccion(&img);

  //***************************************************************************************************************************
  // 2 Crear la imágen BMP a partir del arreglo img.pixel[][]
  //*************************************************************************************************************************** 
  crear_imagen(&img, IMAGEN_TRATADA);
  printf("\nImágen BMP tratada en el archivo: %s\n", IMAGEN_TRATADA);

  //Terminar programa normalmente       
  exit(0);     
}
