//*****************************************************************
//EDGARDO ADRIÁN FRANCO MARTÍNEZ 
//(C) Agosto 2010 Versión 1.5
//Lectura, escritura y tratamiento de imagenes BMP 
//Compilación: "gcc BMP.c -o BMP"
//Ejecución: "./BMP imagen.bmp"
//Observaciones "imagen.bmp" es un BMP de 24 bits
//*****************************************************************

//*****************************************************************
//LIBRERIAS INCLUIDAS
//*****************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bmp24.h"

//*************************************************************************************************************************************************
// Función para abrir la imagen, colocarla en escala de grisis en la estructura imagen imagen (Arreglo de bytes de alto*ancho  --- 1 Byte por pixel 0-255)
// Parametros de entrada: Referencia a un BMP (Estructura BMP), Referencia a la cadena ruta char ruta[]=char *ruta
// Parametro que devuelve: Ninguno
//*************************************************************************************************************************************************
void abrir_imagen(BMP *imagen, char *ruta) {
  FILE *archivo;  // Puntero FILE para el archivo de imágen a abrir
  int i, j;

  // Abrir el archivo de imágen
  archivo = fopen(ruta, "rb+");
  if (! archivo) { 
    // Si la imágen no se encuentra en la ruta dada
    printf("La imágen %s no se encontro\n", ruta);
    exit(1);
  }

  // Leer la cabecera de la imagen y almacenarla en la estructura a la que apunta imagen
  fseek(archivo, 0, SEEK_SET);
  fread(&imagen->bm, sizeof(char), 2, archivo);
  fread(&imagen->tamano, sizeof(int), 1, archivo);
  fread(&imagen->reservado, sizeof(int), 1, archivo);     
  fread(&imagen->offset, sizeof(int), 1, archivo);        
  fread(&imagen->tamanoMetadatos, sizeof(int), 1, archivo);       
  fread(&imagen->alto, sizeof(int), 1, archivo);  
  fread(&imagen->ancho, sizeof(int), 1, archivo); 
  fread(&imagen->numeroPlanos, sizeof(short int), 1, archivo);    
  fread(&imagen->profundidadColor, sizeof(short int), 1, archivo);        
  fread(&imagen->tipoCompresion, sizeof(int), 1, archivo);
  fread(&imagen->tamanoEstructura, sizeof(int), 1, archivo);
  fread(&imagen->pxmh, sizeof(int), 1, archivo);
  fread(&imagen->pxmv, sizeof(int), 1, archivo);
  fread(&imagen->coloresUsados, sizeof(int), 1, archivo);
  fread(&imagen->coloresImportantes, sizeof(int), 1, archivo);    

  // Validar ciertos datos de la cabecera de la imágen   
  if (imagen->bm[0]!='B'||imagen->bm[1]!='M')  {
    printf ("La imagen debe ser un bitmap.\n"); 
    exit(1);
  }
  if (imagen->profundidadColor!= 24) {
    printf ("La imagen debe ser de 24 bits.\n"); 
    exit(1);
  }

  // Reservar memoria para el arreglo que tendra la imágen (Arreglo de tamaño "img.ancho X img.alto")
  imagen->pixel_r = malloc (imagen->alto* sizeof(unsigned char *)); 
  imagen->pixel_g = malloc (imagen->alto* sizeof(unsigned char *)); 
  imagen->pixel_b = malloc (imagen->alto* sizeof(unsigned char *)); 
  for (i = 0; i < imagen->alto; i++) {
    imagen->pixel_r[i] = malloc(imagen->ancho* sizeof(unsigned char)); 
    imagen->pixel_b[i] = malloc(imagen->ancho* sizeof(unsigned char)); 
    imagen->pixel_g[i] = malloc(imagen->ancho* sizeof(unsigned char)); 
  }
  // Pasar la imágen a el arreglo reservado en escala de grises
  for (i = 0; i < imagen->alto; i++) {
    for (j = 0; j < imagen->ancho; j++) {  
      fread(
        &imagen->pixel_b[i][j],
        sizeof(unsigned char), 1, archivo);  // Byte Blue del pixel
      fread(
        &imagen->pixel_g[i][j],
        sizeof(unsigned char), 1, archivo);  // Byte Green del pixel
      fread(
        &imagen->pixel_r[i][j],
        sizeof(unsigned char), 1, archivo);  // Byte Red del pixel
    }   
  }
  //Cerrrar el archivo
  fclose(archivo);      
}

//****************************************************************************************************************************************************
// Función para crear una imagen BMP, a partir de la estructura imagen imagen (Arreglo de bytes de alto*ancho  --- 1 Byte por pixel 0-255)
// Parametros de entrada: Referencia a un BMP (Estructura BMP), Referencia a la cadena ruta char ruta[]=char *ruta
// Parametro que devuelve: Ninguno
//****************************************************************************************************************************************************
void crear_imagen(BMP *imagen, char ruta[]) {
  FILE *archivo;  // Puntero FILE para el archivo de imágen a abrir

  int i, j;

  // Abrir el archivo de imágen
  archivo = fopen(ruta, "wb+");
  if (! archivo) { 
    // Si la imágen no se encuentra en la ruta dada
    printf( "La imágen %s no se pudo crear\n",ruta);
    exit(1);
  }

  // Escribir la cabecera de la imagen en el archivo
  fseek(archivo,0, SEEK_SET);
  fwrite(&imagen->bm,sizeof(char),2, archivo);
  fwrite(&imagen->tamano,sizeof(int),1, archivo);       
  fwrite(&imagen->reservado,sizeof(int),1, archivo);    
  fwrite(&imagen->offset,sizeof(int),1, archivo);       
  fwrite(&imagen->tamanoMetadatos,sizeof(int),1, archivo);      
  fwrite(&imagen->alto,sizeof(int),1, archivo); 
  fwrite(&imagen->ancho,sizeof(int),1, archivo);        
  fwrite(&imagen->numeroPlanos,sizeof(short int),1, archivo);   
  fwrite(&imagen->profundidadColor,sizeof(short int),1, archivo);       
  fwrite(&imagen->tipoCompresion,sizeof(int),1, archivo);
  fwrite(&imagen->tamanoEstructura,sizeof(int),1, archivo);
  fwrite(&imagen->pxmh,sizeof(int),1, archivo);
  fwrite(&imagen->pxmv,sizeof(int),1, archivo);
  fwrite(&imagen->coloresUsados,sizeof(int),1, archivo);
  fwrite(&imagen->coloresImportantes,sizeof(int),1, archivo);   

  // Pasar la imágen del arreglo reservado en escala de grises a el archivo (Deben escribirse los valores BGR)
  for (i = 0; i < imagen->alto; i++) {
    for (j=0; j < imagen->ancho; j++) {  
      // Ecribir los 3 bytes BGR al archivo BMP, en este caso todos se igualan al mismo valor (Valor del pixel en la matriz de la estructura imagen)
      fwrite(
        &imagen->pixel_b[i][j],
        sizeof(unsigned char), 1, archivo);  //Escribir el Byte Blue del pixel 
      fwrite(
        &imagen->pixel_g[i][j],
        sizeof(unsigned char), 1, archivo);  //Escribir el Byte Green del pixel
      fwrite(
        &imagen->pixel_r[i][j],
        sizeof(unsigned char), 1, archivo);  //Escribir el Byte Red del pixel
    }   
  }
  // Cerrrar el archivo
  fclose(archivo);
}
