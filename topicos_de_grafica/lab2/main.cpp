#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/opencv.hpp"
#include <iostream>


int load(char* name, cv::Mat& image) {
  // Read the file 
  image = cv::imread(name);
  // Check for invalid input 
  if(! image.data ) {
    std::cout <<  "Could not open or find the image" << std::endl ;
    return -1;
  }
}

void show(cv::Mat& image) {
  // Create a window for display.
  cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);
  // Show our image inside it.
  cv::imshow("Display window", image);
  // Wait for a keystroke in the window
  cv::waitKey(0);
}

void rotate(cv::Mat& src, cv::Mat& dst, double angle) {
  cv::Point2f center(src.cols / 2.0, src.rows / 2.0);
  cv::Mat rot = cv::getRotationMatrix2D(center, angle, 1.0);
  // determine bounding rectangle
  cv::Rect bbox = cv::RotatedRect(
    center, src.size(), angle).boundingRect();
  // adjust transformation matrix
  rot.at<double>(0, 2) += bbox.width / 2.0 - center.x;
  rot.at<double>(1, 2) += bbox.height / 2.0 - center.y;
  cv::warpAffine(src, dst, rot, bbox.size());
}

void brillo(cv::Mat& img, int val) {
  for(int i = 0; i < img.rows; ++i) {
    for(int j = 0; j < img.cols; ++j) {
      // You can now access the pixel value with cv::Vec3b
      img.at<cv::Vec3b>(i, j)[0] += val;
      if (img.at<cv::Vec3b>(i, j)[0] + val 255) {
        img.at<cv::Vec3b>(i, j)[0] = 255;
      };
      img.at<cv::Vec3b>(i, j)[1] += val;
      if (img.at<cv::Vec3b>(i, j)[1] > 255) {
        img.at<cv::Vec3b>(i, j)[1] = 255;
      };
      img.at<cv::Vec3b>(i, j)[2] += val;
      if (img.at<cv::Vec3b>(i, j)[2] > 255) {
        img.at<cv::Vec3b>(i, j)[2] = 255;
      };
    }
  }



  //for (int row = 0; row < image.rows; ++row) {
  //  const uchar *ptr = image.ptr(row);
  //  for (int col = 0; col < image.cols; ++col) {
  //    const uchar* uc_pixel = ptr;
  //    cv::Vec3b& color = image.at<cv::Vec3b>(row, col);
  //    color[0] = (uc_pixel[0] + val);
  //    color[1] = (uc_pixel[1] + val);
  //    color[2] = (uc_pixel[2] + val);
  //    ptr += 3;
  //  }
  //}
}

int main( int argc, char** argv ) {
  if( argc != 2) {
    std::cout <<" Usage: display_image ImageToLoadAndDisplay" << std::endl;
    return -1;
  }

  cv::Mat image, dst;

  load(argv[1], image);
  // rotate(image, dst, 45);
  brillo(image, 50);
  show(image);

  return 0;
}
