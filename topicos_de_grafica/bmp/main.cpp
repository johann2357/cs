#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>


unsigned char* read_bmp(const char* filename) {
    int i;
    FILE* f = std::fopen(filename, "rb");

    if (f == NULL) {
        throw "Argument Exception";
    }

    unsigned char info[54];
    std::fread(info, sizeof(unsigned char), 54, f); // read the 54-byte header

    // extract image height and width from header
    // Bytes - Content
    //     2 - Signature
    //     4 - File size
    //     4 - Reserved
    //     4 - File Offset to PixelArray 
    //     4 - DIB Header Size
    //     4 - WIDTH 
    //     4 - HEIGHT 
    int width = *(int*)&info[18];
    int height = *(int*)&info[22];

    std::cout << std::endl;
    std::cout << "  Name: " << filename << std::endl;
    std::cout << " Width: " << width << std::endl;
    std::cout << "Height: " << height << std::endl;

    int row_padded = (width * 3 + 3) & (~3);
    unsigned char* data = new unsigned char[row_padded];
    unsigned char tmp;

    for (int i = 0; i < height; ++i) {
        std::fread(data, sizeof(unsigned char), row_padded, f);
        for(int j = 0; j < width * 3; j += 3) {
            // Convert (B, G, R) to (R, G, B)
            tmp = data[j];
            data[j] = data[j + 2];
            data[j + 2] = tmp;

            std::cout << "RGB(" << (int)data[j];
            std::cout << "," << (int)data[j + 1];
            std::cout << "," << (int)data[j + 2] << ") ";
        }
        std::cout << std::endl;
    }

    std::fclose(f);
    return data;
}

int main() {
  read_bmp("test.bmp");
  return 0;
}
