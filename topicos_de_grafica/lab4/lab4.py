import numpy as np
import cv2

from matplotlib import pyplot as plt


img = cv2.imread('lennaHor.png', 0)

f = np.fft.fft2(img)
fshift = np.fft.fftshift(f)

mag_spec = 20 * np.log(np.abs(fshift))

rows, cols = img.shape
crow, ccol = rows / 2, cols / 2
tmpfshift = np.copy(fshift)
tmpfshift[crow - 30:crow + 30, ccol - 30:ccol + 30] = 6
for idxr, r in enumerate(fshift):
    for idxelem, elem in enumerate(r):
        if not tmpfshift[idxr][idxelem] == 6:
            fshift[idxr][idxelem] = 0


mag_spec2 = 20 * np.log(np.abs(fshift))

f_ishift = np.fft.ifftshift(fshift)
img_back = np.fft.ifft2(f_ishift)
img_back = np.abs(img_back)

plt.subplot(141), plt.imshow(img, cmap='gray')
plt.subplot(142), plt.imshow(mag_spec, cmap='gray')
plt.subplot(143), plt.imshow(img_back, cmap='gray')
plt.subplot(144), plt.imshow(mag_spec2, cmap='gray')
plt.show()


#cv2.imshow('image', img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
