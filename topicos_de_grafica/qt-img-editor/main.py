import cv2
import sys
import numpy as np

from shutil import copyfile

from PyQt4 import (
    QtGui,
    QtCore,
)


WINDOW_X = 50
WINDOW_Y = 50
WINDOW_WIDTH = 500
WINDOW_HEIGHT = 300
WINDOW_TITLE = 'Topicos de Grafica'

WINDOW = dict(
    x=WINDOW_X,
    y=WINDOW_Y,
    width=WINDOW_WIDTH,
    height=WINDOW_HEIGHT,
    title=WINDOW_TITLE
)


class Window(QtGui.QMainWindow):

    def __init__(self, *args, **kwargs):
        self.x = kwargs.pop('x', 0)
        self.y = kwargs.pop('y', 0)
        self.width = kwargs.pop('width', 100)
        self.height = kwargs.pop('height', 100)
        self.title = kwargs.pop('title', '')
        self.icon = kwargs.pop('icon', None)
        super(Window, self).__init__(*args, **kwargs)
        self.tmp_image_fn = '/tmp/tmpimg'
        self.start()

    def start(self):
        self.setGeometry(self.x, self.y, self.width, self.height)
        self.setWindowTitle(self.title)
        if self.icon is not None:
            self.setWindowIcon(QtGui.QIcon(self.icon))
        # self.start_home()
        self.start_menu()
        # self.start_toolbar()

    def start_menu(self):
        self.start_menu_file()
        self.start_menu_edit()

    def start_menu_file(self):
        open_action = QtGui.QAction('Open Image', self)
        open_action.setShortcut('Ctrl+O')
        open_action.setStatusTip('Open an image')
        open_action.triggered.connect(self.open_image)

        save_action = QtGui.QAction('Save Image', self)
        save_action.setShortcut('Ctrl+S')
        save_action.setStatusTip('Save the modified image')
        save_action.triggered.connect(self.save_image)

        quit_action = QtGui.QAction('Quit', self)
        quit_action.setShortcut('Ctrl+Q')
        quit_action.setStatusTip('Leave the App')
        quit_action.triggered.connect(self.close_app)

        self.statusBar()
        main_menu = self.menuBar()

        file_menu = main_menu.addMenu('&File')
        file_menu.addAction(open_action)
        file_menu.addAction(save_action)
        file_menu.addAction(quit_action)

    def start_menu_edit(self):
        enlarge_action = QtGui.QAction('Enlarge Image', self)
        enlarge_action.setShortcut('Ctrl+E')
        enlarge_action.setStatusTip('Enlarge the current image')
        enlarge_action.triggered.connect(self.enlarge_image)

        smooth_action = QtGui.QAction('Smooth Image', self)
        smooth_action.setShortcut('Ctrl+H')
        smooth_action.setStatusTip('Generate an smooth image')
        smooth_action.triggered.connect(self.smooth_image)

        sharpen_action = QtGui.QAction('Sharpen Image', self)
        sharpen_action.setShortcut('Ctrl+R')
        sharpen_action.setStatusTip('Generate the sharpened image')
        sharpen_action.triggered.connect(self.sharpen_image)

        self.statusBar()
        main_menu = self.menuBar()

        file_menu = main_menu.addMenu('&Edit')
        file_menu.addAction(enlarge_action)
        file_menu.addAction(smooth_action)
        file_menu.addAction(sharpen_action)

    def open_image(self):
        fn = QtGui.QFileDialog.getOpenFileName(
            self, caption='Open Image', filter='Images (*.png *.jpg *.bmp)')
        self.tmp_image_fn += '.' + fn.split('.')[-1]
        copyfile(fn, self.tmp_image_fn)
        self.show_image()

    def show_image(self):
        pic = QtGui.QLabel(self)
        pic.setGeometry(10, 10, 10, 10)
        pic.setPixmap(QtGui.QPixmap(self.tmp_image_fn))
        self.setCentralWidget(pic)

    def save_image(self):
        print 'save image'

    def enlarge_image(self):
        # FUTURE: manual increase factor
        img = cv2.imread(str(self.tmp_image_fn))
        height, width = img.shape[:2]
        res = cv2.resize(
            img,
            (2 * width, 2 * height),
            interpolation=cv2.INTER_LINEAR      # The best
            # interpolation=cv2.INTER_CUBIC
            # interpolation=cv2.INTER_LANCZOS4
            # interpolation=cv2.INTER_AREA
            # interpolation=cv2.INTER_NEAREST
        )
        cv2.imwrite(str(self.tmp_image_fn), res)
        self.show_image()

    def smooth_image(self):
        """
        http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_filtering/py_filtering.html
        """
        img = cv2.imread(str(self.tmp_image_fn))
        # blur = cv2.blur(img, (5, 5))
        # blur = cv2.medianBlur(img, 5)
        blur = cv2.bilateralFilter(img, 9, 75, 75)
        cv2.imwrite(str(self.tmp_image_fn), blur)
        self.show_image()

    def sharpen_image(self):
        # Load source / input image as grayscale, also works on color images...
        img = cv2.imread(str(self.tmp_image_fn))

        # generating the kernels
        # kernel_sharpen_1 = np.array([
        #     [-1, -1, -1],
        #     [-1, 9, -1],
        #     [-1, -1, -1]
        # ])
        # kernel_sharpen_2 = np.array([
        #     [1, 1, 1],
        #     [1, -7, 1],
        #     [1, 1, 1]]
        # )
        kernel_sharpen_3 = np.array([
            [-1, -1, -1, -1, -1],
            [-1, 2, 2, 2, -1],
            [-1, 2, 8, 2, -1],
            [-1, 2, 2, 2, -1],
            [-1, -1, -1, -1, -1]
        ]) / 8.0

        # applying different kernels to the input image
        # output_1 = cv2.filter2D(img, -1, kernel_sharpen_1)
        # output_2 = cv2.filter2D(img, -1, kernel_sharpen_2)
        output = cv2.filter2D(img, -1, kernel_sharpen_3)

        cv2.imwrite(str(self.tmp_image_fn), output)
        self.show_image()

    def close_app(self):
        choice = QtGui.QMessageBox.question(
            self,
            'Extract!',
            'Quit?',
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
        )
        if choice == QtGui.QMessageBox.Yes:
            QtCore.QCoreApplication.instance().quit()

    def show(self):
        super(Window, self).show()

    # def start_home(self):
    #     btn = QtGui.QPushButton('&Quit', self)
    #     btn.clicked.connect(self.close_app)
    #     btn.resize(btn.sizeHint())
    #     btn.move(50, 50)

    # def start_toolbar(self):
    #     extract_action = QtGui.QAction(
    #         QtGui.QIcon(''),
    #         'Open File',
    #         self
    #     )
    #     extract_action.triggered.connect(self.close_app)

    #     self.toolbar = self.addToolBar('Extraction')
    #     self.toolbar.addAction(extract_action)


def main():
    app = QtGui.QApplication(sys.argv)
    gui = Window(**WINDOW)
    gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
