import sys
from PyQt4 import QtGui


WINDOW_X = 50
WINDOW_Y = 50
WINDOW_WIDTH = 500
WINDOW_HEIGHT = 300
WINDOW_TITLE = 'Topicos de Grafica'


def main():
    app = QtGui.QApplication(sys.argv)

    window = QtGui.QWidget()

    window.setGeometry(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT)
    window.setWindowTitle(WINDOW_TITLE)
    window.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
