#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QTime>

#include <opencv2/highgui/highgui.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void timerEvent(QTimerEvent *event);

private slots:
    void on_actionStart_triggered();

private:
    Ui::MainWindow *ui;

    cv::VideoCapture mCapture;

    QLabel mInfoLabel;

    QTime mFpsTimer;
};

#endif // MAINWINDOW_H
