#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

#include <vector>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->statusBar->addWidget( &mInfoLabel );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionStart_triggered()
{
    if( !mCapture.isOpened() )
        if( !mCapture.open( 0 ) )
            return;

    startTimer(0); // Maximum speed

    mFpsTimer.start();
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

    cv::Mat raw;
    mCapture >> raw;

    // >>>>> Params from widgets
    QString comprType;
    int quality = ui->horizontalSlider_quality->value();
    int compr;

    if( ui->radioButton_jpeg->isChecked() )
    {
        comprType = ".jpg";
        compr = cv::IMWRITE_JPEG_QUALITY;
    }
    else
    {
        comprType = ".png";
        quality = qMin( 9, quality/10 ); // PNG quality is in [0,9] range
        compr = cv::IMWRITE_PNG_COMPRESSION;
    }
    // <<<<< Params from widgets

    // >>>>> FPS Calculation
    static int frameCount = 0;
    static qreal timeAcc = 0;
    static qreal fps=0.0;

    int nFrm = 10; // 10 frames

    qreal elapsed = (qreal)mFpsTimer.elapsed();
    timeAcc += elapsed;
    frameCount++;

    if(frameCount%nFrm==0) // Update every "nFrm" frames
    {
        fps = 1000.0/(timeAcc/nFrm);
        timeAcc=0;
        frameCount=0;
    }

    mFpsTimer.restart();
    // <<<<< FPS Calculation

    if( !raw.empty() )
    {
        // >>>>> Jpeg compression in memory
        vector<uchar> buf; // Memory buffer
        vector<int> params;

        params.push_back( compr );
        params.push_back( quality ); // Quality of compression

        cv::imencode( comprType.toLocal8Bit().data(), raw, buf, params );
        cv::Mat compressed;
        cv::imdecode( cv::Mat(buf), cv::IMREAD_COLOR, &compressed );

        mInfoLabel.setText( tr("<b>Compression quality:</b> %1 - <b>Raw data:</b> %2 bytes - <b>Compressed data:</b> %3 bytes - <b>FPS:</b> %4")
                            .arg(quality).arg(raw.cols*raw.rows*raw.channels()).arg(buf.size()).arg(fps) );
        // <<<<< Jpeg compression in memory

        // Show the images
        ui->widget_raw->showImage( raw );

        if( !compressed.empty())
            ui->widget_compressed->showImage( compressed );
    }
}
