$(document).ready(function() {
  $("#doquery").submit(function(event) {
    var input_string = $("#word").val();
    var number = $("#number").val();
    $.ajax({
      url : "/query/" + input_string + '/' + number,
      type : "GET",
      dataType: "html",
      success : function(data) {
        if (data == '') {
          data = 'No results :(';
        }
        $('#result').html(data);
      },
    });
    event.preventDefault();
  });
});
