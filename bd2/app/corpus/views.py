import MySQLdb
from stems import stem_id
from words import stem_word_rel
from math import sqrt
from enchant import Dict
from nltk.stem import PorterStemmer
from django.shortcuts import render_to_response
from django.http import HttpResponse

# LOADING GLOBAL VARIABLES

print "LOADING PROJECT BASE ...",

id_vector = {}

query = 'select id, vector from stems_data'

db_con = MySQLdb.connect(
    host="localhost",       # your host, usually localhost
    user="root",            # your username
    passwd="rock'n'roll",   # your password
    db="google_corpus"
)

with db_con:
    cur = db_con.cursor()
    cur.execute(query)
    for x in cur:
        id_vector[x[0]] = eval(x[-1])
    cur.close()

english_checker = Dict("en_US")
stemmer = PorterStemmer()

print " DONE"

# END OF GLOBAL VARIABLES


def get_cos(vec1, vec2):
    norma_vec1 = 0
    for v in vec1.itervalues():
        norma_vec1 += v * v
    norma_vec1 = sqrt(norma_vec1)
    norma_vec2 = 0
    for v in vec2.itervalues():
        norma_vec2 += v * v
    norma_vec2 = sqrt(norma_vec2)
    dot_product = 0
    keys_vec1 = set(vec1.keys())
    keys_vec2 = set(vec2.keys())
    intersection = keys_vec1 & keys_vec2
    for x in intersection:
        dot_product += vec1[x] * vec2[x]
    if norma_vec1 and norma_vec2:
        return dot_product / (norma_vec1 * norma_vec2)
    return 0


def index(request):
    return render_to_response('index.html', {})


def get_words(request, word, number):
    if not english_checker.check(word):
        return HttpResponse('!!! `%s` is NOT AN ENGLISH WORD.' % word)
    stem = stemmer.stem(word)
    stem_pk = stem_id.get(stem, None)
    if not stem_pk:
        return HttpResponse(
            '!!! `%s` is NOT A FUNCTIONAL ENGLISH WORD.' % word
        )
    row = "<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>"
    result = ""
    results = []
    tmp = 0
    for k, v in stem_id.iteritems():
        if k != stem:
            tmp = get_cos(id_vector[stem_pk], id_vector[v])
            if tmp:
                results.append((k, tmp))
    results.sort(key=lambda tpl: tpl[1], reverse=True)
    for i, r in enumerate(iter(results[:int(number)])):
        result += row.format(
            i,
            stem_word_rel.get(r[0], r[0].capitalize()),
            r[1]
        )
    return HttpResponse(result)
