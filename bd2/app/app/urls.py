from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^$', 'corpus.views.index', name='home'),
    url(r'^query/(?P<word>\w+)/(?P<number>[0-9]+)/$', 'corpus.views.get_words', name='get_words'),

)
