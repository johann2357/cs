select usr.usuario, c.ds_comentario, c.fch_publicacion, z.ds_zona
from comentario c
join zona z
on z.id_zona=c.id_zona
join cuenta usr
on usr.id_cuenta=c.id_cuenta;

select zhijo.ds_zona, zpadre.ds_zona
from zona zhijo
join zona zpadre
on zpadre.id_zona=zhijo.id_padre;

select z.ds_zona, if(c.ds_comentario, c.ds_comentario, 'no tengo comentario')
from zona z
left join comentario c
on z.id_zona=c.id_zona;

select z.ds_zona, if(c.ds_comentario, c.ds_comentario, 'no tengo comentario')
from comentario c
right join zona z
on z.id_zona=c.id_zona;

select z.ds_zona, count(z.ds_zona)
from zona z
join comentario c
on z.id_zona=c.id_zona
group by ds_zona;

select year(c.fecha) , month(c.fecha), count(month(c.fecha))
from comentario c
group by year(c.fecha), month(c.fecha);
