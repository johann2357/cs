DROP TABLE IF EXISTS ventas;

CREATE TABLE ventas (
  producto varchar(30) not null,
  region varchar(20) not null,
  fecha date not null,
  monto int not null
) CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO ventas (producto, region, fecha, monto) 
  VALUES
    ('papa', 'Arequipa', '2013-01-01', 2),
    ('cebolla', 'Arequipa', '2013-04-10', 3),
    ('atun', 'Lima', '2014-12-01', 4),
    ('papa', 'Arequipa', '2013-02-02', 3),
    ('atun', 'Lima', '2013-01-01', 4),
    ('atun', 'Arequipa', '2013-11-01', 2), 
    ('cebolla', 'Arequipa', '2014-05-15', 3), 
    ('cebolla', 'Arequipa', '2013-11-17', 4)
;

DROP PROCEDURE IF EXISTS consulta;

delimiter //
CREATE PROCEDURE consulta()
BEGIN
  set @min_date = 0;
  set @max_date = 0;
  SELECT min(fecha), max(fecha) FROM ventas INTO @min_date, @max_date;
  DROP TABLE IF EXISTS tmp;
  CREATE temporary TABLE tmp (
    fecha varchar(6) not null,
    region varchar(20) not null
  ) CHARACTER SET utf8 COLLATE utf8_general_ci;
  WHILE (@min_date <= @max_date) DO
    INSERT into tmp (fecha, region) VALUES (
      date_format(@min_date, '%b-%y'), 'Arequipa'
    );
    INSERT INTO tmp (fecha, region) VALUES (
      date_format(@min_date, '%b-%y'), 'Lima'
    );
    set @min_date = date_add(@min_date, interval 1 month);
  END WHILE;
  SELECT t.fecha Fecha, t.region Region, ifnull(result.monto, 0) Monto
  FROM tmp t
  LEFT JOIN (
      SELECT date_format(fecha, '%b-%y') fecha, region, sum(monto) monto
      FROM ventas GROUP BY year(fecha), month(fecha), region
    ) result
  ON t.fecha=result.fecha AND t.region=result.region;
  DROP TABLE tmp;
END
//
delimiter ;

