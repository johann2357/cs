import MySQLdb

id_vector = {}

query = 'select stem, word from word_stem'

db_con = MySQLdb.connect(
    host="localhost",       # your host, usually localhost
    user="root",            # your username
    passwd="rock'n'roll",   # your password
    db="google_corpus"
)

d = {}

with db_con:
    cur = db_con.cursor()
    cur.execute(query)
    for x in cur:
        if not d.get(x[0], None):
            d[x[0]] = x[1].capitalize()
        elif len(x[1]) < len(d[x[0]]):
            d[x[0]] = x[1].capitalize()
    cur.close()

print 'stem_word_rel = ',
print str(d)
