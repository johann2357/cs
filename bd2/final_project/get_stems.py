import MySQLdb

query = "select stem, id from stems_data"

d = {}

db_con = MySQLdb.connect(
    host="localhost",       # your host, usually localhost
    user="root",            # your username
    passwd="rock'n'roll",   # your password
    db="google_corpus"
)
try:
    with db_con:
        cur = db_con.cursor()
        cur.execute(query)
        for x in cur:
            d[x[0]] = int(x[-1])
        cur.close()
        print d
except:
    print " whoops "
