import MySQLdb
from stems import stem_id


query = 'select stem2, w from bigram_stems_w where stem1="%s"'
update = 'update stems_data set stems_data.vector="%s" where id=%i'

db_con = MySQLdb.connect(
    host="localhost",       # your host, usually localhost
    user="root",            # your username
    passwd="rock'n'roll",   # your password
    db="google_corpus"
)

for word, pk in stem_id.iteritems():
    d = {}
    with db_con:
        cur = db_con.cursor()
        cur.execute(query % word)
        for x in cur:
            spk = stem_id.get(x[0], -1)
            if spk > 0:
                d[spk] = x[-1]
        cur.execute(update % (str(d).replace(' ', ''), pk))
        cur.close()
