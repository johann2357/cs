DROP TABLE IF EXISTS bigram;

CREATE TABLE bigram (
  id int(11) NOT NULL AUTO_INCREMENT,
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE bigram_stems (
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE bigram_stems_tf_idf (
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency bigint(20) NOT NULL,
  tf double NOT NULL DEFAULT -1,
  idf double NOT NULL DEFAULT -1,
  w double NOT NULL DEFAULT -1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE test (
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency bigint(20) NOT NULL,
  tf double NOT NULL DEFAULT -1,
  idf double NOT NULL DEFAULT -1,
  w double NOT NULL DEFAULT -1,
  KEY `idx_word2` (`word2`) USING HASH,
  KEY `idx_word1` (`word1`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

insert into test (word1, word2, frequency) select * from bigram_stems;

CREATE TABLE bigram_stems_tf_idf (
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency float(5, 4) NOT NULL
)

INSERT INTO bigram (word1, word2, frequency) 
  VALUES
    ('word1', 'word2', 0),
    ('word1', 'word2', 1),
    ('word1', 'word2', 2),
    ('word1', 'word2', 3),
    ('word1', 'word2', 4),
    ('word1', 'word2', 5)
;

CREATE TABLE test_stems (
  id int(11) NOT NULL AUTO_INCREMENT,
  stem varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--  INDEX (id)

CREATE INDEX idx_word2 ON bigram_stems_tf_idf (word2) USING HASH;


-- ELIMINATING REPEATED WORDS IN THE SAME STEM

delete from test where word1=word2;

-- GENERATING STEMS TABLE

select tmp.word1 from (
  select b.word1 from bigram_stems_reduced b group by b.word1
) tmp order by tmp.word1;


insert into test_stems (stem) select tmp.word1 from (
  select b.word1 from test b group by b.word1
) tmp order by tmp.word1;

-- tf = frequency/max(frequency of word1)
-- idf = ln( (Num of stems)/(appereances of word2 with each word1) )
-- w = tf * idf

-- GENERATING TF
update test, (
  select src.word1, max(src.frequency) mx
  from test src
  group by src.word1
) result
SET test.tf=test.frequency/result.mx
where test.word1=result.word1 ;

-- GENERATING IDF

select count(*)
from (
  select word1, count(*)
  from test
  group by word1
) tmp;

--+----------+
--| count(*) |
--+----------+
--|    16456 |
--+----------+

update test, (
  select src.word2 stem, count(*) num
  from test src
  group by src.word2
) result
SET test.idf=LN(37653/result.num)
where test.word2=result.stem ;

-- GENERATING w

update test
SET test.w=test.tf*test.idf;

CREATE TABLE bigram_stems_w (
  stem1 varchar(50) NOT NULL,
  stem2 varchar(50) NOT NULL,
  w float(6,4) NOT NULL,
  KEY `idx_stem1` (`stem1`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE stems_data (
  id int(5) NOT NULL AUTO_INCREMENT,
  stem varchar(50) NOT NULL,
  vector varchar(10000) NOT NULL DEFAULT "{}",
  KEY `idx_stem` (`stem`),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO bigram_stems_w (stem1, stem2, w)
SELECT test.word1, test.word2, round(test.w, 4)
FROM test
WHERE test.w > 0.1
ORDER BY test.word1, test.word2 ASC;


--mysql> select avg(tmp.num) from (select count(word2) num from test where w>0.1 group by test.word1) tmp order by tmp.num;
--+--------------+
--| avg(tmp.num) |
--+--------------+
--|      23.8929 |
--+--------------+
--1 row in set (3.82 sec)
--
--mysql> select max(w) from test where w > 0.1;
--+--------------------+
--| max(w)             |
--+--------------------+
--| 10.536167911328556 |
--+--------------------+
--1 row in set (3.18 sec)
--
--mysql> select min(w) from test where w > 0.1;
--+---------------------+
--| min(w)              |
--+---------------------+
--| 0.10000042417669404 |
--+---------------------+
--1 row in set (3.20 sec)
--
--mysql> 

insert into stems_data (stem) select tmp.stem1 from (
  select b.stem1 from bigram_stems_w b group by b.stem1
) tmp;
