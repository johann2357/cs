import MySQLdb
from nltk.stem import PorterStemmer

letters = ['g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
           't', 'u', 'v', 'w', 'x', 'y', 'z']
stemmer = PorterStemmer()

for letter in letters:

    print "/* with letter: " + letter + " */"
    count = "select count(*) from bigram_ci where `LOWER(word1)` LIKE '" +\
            letter + "%';"
    select = "SELECT * FROM bigram_ci WHERE `LOWER(word1)` LIKE '" +\
             letter + "%' LIMIT "
    select_limit = ",100000;"
    bulk_create = "INSERT INTO bigram_stems (word1, word2, frequency) VALUES "
    start = 0
    end = -1
    d = {}

    db_con = MySQLdb.connect(
        host="localhost",       # your host, usually localhost
        user="root",            # your username
        passwd="rock'n'roll",   # your password
        db="google_corpus"
    )
    try:
        with db_con:
            cur = db_con.cursor()
            cur.execute(count)
            end = cur.fetchall()[0][0]
            while start <= end:
                cur.execute(select + str(start) + select_limit)
                for x in cur:
                    t = (stemmer.stem(x[0]), stemmer.stem(x[1]))
                    d[t] = x[2] + d.get(t, 0)
                start += 100000
            values = ""
            num_val = 1
            total = len(d)
            for k, v in d.iteritems():
                values += str(k)[:-1] + ", " + str(v) + "),"
                if num_val % 100000 == 0 or num_val == total:
                    print bulk_create + values[:-1] + ";"
                    values = ""
                num_val += 1
            cur.close()
    except:
        print "/* whoops something went wrong with letter: " + letter + " */"
