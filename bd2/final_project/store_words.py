import MySQLdb
from enchant import Dict
from nltk import pos_tag
from constants import CONTENT_WORDS

files = [
    '/home/johann2357/Documents/google_corpus/2gm-0000',
    '/home/johann2357/Documents/google_corpus/2gm-0001',
    '/home/johann2357/Documents/google_corpus/2gm-0002',
    '/home/johann2357/Documents/google_corpus/2gm-0003',
    '/home/johann2357/Documents/google_corpus/2gm-0004',
    '/home/johann2357/Documents/google_corpus/2gm-0005',
    '/home/johann2357/Documents/google_corpus/2gm-0006',
    '/home/johann2357/Documents/google_corpus/2gm-0007',
    '/home/johann2357/Documents/google_corpus/2gm-0008',
    '/home/johann2357/Documents/google_corpus/2gm-0009',
    '/home/johann2357/Documents/google_corpus/2gm-0010',
    '/home/johann2357/Documents/google_corpus/2gm-0011',
    '/home/johann2357/Documents/google_corpus/2gm-0012',
    '/home/johann2357/Documents/google_corpus/2gm-0013',
    '/home/johann2357/Documents/google_corpus/2gm-0014',
    '/home/johann2357/Documents/google_corpus/2gm-0015',
    '/home/johann2357/Documents/google_corpus/2gm-0016',
    '/home/johann2357/Documents/google_corpus/2gm-0017',
    '/home/johann2357/Documents/google_corpus/2gm-0019',
    '/home/johann2357/Documents/google_corpus/2gm-0020',
    '/home/johann2357/Documents/google_corpus/2gm-0021',
    '/home/johann2357/Documents/google_corpus/2gm-0022',
    '/home/johann2357/Documents/google_corpus/2gm-0023',
    '/home/johann2357/Documents/google_corpus/2gm-0024',
    '/home/johann2357/Documents/google_corpus/2gm-0025',
    '/home/johann2357/Documents/google_corpus/2gm-0026',
    '/home/johann2357/Documents/google_corpus/2gm-0027',
    '/home/johann2357/Documents/google_corpus/2gm-0028',
    '/home/johann2357/Documents/google_corpus/2gm-0029',
    '/home/johann2357/Documents/google_corpus/2gm-0030',
    '/home/johann2357/Documents/google_corpus/2gm-0031',
]

d = Dict("en_US")

for file_name in files:
    p = ['', '']
    bigram = [False, False]
    tags = [('', ''), ('', '')]

    bulk_create = "INSERT INTO bigram (word1, word2, frequency) VALUES "
    values = ""

    with open(file_name, 'rb') as f:
        for line in f:
            p = line.split('\t')
            bigram = p[0].split()
            if d.check(bigram[0]) and d.check(bigram[1]):
                tags = pos_tag(bigram)
                if tags[0][1] in CONTENT_WORDS and tags[1][1] in CONTENT_WORDS:
                    values += '("%s", "%s", %s),' % (
                        bigram[0],
                        bigram[1],
                        p[1].strip()
                    )

    if values:
        bulk_create += values[:-1] + ";"
    print bulk_create
    if values:
        try:
            db_con = MySQLdb.connect(
                host="localhost",       # your host, usually localhost
                user="root",            # your username
                passwd="rock'n'roll",   # your password
                db="google_corpus"
            )

            with db_con:
                cur = db_con.cursor()
                cur.execute(bulk_create)
                cur.close()
        except:
            print "/* fail last bulk insert :'( */"
