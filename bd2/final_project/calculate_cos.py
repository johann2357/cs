import MySQLdb
from stems import stem_id
from math import sqrt

id_vector = {}

query = 'select id, vector from stems_data'

db_con = MySQLdb.connect(
    host="localhost",       # your host, usually localhost
    user="root",            # your username
    passwd="rock'n'roll",   # your password
    db="google_corpus"
)

with db_con:
    cur = db_con.cursor()
    cur.execute(query)
    for x in cur:
        id_vector[x[0]] = eval(x[-1])
    cur.close()


def get_cos(vec1, vec2):
    norma_vec1 = 0
    for v in vec1.itervalues():
        norma_vec1 += v * v
    norma_vec1 = sqrt(norma_vec1)
    norma_vec2 = 0
    for v in vec2.itervalues():
        norma_vec2 += v * v
    norma_vec2 = sqrt(norma_vec2)
    dot_product = 0
    keys_vec1 = set(vec1.keys())
    keys_vec2 = set(vec2.keys())
    intersection = keys_vec1 & keys_vec2
    for x in intersection:
        dot_product += vec1[x] * vec2[x]
    if norma_vec1 and norma_vec2:
        return dot_product / (norma_vec1 * norma_vec2)
    return 0

import timeit
start_time = timeit.default_timer()

word = 'keyboard'

results = []

for k, v in stem_id.iteritems():
    if k != word:
        results.append((k, get_cos(id_vector[stem_id[word]], id_vector[v])))

results.sort(key=lambda tpl: tpl[1])

elapsed = timeit.default_timer() - start_time
for r in iter(results):
    print r[0], ' -> ', r[1]
print elapsed
