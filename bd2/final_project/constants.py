CONTENT_WORDS = (
    'FW',    # Foreign word
    'JJ',    # Adjective
    'JJR',   # Adjective, comparative
    'JJS',   # Adjective, superlative
    'NN',    # Noun, singular or mass
    'NNS',   # Noun, plural
    'RB',    # Adverb
    'RBR',   # Adverb, comparative
    'RBS',   # Adverb, superlative
    'RP',    # Particle
    'VB',    # Verb, base form
    'VBD',   # Verb, past tense
    'VBG',   # Verb, gerund or present participle
    'VBN',   # Verb, past participle
    'VBP',   # Verb, non-3rd person singular present
    'VBZ'    # Verb, 3rd person singular present
)

FUNCTION_WORDS = (
    'CC',     # Coordinating conjunction
    'CD',     # Cardinal number
    'DT',     # Determiner
    'EX',     # Existential there
    'IN',     # Preposition or subordinating conjunction
    'LS',     # List item marker
    'MD',     # Modal
    'NNP',    # Proper noun, singular
    'NNPS',   # Proper noun, plural
    'PDT',    # Predeterminer
    'POS',    # Possessive ending
    'PRP',    # Personal pronoun
    'PRP$',   # Possessive pronoun
    'SYM',    # Symbol
    'TO',     # to
    'UH',     # Interjection
    'WDT',    # Wh-determiner
    'WP',     # Wh-pronoun
    'WP$',    # Possessive wh-pronoun
    'WRB'     # Wh-adverb
)

# http://www.monlp.com/2011/11/08/part-of-speech-tags/
