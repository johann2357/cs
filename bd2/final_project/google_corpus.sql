DROP TABLE IF EXISTS bigram;

CREATE TABLE bigram (
  id int(11) NOT NULL AUTO_INCREMENT,
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE bigram_stems (
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE bigram_stems_tf_idf (
  word1 varchar(50) NOT NULL,
  word2 varchar(50) NOT NULL,
  frequency bigint(20) NOT NULL,
  tf double NOT NULL DEFAULT -1,
  idf double NOT NULL DEFAULT -1,
  w double NOT NULL DEFAULT -1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO bigram (word1, word2, frequency) 
  VALUES
    ('word1', 'word2', 0),
    ('word1', 'word2', 1),
    ('word1', 'word2', 2),
    ('word1', 'word2', 3),
    ('word1', 'word2', 4),
    ('word1', 'word2', 5)
;

CREATE TABLE stems (
  id int(11) NOT NULL AUTO_INCREMENT,
  stem varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--  INDEX (id)

CREATE INDEX idx_word2 ON bigram_stems_tf_idf (word2) USING HASH;


-- ELIMINATING REPEATED WORDS IN THE SAME STEM

delete from bigram_stems_reduced where word1=word2;

-- GENERATING STEMS TABLE

select tmp.word1 from (
  select b.word1 from bigram_stems_reduced b group by b.word1
) tmp order by tmp.word1;


insert into stems (stem) select tmp.word1 from (
  select b.word1 from bigram_stems_reduced b group by b.word1
) tmp order by tmp.word1;

-- tf = frequency/max(frequency of word1)
-- idf = ln( (Num of stems)/(appereances of word2 with each word1) )
-- w = tf * idf

-- GENERATING TF
update bigram_stems_tf_idf, (
  select src.word1, max(src.frequency) mx
  from bigram_stems_tf_idf src
  group by src.word1
) result
SET bigram_stems_tf_idf.tf=bigram_stems_tf_idf.frequency/result.mx
where bigram_stems_tf_idf.word1=result.word1 ;

-- GENERATING IDF

select count(*)
from (
  select word1, count(*)
  from bigram_stems_tf_idf
  group by word1
) tmp;

--+----------+
--| count(*) |
--+----------+
--|    16456 |
--+----------+

update bigram_stems_tf_idf, (
  select src.word2 stem, count(*) num
  from bigram_stems_tf_idf src
  group by src.word2
) result
SET bigram_stems_tf_idf.idf=LN(16456/result.num)
where bigram_stems_tf_idf.word2=result.stem ;

-- GENERATING w

update bigram_stems_tf_idf
SET bigram_stems_tf_idf.w=bigram_stems_tf_idf.tf*bigram_stems_tf_idf.idf;

-- WORD - STEM table

CREATE TABLE word_stem (
  id int(5) NOT NULL AUTO_INCREMENT,
  word varchar(50) NOT NULL,
  stem varchar(50) NOT NULL DEFAULT '-',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO word_stem (word)
SELECT `LOWER(word1)`
FROM bigram_ci
GROUP BY `LOWER(word1)`;
