import MySQLdb
from nltk.stem import PorterStemmer

stemmer = PorterStemmer()

query = 'select id, word from word_stem'
update = 'update word_stem set word_stem.stem="%s" where id=%i'

db_con = MySQLdb.connect(
    host="localhost",       # your host, usually localhost
    user="root",            # your username
    passwd="rock'n'roll",   # your password
    db="google_corpus"
)

with db_con:
    cur = db_con.cursor()
    cur.execute(query)

    for row in cur:
        with db_con:
            cur2 = db_con.cursor()
            cur2.execute(update % (stemmer.stem(row[1]), row[0]))
            cur2.close()

    cur.close()
