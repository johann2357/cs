#include <iostream>
#include <algorithm>

#define inf 999999


int change1(int* coins, int i, int change)
{
  if (change == 0)
    return 0;
  else if ((i == -1) || (change < 0))
    return inf;
  return std::min(
    change1(coins, i-1, change),
    1 + change1(coins, i, change-coins[i])
  );
}

int change2(int* coins, int N, int change )
{
  int* amount = new int [change + 1];

  for (int i = 1; i <= change; ++i)
  {
    amount[i] = inf;
    int temp = inf;
    for(int c = 0; c < N; ++c)
    {
      if(coins[c] <= i)
      {
        int temp_amt = amount[i-coins[c]] + 1;

        if(temp_amt < temp)
        {
          temp = temp_amt;
          amount[i] = temp;
        }
      }
    }
  }
  return amount[change];
}

int main()
{
  int change;
  std::cout << "change: ";
  std::cin >> change;
  int num;
  std::cout << "num coins: ";
  std::cin >> num;
  int* coins = new int[num];
  for (int i = 0; i < num; ++i)
  {
    std::cout << "coin " << i+1  << ": ";
    std::cin >> coins[i];
  }
  std::cout << "resultdp: " << change2(coins, num, change) << std::endl;
  std::cout << "resultb: " << change1(coins, num-1, change);
  std::cout << "\n";
  return 0;
}
