#include <iostream>
#include <algorithm>

void sift_down(int* vec, int start, int end)
{
  int root = start;
  while ((root * 2 + 1) <= end)
  {
    int child = root * 2 + 1;
    int swap = root;
    if (vec[swap] < vec[child])
      swap = child;
    if (((child + 1) <= end) && (vec[swap] < vec[child + 1]))
      swap = child + 1;
    if (swap != root)
    {
      std::swap(vec[root], vec[swap]);
      root = swap;
    }
    else
      return;
  }
}

void heap_sort(int* vec, int size)
{
  for (int start = (size - 2) / 2; start >= 0; --start)
    sift_down(vec, start, size - 1);
  //
  int end = size - 1;
  while (end > 0)
  {
    std::swap(vec[0], vec[end]);
    --end;
    sift_down(vec, 0, end);
  }
}

int main()
{
  int vec[10] = {14, 6, 2, 18, 9, 1, 3, 5, 10, 7};
  int tam = 10;
  //
  for (int i = 0; i < tam; ++i)
    std::cout << " " << vec[i];
  std::cout << "\n";
  //
  heap_sort(vec, tam);
  //
  for (int i = 0; i < tam; ++i)
    std::cout << " " << vec[i];
  std::cout << "\n";
  //
  return 0;
}
