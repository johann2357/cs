#include <iostream>

void analize(int rows, int columns, int mines)
{
  int non_mined = rows * columns - mines;
  int rows_to_fill = non_mined / columns;
  int extra = non_mined / columns;
  //
  bool impossible = false;
  //
  // IMPOSSIBLE CASES TOO MANY MINES
  //
  if (non_mined % 2 == 0)
  {
    if (non_mined < 4)
      impossible = true;
  }
  else if (non_mined % 2)
  {
    if (non_mined < 9)
    {
      if ((rows < 3) || (columns < 3))
        impossible = true;
    }
    if (((columns == 2) || (rows == 2)))
      impossible = true;
  }
  //
  if ((rows == 1) || (columns == 1))
    impossible = false;
  //
  if (non_mined == 0)
    impossible = true;
  else if (non_mined == 1)
    impossible = false;
  else if ((mines == 1) && ((columns == 2) || (rows == 2)))
    impossible = true;
  else if ((non_mined < 4) && (columns > 2) && (rows > 2))
    impossible = true;
  else if ((non_mined == 5) && (columns > 2) && (rows > 2))
    impossible = true;
  else if ((non_mined == 7) && (columns > 2) && (rows > 2))
    impossible = true;
  //
  if (impossible)
  {
    std::cout << "Impossible\n";
  }
  else
  {
    // CREATING AND FILLING MATRIX
    int** matrix = new int* [rows];
    matrix[0] = new int [rows*columns];
    for (int i = 0; i < rows*columns; ++i)
      matrix[0][i] = -1;
    for (int i = 1; i < rows; ++i)
      matrix[i] = &(matrix[0][columns*i]);
    //
    // SINGULAR NON MINED
    //
    if (non_mined == 1)
    {
      matrix[0][0] = 0;
    }
    //
    // NO MINES
    //
    else if (mines == 0)
    {
      for (int i = 0; i < rows*columns; ++i)
        matrix[0][i] = 0;
    }
    //
    // SINGULAR CASES (1 row/column)
    //
    else if (rows == 1)
    {
      for (int i = 0; i < columns; ++i)
      {
        if (i < non_mined)
          matrix[0][i] = 0;
        else
          matrix[0][i] = -1;
      }
    }
    else if (columns == 1)
    {
      for (int i = 0; i < rows; ++i)
      {
        if (i < non_mined)
          matrix[i][0] = 0;
        else
          matrix[i][0] = -1;
      }
    }
    //
    // CANT FILL FIRST TWO ROWS CASE 1
    //
    else if ((non_mined % 2 == 0) && (non_mined / columns < 2))
    {
      for (int i = 0; i < (non_mined/2); ++i)
        matrix[0][i] = 0;
      for (int i = 0; i < (non_mined/2); ++i)
        matrix[1][i] = 0;
    }
    //
    // CANT FILL FIRST TWO ROWS CASE 2 ( and 3rd row < 3 non-mines)
    //
    else if ((non_mined % 2) && (non_mined / columns < 3) && (non_mined % columns) && (mines > columns-2))
    {
      for (int i = 0; i < (non_mined-3)/2; ++i)
        matrix[0][i] = 0;
      for (int i = 0; i < (non_mined-3)/2; ++i)
        matrix[1][i] = 0;
      for (int i = 0; i < 3; ++i)
        matrix[2][i] = 0;
    }
    //
    // SINGLE NON MINE IN NEW ROW
    //
    else if ((non_mined % columns == 1) && (non_mined / columns > 2))
    {
      int times = non_mined;
      for (int i = 0; i < rows; ++i)
      {
        for (int j = 0; j < columns; ++j)
        {
          if (times > 2)
          {
            matrix[i][j] = 0;
            --times;
          }
          else if (times == 2)
          {
            --times;
          }
          else if ((times >= 0) && (times < 2))
          {
            matrix[i][j] = 0;
            --times;
          }
        }
      }
    }
    else
    {
      int count = 0;
      for (int i = 0; i < rows; ++i)
      {
        for (int j = 0; j < columns; ++j)
        {
          if (count < non_mined)
          {
            matrix[i][j] = 0;
            ++count;
          }
        }
      }
    }
    // PRINT
    for (int i = 0; i < rows; ++i)
    {
      for (int j = 0; j < columns; ++j)
      {
        if ((i == 0) && (j == 0))
          std::cout << "c";
        else if (matrix[i][j] == 0)
          std::cout << ".";
        else if (matrix[i][j] == -1)
          std::cout << "*";
      }
      std::cout << "\n";
    }
    delete [] (*matrix);
    delete [] matrix;
  }
}

int main()
{
  int cases;
  int rows;
  int columns;
  int mines;
  std::cin >> cases;
  for (int k = 1; k <= cases; ++k)
  {
    std::cout << "CASE #" << k << ":\n";
    std::cin >> rows;
    std::cin >> columns;
    std::cin >> mines;
    analize(rows, columns, mines);
  }
  return 0;
}

/*
 * BEST CASE -> 1
 * WORST CASE -> n (filling) + rows (creating matrix) + n (non_mined filling) + n (printing) + c
 *            -> 3*n + rows + c
 *            -> O(n)
*/
