#include <iostream>
#include <cmath>

void print(int* x, int n)
{
  for (int i = 0; i < n; ++i)
  {
    for (int j = 0; j < n; ++j)
    {
      if (x[i] == j)
        std::cout << " # ";
      else
        std::cout << " - ";
    }
    std::cout << std::endl;
  }
}

bool can_place(int column, int current_queen, int* board)
{
  for (int i = 0; i < current_queen; ++i)
  {
    if (board[i] == column)
      return false;
    if (std::abs(board[i] - column) == std::abs(i - current_queen))
      return false;
  }
  return true;
}

bool nqueens(int current_queen, int n, int* board)
{
  if (current_queen == n)
  {
    print(board, n);
    return true;
  }
  for (int column = 0; column < n; ++column)
  {
    if (can_place(column, current_queen, board))
    {
      board[current_queen] = column;
      if (nqueens(current_queen+1, n, board))
        return true;
      board[current_queen] = -1;
    }
  }
  return false;
}

int main()
{
  std::cout << "queens: ";
  int n;
  std::cin >> n;
  int* board = new int[n];
  for (int i = 0; i < n; ++i)
    board[i] = -1;
  if (!nqueens(0, n, board))
    std::cout << "can't solve it\n";
  return 0;
}
