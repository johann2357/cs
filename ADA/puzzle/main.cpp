#include <queue>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
#define N 9

int goal[N] = {1, 2, 3, 4, 5, 6, 7, 8, 0};
int puzzle[N] = {1, 2, 3, 8, 0, 4, 7, 6, 5};
vector<int*> previous;
struct MComp
{
  int distance(int* array, int* array2)
  {
    int MSum = 0;
    for (int x = 0; x < N; ++x)
      MSum += abs(array[x] - array2[x]);
    return MSum % 10;
  }
  bool operator()(int* t1, int* t2)
  {
    if (distance(t1, goal) > distance(t2, goal))
      return true;
    return false;
  }
};
priority_queue<int*, vector<int*>, MComp> pq;
bool is_goal(int* a1)
{
  for (int x = 0; x < N; ++x)
    if (a1[x] != goal[x])
      return false;
  return true;
}
int* move_u(int* a1)
{
  int* tmp = new int [N];
  std::copy(a1, a1 + N, tmp);
  int x;
  for (x = 0; x < N; ++x)
    if (a1[x] == 0)
      break;
  if (x > 5)
    return tmp;
  else if (x > 2)
    swap(tmp[x], tmp[x+3]);
  else
  {
    swap(tmp[x], tmp[x+3]);
  }
  return tmp;
}
int* move_d(int* a1)
{
  int* tmp = new int [N];
  std::copy(a1, a1 + N, tmp);
  int x;
  for (x = 0; x < N; ++x)
    if (a1[x] == 0)
      break;
  if (x < 3)
    return tmp;
  else if (x < 7)
    swap(tmp[x], tmp[x-3]);
  else
  {
    swap(tmp[x], tmp[x-3]);
  }
  return tmp;
}
int* move_l(int* a1)
{
  int* tmp = new int [N];
  std::copy(a1, a1 + N, tmp);
  int x;
  for (x = 0; x < N; ++x)
    if (a1[x] == 0)
      break;
  if ((x == 1) || (x == 4) || (x == 7))
    swap(tmp[x], tmp[x+1]);
  else if ((x == 0) || (x == 3) || (x == 6))
  {
    swap(tmp[x], tmp[x+1]);
  }
  return tmp;
}
int* move_r(int* a1)
{
  int* tmp = new int [N];
  std::copy(a1, a1 + N, tmp);
  int x;
  for (x = 0; x < N; ++x)
    if (a1[x] == 0)
      break;
  if ((x == 1) || (x == 4) || (x == 7))
    swap(tmp[x], tmp[x+1]);
  else if ((x == 2) || (x == 5) || (x == 8))
  {
    swap(tmp[x], tmp[x-1]);
  }
  return tmp;
}
void print(int* ar)
{
  for (int i = 0; i < N; ++i)
  {
    if (i%3 == 0)
      cout << "\n ------------------- \n";
    cout << " | " << ar[i] << " | ";
  }
  cout << "\n ------------------- \n";
}
bool the_same(int* ar1, int* ar2)
{
  for (int i = 0; i < N; ++i)
    if (ar1[i] != ar2[i])
      return false;
  return true;
}
bool ok(int* ar1)
{
  for (int i = 0; i < previous.size(); ++i)
    if (the_same(ar1, previous[i]))
      return false;
  return true;
}
bool solve()
{
  if (pq.empty())
    return false;
  int* tmp = new int [N];
  std::copy(pq.top(), pq.top() + N, tmp);
  pq.pop();
  if (is_goal(tmp))
    return true;
  int* tmp2;
  tmp2 = move_d(tmp);
  if (! the_same(tmp, tmp2))
  {
    if (ok(tmp2))
    {
      pq.push(tmp2);
      previous.push_back(tmp2);
    }
  }
  tmp2 = move_l(tmp);
  if (! the_same(tmp, tmp2))
  {
    if (ok(tmp2))
    {
      pq.push(tmp2);
      previous.push_back(tmp2);
    }
  }
  tmp2 = move_r(tmp);
  if (! the_same(tmp, tmp2))
  {
    if (ok(tmp2))
    {
      pq.push(tmp2);
      previous.push_back(tmp2);
    }
  }
  tmp2 = move_u(tmp);
  if (! the_same(tmp, tmp2))
  {
    if (ok(tmp2))
    {
      pq.push(tmp2);
      previous.push_back(tmp2);
    }
  }
  if (solve())
    return true;
  else
  {
    print(tmp);
    return false;
  }
}
int main()
{
  int* tmp = new int [N];
  std::copy(puzzle, puzzle + N, tmp);
  pq.push(tmp);
  cout << "?: " << solve() << endl;
  for (int i = 0; i < previous.size(); ++i)
    print(previous[i]);

  return 0;
}
