#include<iostream>
 
int xMove[8] = {  2, 1, -1, -2, -2, -1,  1,  2 };
int yMove[8] = {  1, 2,  2,  1, -1, -2, -2, -1 };

int solve(int x, int y, int movei, int rows, int columns, int** sol);
 
int in_board(int x, int y, int rows, int columns, int** sol)
{
  if ( x >= 0 && x < rows && y >= 0 && y < columns && sol[x][y] == -1)
    return 1;
  return 0;
}
 
void print(int rows, int columns, int** sol)
{
  for (int x = 0; x < rows; x++)
  {
    for (int y = 0; y < columns; y++)
    {
      if (sol[x][y] < 10)
        std::cout << "  " << sol[x][y] << " ";
      else
        std::cout << " " << sol[x][y] << " ";
    }
    std::cout << "\n";
  }
}
 
bool solveKT(int rows, int columns, int x0, int y0)
{
  int** sol = new int* [rows];
  for (int i = 0; i < rows; ++i)
    sol[i] = new int [columns];

  for (int x = 0; x < rows; x++)
    for (int y = 0; y < columns; y++)
      sol[x][y] = -1;


  int x_start = x0;
  int y_start = y0;
  sol[x_start][y_start]  = 0;

  if(solve(x_start, y_start, 1, rows, columns, sol) == false)
  {
    std::cout << "Solution does not exist\n";
    return false;
  }
  print(rows, columns, sol);
  return true;
}
 
int solve(int x, int y, int movei, int rows, int columns, int** sol)
{
  int k, next_x, next_y;
  if (movei == rows*columns)
   return true;

  /* Try all next moves */
  for (k = 0; k < 8; k++)
  {
    next_x = x + xMove[k];
    next_y = y + yMove[k];
    if (in_board(next_x, next_y, rows, columns, sol))
    {
      sol[next_x][next_y] = movei;
      if (solve(next_x, next_y, movei+1, rows, columns, sol) == true)
        return true;
      else
      sol[next_x][next_y] = -1;// backtracking
    }
  }

  return false;
}
 
int main()
{
  int rows, columns, x, y;
  std::cout << "rows: ";
  std::cin >> rows;
  std::cout << "columns: ";
  std::cin >> columns;
  std::cout << "hrow: ";
  std::cin >> x;
  std::cout << "hcolumn: ";
  std::cin >> y;
  solveKT(rows, columns, x, y);
  return 0;
}
