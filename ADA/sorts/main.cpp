#include <iostream>
#include <time.h>

void insert_sort(int* vec, int tam)
{
  for (int i=1; i<tam; ++i)
    for (int j = i;((j > 0) && (vec[j] < vec[j-i])); --j)
      std::swap(vec[j], vec[j-1]);
}

void quick(int* vec, int ini, int fin)
{
  bool state = true;
  if (ini >= fin)
    return;
  int i = ini;
  for (int j = fin - 1; j != i;)
  {
    if (state)
    {
      if (vec[i] > vec[j])
      {
        std::swap(vec[i], vec[j]);
        std::swap(i, j);
        state = !state;
      }
      --j;
    }
    else
    {
      if (vec[i] < vec[j])
      {
        std::swap(vec[i], vec[j]);
        std::swap(i, j);
        state = !state;
      }
      ++j;
    }
  }
  quick(vec, ini, i);
  quick(vec, i+1, fin);
}

int main()
{
  int* a = new int [10];
  a[0] = 3;
  a[1] = 0;
  a[2] = 1;
  a[3] = 8;
  a[4] = 7;
  a[5] = 2;
  a[6] = 5;
  a[7] = 4;
  a[8] = 9;
  a[9] = 6;
  clock_t tStart = clock();
  quick(a, 0, 10);
  std::cout << "Time taken: " << (double)(clock() - tStart)/CLOCKS_PER_SEC;
  std::cout << "\n";
  for (int i = 0; i < 10; ++i)
    std::cout << a[i] << " ";
  std::cout << "\n";
  return 0;
}
