Let points[0..n-1] be the input array.

1) Find the bottom-most point by comparing y coordinate of all points. If there
 are two points with same y value, then the point with smaller x coordinate
 value is considered. Put the bottom-most point at first position.

2) Consider the remaining n-1 points and sort them by polor angle in
 counterclockwise order around points[0]. If polor angle of two points is same,
 then put the nearest point first.

3) Create an empty stack ‘S’ and push points[0], points[1] and points[2] to S.

4) Process remaining n-3 points one by one. Do following for every point
 ‘points[i]‘
    4.1) Keep removing points from stack while orientation of following
         3 points is not counterclockwise or they don’t make a left turn.
            a) Point next to top in stack
            b) Point at the top of stack
            c) points[i]
     4.2) Push points[i] to S

5) Print contents of S

