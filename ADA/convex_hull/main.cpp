#include <iostream>
#include <stack>
#include <stdlib.h>
#include <algorithm>
 
struct Point
{
  int x;
  int y;
};
 
Point p0;
 
// A utility function to find next to top in a stack
Point nextToTop(std::stack<Point> &S)
{
  Point p = S.top();
  S.pop();
  Point res = S.top();
  S.push(p);
  return res;
}
 
// return square of distance between p1 and p2
int dist(Point p1, Point p2)
{
  return (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y);
}
 
// To find orientation of ordered triplet (p, q, r).
int orientation(Point p, Point q, Point r)
{
  int val = (q.y - p.y) * (r.x - q.x) -
            (q.x - p.x) * (r.y - q.y);
  if (val == 0)
    return 0;   // colinear
  if (val > 0)
    return 1;   // clockwise
  return 2;     // countercc
}
 
// A function used by library function qsort() to sort an array of
// points with respect to the first point
int compare(const void *vp1, const void *vp2)
{
  Point *p1 = (Point *)vp1;
  Point *p2 = (Point *)vp2;
 
  // Find orientation
  int orien = orientation(p0, *p1, *p2);
  if (orien == 0)
  {
    if (dist(p0, *p2) >= dist(p0, *p1))
      return -1;
    return 1;
  }
  if (orien == 2)
    return -1;
  return 1;
}
 
void convex_hull(Point points[], int n)
{
   // Find the bottommost point
   int ymin = points[0].y;
   int min = 0;
   for (int i = 1; i < n; i++)
   {
     int y = points[i].y;
 
     // Pick the bottom-most or chose the left most point in case of tie
     if ((y < ymin) || (ymin == y && points[i].x < points[min].x))
        ymin = points[i].y, min = i;
   }
 
   // Place the bottom-most point at first position
   std::swap(points[0], points[min]);
 
   // Sort n-1 points with respect to the first point.  A point p1 comes
   // before p2 in sorted ouput if p2 has larger polar angle (in 
   // counterclockwise direction) than p1
   p0 = points[0];
   qsort(&points[1], n-1, sizeof(Point), compare);
 
   // Create an empty stack and push first three points to it.
   std::stack<Point> S;
   S.push(points[0]);
   S.push(points[1]);
   S.push(points[2]);
 
   // Process remaining n-3 points
   for (int i = 3; i < n; i++)
   {
      // Keep removing top while the angle formed by points next-to-top, 
      // top, and points[i] makes a non-left turn
      while (orientation(nextToTop(S), S.top(), points[i]) != 2)
         S.pop();
      S.push(points[i]);
   }
 
   // Now stack has the output points, print contents of stack
   while (!S.empty())
   {
       Point p = S.top();
       std::cout << "(" << p.x << ", " << p.y <<")" << std::endl;
       S.pop();
   }
}
 
int main()
{
  Point points[] = {
    {3, 0},
    {9, 0},
    {7, 1},
    {3, 3},
    {10, 3},
    {1, 4},
    {3, 6},
    {6, 7},
    {12, 5},
    {10, 6},
    {4, 8},
    {8, 8}
  };

  int n = sizeof(points)/sizeof(points[0]);
  convex_hull(points, n);
  return 0;
}
