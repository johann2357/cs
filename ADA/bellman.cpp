T* BellManFord(T data)
{
  T* bf_table = new T [nodeqty+1];

  bf_table[data]=0;
  for (int i=1; i<=nodeqty;i++)
  {
    if(data != i)
    {
      bf_table[i]=numeric_limits<T>::infinity();
    }
  }

  for (int i = 1; i < nodeqty-1; ++i)
  {
    Node<T> * tmp = SearchNode(i);

    for (typename std::vector<pair<Node<T>*, T> >::iterator a = tmp->edges.begin(); a != tmp->edges.end(); ++a)
    {
      bf_table[((*a).first)->data]=min(bf_table[((*a).first)->data], ((*a).second)+bf_table[i]);

    }

  }

  return bf_table;
}
