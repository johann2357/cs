#include <iostream>

typedef long long int T;

struct Matrix
{
  T data_[2][2];
  Matrix()
  {
    data_[0][0] = 0;
    data_[0][1] = 0;
    data_[1][0] = 0;
    data_[1][1] = 0;
  }
  ~Matrix()
  {
  }
  void operator() (const Matrix& x)
  {
    T x1, x2, x3, x4;
    x1 = data_[0][0]*x.data_[0][0] + data_[0][1]*x.data_[1][0];
    x2 = data_[0][0]*x.data_[0][1] + data_[0][1]*x.data_[1][1];
    x3 = data_[1][0]*x.data_[0][0] + data_[1][1]*x.data_[1][0];
    x4 = data_[1][0]*x.data_[0][1] + data_[1][1]*x.data_[1][1];
    data_[0][0] = x1;
    data_[0][1] = x2;
    data_[1][0] = x3;
    data_[1][1] = x4;
  }
};

T fibo(T n)
{
  Matrix m, acum;
  // initialize m
  m.data_[0][0] = 1;
  m.data_[0][1] = 1;
  m.data_[1][0] = 1;
  // initialize identity
  acum.data_[0][0] = 1;
  acum.data_[1][1] = 1;
  n -= 2;
  while (n)
  {
    if (n & 1)
      acum(m);  // acum = acum * m
    m(m);       // m = m * m
    n /= 2;
  }
  return acum.data_[0][0] + acum.data_[0][1];
}

int main()
{
  std::ios_base::sync_with_stdio(0);
  T num;
  std::cin >> num;
  std::cout << fibo(num) << "\n";
  //std::cout << (fibo(num+1)%10) << "\n";
  return 0;
}
