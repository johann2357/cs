#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <utility>
#include <fstream>

int main()
{
  // put the file in a vector
  std::vector<std::string> input;
  std::string line;
  std::ifstream myfile("input.txt");
  if (!myfile)
    return -1;
  while (std::getline(myfile, line))
    input.push_back(line);
  // get boundaries
  int max_width = input[0].size() - 1;
  int max_height = input.size();
  // initialize queue
  std::queue< std::pair<int, int> > to_fill;
  // initialize x y and new char
  int num_segments = 0;
  int x, y;
  char segment = 50;
  char segment2 = 50;
  bool too_many = false;
  // looping through the figure
  for (int i = 0; i < input.size(); ++i)
  {
    for (int j = 0; j < input[i].size(); j += 2)
    {
      if (input[i][j] == '1')
      {
        x = i;
        y = j;
        to_fill.push(std::make_pair(x, y));
        while (!to_fill.empty())
        {
          x = to_fill.front().first;
          y = to_fill.front().second;
          to_fill.pop();
          if ((x < 0) || (y < 0))
            continue;
          else if ((x >= max_height) || (y >= max_width))
            continue;
          else if (input[x][y] == '1')
          {
            input[x][y] = segment;
            if (too_many && (y > 0))
              input[x][y-1] = segment2;
            if (input[x-1][y] == '1')
              to_fill.push(std::make_pair(x-1, y));
            if (input[x+1][y] == '1')
              to_fill.push(std::make_pair(x+1, y));
            if (input[x][y-2] == '1')
              to_fill.push(std::make_pair(x, y-2));
            if (input[x][y+2] == '1')
              to_fill.push(std::make_pair(x, y+2));
            //
            if (input[x+1][y+2] == '1')
              to_fill.push(std::make_pair(x+1, y+2));
            if (input[x-1][y+2] == '1')
              to_fill.push(std::make_pair(x-1, y+2));
            if (input[x+1][y-2] == '1')
              to_fill.push(std::make_pair(x+1, y-2));
            if (input[x-1][y-2] == '1')
              to_fill.push(std::make_pair(x-1, y-2));
          }
        }
        ++num_segments;
        ++segment;
        if (segment > 126) {
          segment = 50;
          if (too_many)
            ++segment2;
          else
            too_many = true;
        }
        if (segment2 > 126)
          segment2 = 50;
        //std::cout << input[i][j] << " :x-> " <<  i << " :y-> " << j << " \n";
      }
    }
  }
  std::cout << "segmentos: " << num_segments << " \n";
  std::vector<std::string>::iterator it;
  for(it = input.begin(); it != input.end(); ++it)
  {
    std::cout << *it;
    std::cout << "\n";
  }
  return 0;
}

/*
 * BEST CASE -> lines (reading file) + n/2  (loop half matrix) + lines (printing)
 * WORST CASE -> lines (reading file) + n/2 * (flood fill)  (loop half matrix) + lines(printing)
 *
*/
