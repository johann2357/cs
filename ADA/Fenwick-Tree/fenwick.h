typedef int T;
#define SIZE 16

#include <algorithm>

class BIT
{
  T* data_;
  int size_;
public:
  BIT(int n)
  {
    size_ = n;
    data_ = new T[n];
  } 
  ~BIT() 
  {
    if (data_)
      delete data_;
  }
  void put_value(int i, T val) // O(log N)
  {
    while(i < size_)
    {
      data_[i] += val;
      i += (i & (i*-1));     // Add least-sig one   
    }   
  }
  T get_cumul(int i)    // read cumulative value
  {
    T ans = 0;               // initial value
    while(i > 0)
    {
      ans += data_[i];       // include this value
      i = (i & (i - 1));     // remove last sig one
    }
    return ans;
  }  
  T get_prob(int i)          // get individual frecuency
  {
    T ans = data_[i];             // get current prob
    if (i > 0)                    // i = 0 is a special case
    {
      int parent = (i & (i - 1)); // get parent node
      i -= 1;                     // the previous node
      while (parent != i)         // repeat until at prev parent
      {
        ans -= data_[i];          // adjust for traversed value
        i = (i & (i - 1));        // this parent
      }
    }
    return ans;
  }
  void show()                  // O(N)
  {
    for(int i=0; i < size_ ; ++i)
      std::cout << i << ":" << data_[i] << " ";
    std::cout << std::endl;
  }
  T freq_to(int i)
  {
    T ans = 0;
    while(i > 0)
    {
      ans = std::max(ans, data_[i]);
      i = (i & (i - 1));
    }
    return ans;
  }  
  void update(int i, T val)
  {
    while(i < size_)
    {
      data_[i] = std::max(val, data_[i]);
      i += (i & (i*-1));
    }
  }
  void LIS(T* vec)
  {
    for(int i = 0; i < size_; ++i)
    {
      data_[i] = freq_to(vec[i] - 1) + 1;
      update(vec[i], data_[i]);
    }
  }
};


void a_la_mala(T* input)
{
  T* lis = new T [SIZE];
  for(int i = 0; i < SIZE; ++i)
    lis[i] = 1;
  for(int i = 0; i < SIZE; ++i)
    for(int j = 0; j < i; ++j)
      if(input[i] > input[j])
        lis[i] = std::max(lis[i], lis[j] + 1);
  int answer = 1;
  for(int i = 0; i < SIZE; ++i)
    if(answer < lis[i])
      answer = lis[i];
  std::cout << "LIS: " << answer << std::endl;
  delete[] lis;
}
