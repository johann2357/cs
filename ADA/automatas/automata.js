var graph;
function main () {
    // inicializar grafo
    graph = Viva.Graph.graph();

    // inicializar svg junto con algunas animaciones
    var graphics = Viva.Graph.View.svgGraphics(),
        nodeSize = 24;
        highlightRelatedNodes = function(nodeId, isOn) {
           // cambiar de color a todos las aristas que apuntan al nodo
           graph.forEachLinkedNode(nodeId, function(node, link){
               if (nodeId == link.toId) {
                 var linkUI = graphics.getLinkUI(link.id);
                 if (linkUI) {
                     // linkUI is a UI object created by graphics below
                     linkUI.attr('stroke', isOn ? 'blue' : 'gray');
                 }
               }
           });
        };

    graphics.node(function(node) {
      // group of elements: http://www.w3.org/TR/SVG/struct.html#Groups
      var nodetype = 'Node.png';
      if (node.data == 'f')
        nodetype = "NodeFinal.png";
      else if (node.data == 'i')
        nodetype = "InitNode.png";
      else if (node.data == 'I')
        nodetype = "InitNodeFinal.png";
      var ui = Viva.Graph.svg('g'),
          // Create SVG text element with user id as content
          svgText = Viva.Graph.svg('text').attr('y', '-4px').text(node.id),
          img = Viva.Graph.svg('image')
             .attr('width', nodeSize)
             .attr('height', nodeSize)
             .link(nodetype);

      ui.append(svgText);
      ui.append(img);

        $(ui).hover(function() { // mouse over
            highlightRelatedNodes(node.id, true);
            var html = '<b>Selected Node: ' + node.id + '</b><ul>';
            var lks = node.links;
            for (var i = 0; i < lks.length; i += 1) {
                if (lks[i].toId == node.id)
                   html += '<li> <-- ' + lks[i].data + ' <-- ' + lks[i].fromId + '</li>';
            }
            html += '</ul>';
            $('#showrelated').html(html);
        }, function() { // mouse out
            highlightRelatedNodes(node.id, false);
            $('#showrelated').html('No node selected');
        });
      return ui;
    }).placeNode(function(nodeUI, pos) {
        // 'g' element doesn't have convenient (x,y) attributes, instead
        // we have to deal with transforms: http://www.w3.org/TR/SVG/coords.html#SVGGlobalTransformAttribute
        nodeUI.attr('transform',
                    'translate(' +
                          (pos.x - nodeSize/2) + ',' + (pos.y - nodeSize/2) +
                    ')');
    });

    // To render an arrow we have to address two problems:
    //  1. Links should start/stop at node's bounding box, not at the node center.
    //  2. Render an arrow shape at the end of the link.

    // Rendering arrow shape is achieved by using SVG markers, part of the SVG
    // standard: http://www.w3.org/TR/SVG/painting.html#Markers
    var createMarker = function(id) {
            return Viva.Graph.svg('marker')
                       .attr('id', id)
                       .attr('viewBox', "0 0 10 10")
                       .attr('refX', "10")
                       .attr('refY', "5")
                       .attr('markerUnits', "strokeWidth")
                       .attr('markerWidth', "10")
                       .attr('markerHeight', "5")
                       .attr('orient', "auto");
        },

        marker = createMarker('Triangle');
    marker.append('path').attr('d', 'M 0 0 L 10 5 L 0 10 z');

    var renderer = Viva.Graph.View.renderer(graph, {
            graphics : graphics
        });
    renderer.run();

    // Marker should be defined only once in <defs> child element of root <svg> element:
    var defs = graphics.getSvgRoot().append('defs');
    defs.append(marker);

    var geom = Viva.Graph.geom();

    graphics.link(function(link){
        //link
        //var label = Viva.Graph.svg('text').attr('id','label_'+link.data.id).text(link.data)
        //graphics.getSvgRoot().childNodes[0].append(label);
        // Notice the Triangle marker-end attribe:
        return Viva.Graph.svg('path')
                   .attr('stroke', 'gray')
                   .attr('marker-end', 'url(#Triangle)');
                   //.attr('id', link.data.id);
    }).placeLink(function(linkUI, fromPos, toPos) {
        // Here we should take care about
        //  "Links should start/stop at node's bounding box, not at the node center."

        // For rectangular nodes Viva.Graph.geom() provides efficient way to find
        // an intersection point between segment and rectangle
        var toNodeSize = nodeSize,
            fromNodeSize = nodeSize;

        var from = geom.intersectRect(
                // rectangle:
                        fromPos.x - fromNodeSize / 2, // left
                        fromPos.y - fromNodeSize / 2, // top
                        fromPos.x + fromNodeSize / 2, // right
                        fromPos.y + fromNodeSize / 2, // bottom
                // segment:
                        fromPos.x, fromPos.y, toPos.x, toPos.y)
                   || fromPos; // if no intersection found - return center of the node

        var to = geom.intersectRect(
                // rectangle:
                        toPos.x - toNodeSize / 2, // left
                        toPos.y - toNodeSize / 2, // top
                        toPos.x + toNodeSize / 2, // right
                        toPos.y + toNodeSize / 2, // bottom
                // segment:
                        toPos.x, toPos.y, fromPos.x, fromPos.y)
                    || toPos; // if no intersection found - return center of the node

        var data = 'M' + from.x + ',' + from.y +
                   'L' + to.x + ',' + to.y;

        linkUI.attr("d", data);

        //document.getElementById('label_'+linkUI.attr('id'))
       // 	.attr("x", (from.x + to.x) / 2)
       // 	.attr("y", (from.y + to.y) / 2);
    });
}
//============================================================================


    /*/ Finally we add something to the graph:

    var events = Viva.Graph.webglInputEvents(graphics, graph);

    events.click(function (node) {
        console.log(node);
    });
    graph.addNode('Q0', 'I');
    graph.addNode('Q1', 'f');
    graph.addNode('Q2', 'n');
    graph.addNode('Q3', 'n');
    graph.addLink('Q2', 'Q3');
    graph.addLink('Q1', 'Q2');
    graph.addLink('Q0', 'Q3');
    graph.addLink('Q3', 'Q1');
    /*/
