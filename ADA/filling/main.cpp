#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <utility>
#include <fstream>

int main()
{
  //436 x 384
  std::vector<std::string> input;
  std::string line;
  std::ifstream myfile("gato.txt");
  if (!myfile)
    return -1;
  while (std::getline(myfile, line))
    input.push_back(line);
  int thick = 2;
  std::cout << "FIND first position inside the figure..." << std::endl;
  bool first_inside = false;
  int x = 0;
  int y = 0;
  for (int i = 0; ((i < input.size()) && !first_inside); i += 1)
  {
    int inside = -1;
    bool close = true;
    bool stop = false;
    bool keep_looking = false;
    for (int j = 0; ((j < input[i].size()) && !stop); j += 2)
    {
      if (thick && (input[i][j] == '1'))
      {
        --thick;
        stop = true;
      }
      else
      {
        if (input[i][j] == '1') 
          keep_looking = true;
        else if (keep_looking)
        {
          if (close)
            inside = j;
          close = !close;
          keep_looking = false;
        }
      }
    }
    if (!stop && (close && (inside != -1)))
    {
      std::cout << input[i] << std::endl;
      std::cout << i << std::endl;
      std::cout << inside << std::endl;
      x = i;
      y = inside;
      first_inside = true;
    }
  }
  std::cout << "FILLING figure..." << std::endl;
  std::queue< std::pair<int, int> > to_fill;
  to_fill.push(std::make_pair(x, y));
  while (!to_fill.empty())
  {
    x = to_fill.front().first;
    y = to_fill.front().second;
    to_fill.pop();
    if (input[x][y] == '0')
    {
      input[x][y] = '1';
      if (input[x-1][y] == '0')
        to_fill.push(std::make_pair(x-1, y));
      if (input[x+1][y] == '0')
        to_fill.push(std::make_pair(x+1, y));
      if (input[x][y-2] == '0')
        to_fill.push(std::make_pair(x, y-2));
      if (input[x][y+2] == '0')
        to_fill.push(std::make_pair(x, y+2));
    }
  }
  std::vector<std::string>::iterator it;
  for(it = input.begin(); it != input.end(); ++it)
  {
    std::cout << *it;
    std::cout << "\n";
  }
  return 0;
  /*
  */
}
