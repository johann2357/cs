def decompose(number):
    #constants
    begin = 0
    end = number - 1
    # dynamic
    first = [-1] * number
    first[0] = number
    # counter
    counter = number
    # result
    result = []
    result.append(tuple(first))
    while counter > 0:
        # copy last one
        last_result = result[-1]
        # detect change
        next_change = True
        it = end
        while it > begin and next_change:
            if abs(last_result[it]) != 1:
                next_change = False
            else:
                it -= 1
        # new change or continue decomposing
        if next_change:
            first[0] = first[0] - 1
            tmp_first = tuple(first)
            balance = number - first[0]
            if balance > number / 2.0:
                # fix list to fit in
                balance = first[0]
            while balance > 1:
                tmp_it = 1
                tmp_number = number - first[0]
                while tmp_number > balance:
                    first[tmp_it] = balance
                    tmp_number -= balance
                    tmp_it += 1
                if tmp_number > 0:
                    first[tmp_it] = tmp_number
                result.append(tuple(first))
                first = list(tmp_first)
                balance -= 1
            counter -= 1
        else:
            tmp = list(last_result[0:it]) + [last_result[it] - 1] + [1]
            tmp += list(last_result[it + 1:-1])
            result.append(tuple(tmp))
    result.append(tuple([1] * number))
    return result


if __name__ == '__main__':
    for elem in decompose(12):
        print elem
