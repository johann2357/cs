#include <iostream>
#include <cstring>

int main()
{
  int number;
  std::cout << "enter a number: ";
  std::cin >> number;
  int begin = 0;
  int end = number - 1;
  int* line = new int [number];
  int* last = new int [number];
  int* tmp = new int [number];
  std::memset(line, 0, number*sizeof(int));
  line[0] = number;
  std::memcpy(last, line, number*(sizeof(int)));
  std::cout << number << "\n";
  for (int counter = 0; counter < number;)
  {
    bool next_change = true;
    int it = end;
    while ((it > begin) && next_change)
    {
      if (!((last[it] == 1) || (last[it] == 0)))
        next_change = false;
      else
        --it;
    }
    if (next_change)
    {
      line[0] = line[0] - 1;
      std::memcpy(tmp, line, number*(sizeof(int)));
      int balance = number - line[0];
      if (balance > (number / 2.0))
        balance = line[0];
      while (balance > 1)
      {
        int tmp_it = 1;
        int tmp_number = number - line[0];
        while (tmp_number > balance)
        {
          line[tmp_it] = balance;
          tmp_number -= balance;
          ++tmp_it;
        }
        if (tmp_number > 0)
          line[tmp_it] = tmp_number;
        std::cout << line[0];
        for (int i = 1; ((i < number) && (line[i] != 0)); ++i)
          std::cout << "+" << line[i];
        std::cout << "\n";
        std::memcpy(last, line, number*(sizeof(int)));
        std::memcpy(line, tmp, number*(sizeof(int)));
        --balance;
      }
      ++counter;
    }
    else
    {
      int k = 0;
      while (k < it)
      {
        tmp[k] = last[k];
        ++k;
      }
      tmp[k] = last[it] - 1;
      ++k;
      tmp[k] = 1;
      ++k;
      while (it < (number - 2))
      {
        tmp[k] = last[it+1];
        ++k;
        ++it;
      }
      std::memcpy(last, tmp, number*(sizeof(int)));
      std::cout << last[0];
      for (int i = 1; ((i < number) && (last[i] != 0)); ++i)
        std::cout << "+" << last[i];
      std::cout << "\n";
    }
  }
  for (int i = 0; (i < number-1); ++i)
    std::cout << 1 << "+";
  std::cout << "1\n";
  return 0;
}
